using System;

namespace Exceptions
{
    public class RonikaStructureException : RonikaException
    {
        public RonikaStructureException(string message) : base(message)
        {
        }

        public RonikaStructureException(string message, string ocureAddress) : base(message, ocureAddress)
        {
        }

        public RonikaStructureException(string message, string ocureAddress, Exception innerException) : base(message, ocureAddress, innerException)
        {
        }

        public RonikaStructureException(string message, object model) : base(message, model)
        {
        }
    }
}