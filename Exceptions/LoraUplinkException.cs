using System;

namespace Exceptions
{
    public class LoraUplinkException:RonikaException
    {
        public LoraUplinkException(string message) : base(message)
        {
        }

        public LoraUplinkException(string message, string ocureAddress) : base(message, ocureAddress)
        {
        }

        public LoraUplinkException(string message, string ocureAddress, Exception innerException) : base(message,
            ocureAddress, innerException)
        {
        }

        public LoraUplinkException(string message, object model) : base(message, model)
        {
        }
    }
}