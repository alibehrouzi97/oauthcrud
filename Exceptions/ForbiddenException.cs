using System;

namespace Exceptions
{
    public class ForbiddenException : RonikaException
    {
        public ForbiddenException(string message) : base(message)
        {
        }

        public ForbiddenException(string message, int statusCode) : base(message, statusCode)
        {
        }

        public ForbiddenException(string message, string ocureAddress) : base(message, ocureAddress)
        {
        }

        public ForbiddenException(string message, string ocureAddress, Exception innerException) : base(message, ocureAddress, innerException)
        {
        }

        public ForbiddenException(string message, object model) : base(message, model)
        {
        }
    }
}