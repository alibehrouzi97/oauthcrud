using System;

namespace Exceptions
{
    public class BadRequestException:RonikaException
    {
        
        public BadRequestException(string message) : base(message)
        {
        }

        public BadRequestException(string message, string ocureAddress) : base(message, ocureAddress)
        {
        }

        public BadRequestException(string message, string ocureAddress, Exception innerException) : base(message,
            ocureAddress, innerException)
        {
        }

        public BadRequestException(string message, object model) : base(message, model)
        {
        }
        
    }
}