using System;

namespace Exceptions
{
    public class RonikaDbException:RonikaException
    {
        
        public RonikaDbException(string message) : base(message)
        {
        }

        public RonikaDbException(string message, string ocureAddress) : base(message, ocureAddress)
        {
        }

        public RonikaDbException(string message, string ocureAddress, Exception innerException) : base(message,
            ocureAddress, innerException)
        {
        }

        public RonikaDbException(string message, object model) : base(message, model)
        {
        }
        
    }
}