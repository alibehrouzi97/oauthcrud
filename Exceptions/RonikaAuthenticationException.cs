using System;

namespace Exceptions
{
    public class RonikaAuthenticationException : RonikaDbException
    {
        public RonikaAuthenticationException(string message) : base(message)
        {
        }

        public RonikaAuthenticationException(string message, string ocureAddress) : base(message, ocureAddress)
        {
        }

        public RonikaAuthenticationException(string message, string ocureAddress, Exception innerException) : base(message,
            ocureAddress, innerException)
        {
        }

        public RonikaAuthenticationException(string message, object model) : base(message, model)
        {
        }
    }
}