using System;

namespace Exceptions
{
    public class NotFoundException : RonikaException
    {
        public NotFoundException(string message) : base(message)
        {
        }

        public NotFoundException(string message, string ocureAddress) : base(message, ocureAddress)
        {
        }

        public NotFoundException(string message, string ocureAddress, Exception innerException) : base(message,
            ocureAddress, innerException)
        {
        }

        public NotFoundException(string message, object model) : base(message, model)
        {
        }
    }
}