using System;

namespace Exceptions
{
    public class RonikaException : Exception
    {
        public ExceptionDetails ExceptionDetails { get; set; }
        public int StatusCode {get; set;}
                
        public RonikaException(string message):base(message)
        {
        }

        public RonikaException(string message, int statusCode): base(message)
        {
            StatusCode = statusCode;
        }
        
        public RonikaException(string message ,string ocureAddress ):base(message)
        {
        }

        public RonikaException(string message, string ocureAddress , Exception innerException):base(message,innerException)
        {
        }

        public RonikaException(string message , Object model):base(message)
        {
            
        }
        
    }

    public class ExceptionDetails
    {
        public string Message { set; get; }//Message to show to user
        public string OccurAddress { set; get; }
    }
}