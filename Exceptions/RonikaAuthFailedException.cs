using System;

namespace Exceptions
{
    public class RonikaAuthFailedException:RonikaException
    {
        
        public RonikaAuthFailedException(string message) : base(message, 401)
        {
        }

        public RonikaAuthFailedException(string message, string ocureAddress) : base(message, ocureAddress)
        {
        }

        public RonikaAuthFailedException(string message, string ocureAddress, Exception innerException) : base(message,
            ocureAddress, innerException)
        {
        }

        public RonikaAuthFailedException(string message, object model) : base(message, model)
        {
        }
        
    }
}