using System;

namespace Exceptions
{
    public class RonikaServerException:RonikaException
    {
        
        public RonikaServerException(string message) : base(message)
        {
        }

        public RonikaServerException(string message, string ocureAddress) : base(message, ocureAddress)
        {
        }

        public RonikaServerException(string message, string ocureAddress, Exception innerException) : base(message,ocureAddress, innerException)
        {
        }

        public RonikaServerException(string message, object model) : base(message, model)
        {
        }
        
    }
}