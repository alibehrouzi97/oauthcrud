using Microsoft.Extensions.DependencyInjection;
using Ronika.Utility.ApiResponse;

namespace Ronika.Utility.Extensions
{
    public static class ResponseCreatorExtension 
    {
        public static IServiceCollection AddResponseCreatorExtension(this IServiceCollection services)
        {
            services.AddTransient<IResponseCreator, ResponseCreator>();

            return services;
        }
    }
}