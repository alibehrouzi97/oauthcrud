namespace Ronika.Utility.Consts
{
    public static class RonikaErrorMessages
    {
        public static class Public
        {
            public static class StructureError
            {
                public const string Message = "StructureError";
            }
            public static class BadRequest
            {
                public const string Message = "BadRequest";
            }
            
            public static class ApiModelIsNull
            {
                public const string Message = "ApiModelIsNull";
            }
            
            public static class IdIsNull
            {
                public const string Message = "IdIsNull";
            }
            public static class ModelNotValid
            {
                public const string Message = "Model Not Valid";
            }
            
            public static class NotFound
            {
                public const string Message = "Not Found";
            }
            
            public static class DevEUIIsNull
            {
                public const string Message = "DevEUI Is Null";
            }
            
            public static class DuplicateId
            {
                public const string Message = "This Id Is Exist";
            }
            
            public static class InternalServerError
            {
                public const string Message = "Internal Server Error";
            }
            
            public static class DevEuiNotValid
            {
                public const string Message = "DEV-EUI معتبر نمی باشد ";
            }
            
            public static class NodeNameExist
            {
                public const string Message = "دستگاهی با این نام وجود دارد";
            }
            
            public static class DeviceIsActive
            {
                public const string Message = "این دستگاه فعال می باشد ";
            }
        }
        
        public static class User
        {
            public static class UserNameExits
            {
                public const string Message = "UserName Already Exits";
            }
            
            public static class ErrorCreatingUser
            {
                public const string Message = "Error Creating User";
            }
            
            public static class ErrorAddingClaim
            {
                public const string Message = "Error Adding Claim";
            }
            
        }
        
        public static class ApiResource
        {
            public static class ApiResourceNameExits
            {
                public const string Message = "ApiResource Already Exits";
            }
            
        }
        
        public static class Authentication
        {
            public static class UserNameAlreadyExits
            {
                public const string Message = "کاربر با این ایمیل وجود دارد";
            }
            public static class InvalidUserCredentials
            {
                public const string Message = "ایمیل یا رمز ورود شما نادرست می باشد";
            }
        }
    }
}