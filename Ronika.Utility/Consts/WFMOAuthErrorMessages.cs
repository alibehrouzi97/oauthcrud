namespace Ronika.Utility.Consts
{
    public static class OAuthErrorMessages
    {
        public static class Public
        {
            public static class StructureError
            {
                public const string Message = "StructureError";
            }
            
            public static class ApiModelIsNull
            {
                public const string Message = "ApiModelIsNull";
            }
            
            public static class IdIsNull
            {
                public const string Message = "IdIsNull";
            }
            
            public static class InternalServerError
            {
                public const string Message = "Internal Server Error";
            }
        }
        
        public static class User
        {
            public static class UserNameExits
            {
                public const string Message = "UserName Already Exits";
            }
            
            public static class ErrorCreatingUser
            {
                public const string Message = "Error Creating User";
            }

            public static class UserOrPasswordWrong
            {
                public const string Message = "Username or password is wrong";
            }   
            public static class ErrorAddingClaim
            {
                public const string Message = "Error adding claim to user";
            }
            
        }
        
        public static class ApiResource
        {
            public static class ApiResourceNameExits
            {
                public const string Message = "ApiResource Already Exits";
            }
            
        }
    }
}