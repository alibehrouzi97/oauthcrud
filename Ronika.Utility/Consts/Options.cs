namespace Ronika.Utility.Consts
{
    public class GlobalOptions
    {
        public string OAuthUrl {get; set;}
        public string UserWebApiUrl { get; set; }
        public string OAuthClientId { get; set; }
        public string OAuthClientSecret { get; set; }
        public string InfluxUrl {get; set;}
        public string InfluxDb {get; set;}
        public string InfluxUser {get; set;}
        public string InfluxPass {get; set;}
        public string DataPath {get; set;}

        public string NotificationServiceUrl {get; set;}
        public string NotificationToken {get; set;}

        public string LoraServiceProfileId {get; set;}
        public string LoraBaseUri {get; set;}
        public string LoraApplicationId {get; set;}
        public string LoraDeviceProfileId {get; set;}

        public string EmailNRUsername {get; set;}
        public string EmailNRPassword {get; set;}

        public string EmailTUsername { get; set; }
        public string EmailTPassword {get; set;}

        public GlobalOptions()
        {
            OAuthUrl = "http://localhost:5000";
            OAuthClientId = "client";
            OAuthClientSecret = "secret";
            UserWebApiUrl = "http://localhost:5001/user";
            InfluxUrl = "http://localhost:8086";
            InfluxDb = "InfluxTestDb";
            InfluxUser = "admin";
            InfluxPass = "password";
            DataPath = "C:/Users/Ali_bn99/RiderProjects/ronikawaterflowmeter/WaterFlowMeter_RonikaPars_V1";

            NotificationServiceUrl = "https://api.pushe.co";
            NotificationToken = "8ep64v4qpm20yojd";
            
            LoraServiceProfileId = "4d33d0b6-0c86-4293-9888-4d46fdabc1cd";
            LoraBaseUri = "http://ronikapars.com:8080/api/devices";
            LoraApplicationId = "2";
            LoraDeviceProfileId = "0fdb9177-a3e0-4256-a3fa-b10e7d417c00";
        }
    }
}