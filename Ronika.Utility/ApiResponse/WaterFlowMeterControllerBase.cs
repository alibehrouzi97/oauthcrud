using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.Extensions.DependencyInjection;
using Ronika.Utility.ApiResponse.Models;

namespace Ronika.Utility.ApiResponse
{
    public class RonikaControllerBase : ControllerBase
    {

        private IResponseCreator _responseCreator;
        
        public IResponseCreator ResponseCreator
        {
            get
            {
                if (_responseCreator == null)
                {
                    var httpContext = HttpContext;
                    IResponseCreator responseCreator;
                    if (httpContext == null)
                    {
                        responseCreator = (IResponseCreator) null;
                    }
                    else
                    {
                        responseCreator = httpContext.RequestServices?.GetRequiredService<IResponseCreator>();
                    }

                    _responseCreator = responseCreator;
                }

                return _responseCreator;
            }

            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException(nameof(value));
                }

                _responseCreator = value;
            }
        }

        protected RonikaControllerBase()
        {
            
        }

        public IActionResult ValidationError(ModelStateDictionary modelStateDictionary)
        {
            var errors = new List<ErrorMessage>();
            foreach (var (key, value) in ModelState)
            {
                if (value.Errors.Count >= 1)
                {
                    errors.Add(new ErrorMessage
                    {
                        Property = key,
                        Message = value.Errors.First().ErrorMessage
                    });
                }
            }

            return BadRequest(errors);
        }
        
        public override OkObjectResult Ok(object value)
        {
            return base.Ok(ResponseCreator.CreateOkResponse(value));
        }

        public ObjectResult Created(object value)
        {
            return base.StatusCode(202, ResponseCreator.CreateOkResponse(value));
        }
        
        public ObjectResult Updated(object value)
        {
            return base.StatusCode(204, ResponseCreator.CreateOkResponse(value));
        }
        
        public ObjectResult Deleted(object value)
        {
            return base.StatusCode(204, ResponseCreator.CreateOkResponse(value));
        }

        public override ObjectResult StatusCode(int statusCode, object value)
        {
            return base.StatusCode(statusCode, ResponseCreator.CreateOkResponse(value));
        }
        
        public override UnauthorizedObjectResult Unauthorized(object value)
        {
            if (value == null)
            {
                value = new ErrorMessage();
            }
            var msg = (ErrorMessage) value;
            return base.Unauthorized(ResponseCreator.CreateErrorResponse(msg));
        }

        public UnauthorizedObjectResult Unauthorized(string message)
        {
            return base.Unauthorized(ResponseCreator.CreateErrorResponse(message));
        }

        public UnauthorizedObjectResult Unauthorized(List<ErrorMessage> messages)
        {
            return base.Unauthorized(ResponseCreator.CreateErrorResponse(messages));
        }
        
        
        public override NotFoundObjectResult NotFound(object value)
        {
            if (value == null)
            {
                value = new ErrorMessage();
            }
            var msg = (ErrorMessage) value;
            return base.NotFound(ResponseCreator.CreateErrorResponse(msg));
        }

        public NotFoundObjectResult NotFound(string message)
        {
            return base.NotFound(ResponseCreator.CreateErrorResponse(message));
        }
        public NotFoundObjectResult NotFound(List<ErrorMessage> messages)
        {
            return base.NotFound(ResponseCreator.CreateErrorResponse(messages));
        }

        public override BadRequestObjectResult BadRequest(object value)
        {
            if (value == null)
            {
                value = new ErrorMessage();
            }
            var msg = (ErrorMessage) value;
            return base.BadRequest(ResponseCreator.CreateErrorResponse(msg));
        }

        public BadRequestObjectResult BadRequest(string message)
        {
            return base.BadRequest(ResponseCreator.CreateErrorResponse(message));
        }
        
        public BadRequestObjectResult BadRequest(List<ErrorMessage> messages)
        {
            return base.BadRequest(ResponseCreator.CreateErrorResponse(messages));
        }

        public ObjectResult InternalServerError(object value)
        {
            if (value == null)
            {
                value = new List<ErrorMessage>();
            }

            var msg = (ErrorMessage) value;
            
            return base.StatusCode(500, ResponseCreator.CreateOkResponse(msg));
        }

        public ObjectResult InternalServerError(string message)
        {
            return base.StatusCode(500, ResponseCreator.CreateErrorResponse(message));
        }
        
        public ObjectResult InternalServerError(List<ErrorMessage> messages)
        {
            return base.StatusCode(500, ResponseCreator.CreateErrorResponse(messages));
        }
    }
}