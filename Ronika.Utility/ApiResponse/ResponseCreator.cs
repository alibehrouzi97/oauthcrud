using System.Collections.Generic;
using Ronika.Utility.ApiResponse.Models;

namespace Ronika.Utility.ApiResponse
{
    public interface IResponseCreator
    {
        OkResponseData CreateOkResponse(object data);
        ErrorResponseData CreateErrorResponse(string message);
        ErrorResponseData CreateErrorResponse(ErrorMessage messages);
        ErrorResponseData CreateErrorResponse(List<ErrorMessage> messages);
    }
    
    public class ResponseCreator : IResponseCreator
    {
        public OkResponseData CreateOkResponse(object data)
        {
            return new OkResponseData
            {
                Status = "Ok",
                Data = data
            };
        }

        public ErrorResponseData CreateErrorResponse(string message)
        {
            
            return new ErrorResponseData
            {
                Status = "Error",
                ErrorMessages = new List<ErrorMessage>
                {
                    new ErrorMessage
                    {
                        Message = message
                    }
                }
            };
        }

        public ErrorResponseData CreateErrorResponse(ErrorMessage messages)
        {
            return new ErrorResponseData
            {
                Status = "Error",
                ErrorMessages = new List<ErrorMessage>
                {
                    messages
                }
            };
        }

        public ErrorResponseData CreateErrorResponse(List<ErrorMessage> messages)
        {
            if (messages == null)
            {
                messages = new List<ErrorMessage>();
            }
            
            return new ErrorResponseData
            {
                Status = "Error",
                ErrorMessages = messages
            };
        }
    }
}