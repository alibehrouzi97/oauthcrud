using System.Collections.Generic;
using Newtonsoft.Json;

namespace Ronika.Utility.ApiResponse.Models
{
    public class OkResponseData
    {
        public string Status { get; set; }
        public object Data { get; set; } 
    }

    public class ErrorResponseData
    {
        [JsonProperty("status")] public string Status { get; set; }
        [JsonProperty("error")] public List<ErrorMessage> ErrorMessages { get; set; }
    }

    public class ErrorMessage
    {
        [JsonProperty("property", NullValueHandling = NullValueHandling.Ignore)] public string Property { get; set; }
        [JsonProperty("message")]  public string Message { get; set; } 
    }
}