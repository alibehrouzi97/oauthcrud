using System;
using Microsoft.Extensions.Logging;

namespace Ronika.Utility
{
    public class LogRequest
    {
        public string Application {get; set;}
        public LogLevel Level { get; set; }
        public string Message { get; set; }
        public EventId EventId { get; set; }
        public Exception Exception { get; set; }

        public LogRequest(string application, LogLevel logLevel, EventId eventId, Exception exception, string message)
        {
            Level = logLevel;
            EventId = eventId;
            Exception = exception;
            Message = message;
            Application = application;
        }
    }
}