using System;
using System.Net;
using System.Threading.Tasks;
using Exceptions;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Ronika.Utility.ApiResponse;

namespace Ronika.ExceptionMiddleware
{
    public class ExceptionMiddleware
    {
        
        private readonly RequestDelegate _next;
        
 

        public ExceptionMiddleware(RequestDelegate next)
        {
            _next = next;
        }


        private async Task GenerateResponse(string message, HttpContext context, int stsCode = 0)
        {
            IServiceProvider requestServices = context.RequestServices;
            var responseGenerator = requestServices.GetRequiredService<IResponseCreator>();

            var result =
                JsonConvert.SerializeObject(responseGenerator.CreateErrorResponse(message));
            context.Response.ContentType = "application/json";
            if (stsCode == 0)
            {
                context.Response.StatusCode = (int) HttpStatusCode.InternalServerError;
            }
            else
            {
                context.Response.StatusCode = stsCode;

            }
            await context.Response.WriteAsync(result);
        }

        public async Task Invoke(HttpContext context, ILogger<ExceptionMiddleware> logger)
        {

            try
            {
                await _next(context);
            }
            catch (RonikaAuthenticationException ex)
            {
                logger.LogWarning(
                    $"Exception : type of {ex.GetType()} Occur in : {ex.ExceptionDetails.OccurAddress}\n" +
                    $"{ex.Message}\n" +
                    $"{ex.InnerException?.Message}\n" +
                    $"End Exception");

                var msgText = ex.ExceptionDetails.Message;

                await GenerateResponse(msgText, context, 401);
            }
            catch (BadRequestException ex)
            {
                logger.LogWarning(
                    $"Exception : type of {ex.GetType()} Occur in : {ex.ExceptionDetails.OccurAddress}\n" +
                    $"{ex.Message}\n" +
                    $"{ex.InnerException?.Message}\n" +
                    $"End Exception");

                var msgText = ex.ExceptionDetails.Message;

                await GenerateResponse(msgText, context, 400);
            }
            catch (NotFoundException ex)
            {
                logger.LogWarning(
                    $"Exception : type of {ex.GetType()} Occur in : {ex.ExceptionDetails.OccurAddress}\n" +
                    $"{ex.Message}\n" +
                    $"{ex.InnerException?.Message}\n" +
                    $"End Exception");

                var msgText = ex.ExceptionDetails.Message;

                await GenerateResponse(msgText, context, 404);
            }
            catch (RonikaStructureException ex)
            {
                logger.LogWarning(
                    $"Exception : type of {ex.GetType()} Occur in : {ex.ExceptionDetails.OccurAddress}\n" +
                    $"{ex.Message}\n" +
                    $"{ex.InnerException?.Message}\n" +
                    $"End Exception");

                var msgText = ex.ExceptionDetails.Message;

                await GenerateResponse(msgText, context, 500);
            }
            catch (RonikaDbException ex)
            {
                logger.LogWarning(
                    $"Exception : type of {ex.GetType()} Occur in : {ex.ExceptionDetails.OccurAddress}\n" +
                    $"{ex.Message}\n" +
                    $"{ex.InnerException?.Message}\n" +
                    $"End Exception");

                var msgText = ex.ExceptionDetails.Message;

                await GenerateResponse(msgText, context, 500);
            }
            catch (LoraUplinkException ex)
            {
                logger.LogWarning(
                    $"Exception : type of {ex.GetType()} Occur in : {ex.ExceptionDetails.OccurAddress}\n" +
                    $"{ex.Message}\n" +
                    $"{ex.InnerException?.Message}\n" +
                    $"End Exception");

                var msgText = ex.ExceptionDetails.Message;

                await GenerateResponse(msgText, context, 500);
            }
            catch (ForbiddenException ex)
            {
                logger.LogWarning(
                    $"Exception : type of {ex.GetType()} Occur in : {ex.ExceptionDetails.OccurAddress}\n" +
                    $"{ex.Message}\n" +
                    $"{ex.InnerException?.Message}\n" +
                    $"End Exception");

                var msgText = ex.ExceptionDetails.Message;

                await GenerateResponse(msgText, context, 403);
            }
            catch (RonikaException ex)
            {
                logger.LogError(
                    $"Exception : type of {ex.GetType()} Occur in : {ex.ExceptionDetails.OccurAddress}\n" +
                    $"{ex.Message}\n" +
                    $"{ex.InnerException?.Message}\n" +
                    $"End Exception");

                var msgText = ex.ExceptionDetails.Message;
                
                if (ex.StatusCode > 0)
                {
                    await GenerateResponse(msgText, context, ex.StatusCode);
                }
                else
                {
                    await GenerateResponse(msgText, context);
                }
            }
            catch (Exception ex)
            {
                logger.LogError(
                    $"Unhandled Exception : type of {ex.GetType()} Occur in : {ex.Source}\n" +
                    $"{ex.Message}\n" +
                    $"{ex.InnerException?.Message}\n" +
                    $"Trace :\n" +
                    $"{ex.StackTrace}\n" +
                    $"End Exception");

                await GenerateResponse("public.InternalServer", context);
            }
        }
    }
}