using Microsoft.AspNetCore.Builder;

namespace Ronika.ExceptionMiddleware.Extensions
{
    public static class AddExceptionMiddleWareExtension
    {
        public static IApplicationBuilder AddExceptionMiddleWare(this IApplicationBuilder app)
        {
            app.UseMiddleware<ExceptionMiddleware>();
            return app;
        }
    }
}