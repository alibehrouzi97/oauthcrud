using System;
using System.Collections.Generic;
using System.Reflection;
using System.Threading.Tasks;
using Exceptions;
using Microsoft.Extensions.Logging;
using OAuthCRUD.Models.ApiModels.ClientClaimApiModels;
using OAuthCRUD.Models.Entities.ClientClaimEntity;
using OAuthCRUD.Models.RepoParams.ClientClaim;
using OAuthCRUD.Repository;
using Ronika.Utility.Consts;

namespace OAuthCRUD.Manager
{
    public interface IClientClaimManager
    {
        Task<ClientClaimObject> UpdateClientClaimAsync(UpdateClientClaimApiModel clientClaimModel, int id);
        Task<ClientClaimObject> CreateClientClaimAsync(CreateClientClaimApiModel createClientClaimApiModel);
        Task<ClientClaimObject> GetOneClientClaimAsync(int id);
        Task<List<ClientClaimObject>> GetAllClientClaimsAsync();
        Task DeleteClientClaimAsync(int id);
    }
    public class ClientClaimManager : IClientClaimManager
    {
        
        private readonly IClientClaimRepository _clientClaimRepository;
        
        public ClientClaimManager( IClientClaimRepository clientClaimRepository)
        {
            _clientClaimRepository = clientClaimRepository;
            
        }
        
        public async Task<ClientClaimObject> CreateClientClaimAsync(CreateClientClaimApiModel createClientClaimApiModel)
        {
            

            var createClientClaimRepoParam = new CreateClientClaimRepoParam(createClientClaimApiModel);

            var createClientClaimRepoResult=await _clientClaimRepository.CreateClientClaimAsync(createClientClaimRepoParam);

            if (createClientClaimRepoResult == null)
            {
                throw new RonikaStructureException("createClientClaimRepoResult Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 49"
                    }
                };               

            }
            
            return new ClientClaimObject(createClientClaimRepoResult);
            
        }
        public async Task<ClientClaimObject> UpdateClientClaimAsync(UpdateClientClaimApiModel clientClaimModel,int id)
        {
            
            if (id.ToString() == null)
            {
                throw new RonikaStructureException(" Id IS Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 68"
                    }
                };
                
            }
            

            var updateClientClaimRepoParam = new UpdateClientClaimRepoParam(clientClaimModel);
            
            var createClientClaimRepoResult=await _clientClaimRepository.UpdateClientClaimAsync(updateClientClaimRepoParam,id);

            if (createClientClaimRepoResult == null)
            {
                throw new RonikaStructureException("createClientClaimRepoResult Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 86"
                    }
                };               

            }
            return new ClientClaimObject(createClientClaimRepoResult);
            
        }
        
        public async Task<List<ClientClaimObject>> GetAllClientClaimsAsync()
        {
            
            var clientClaimRepoResults = await _clientClaimRepository.GetAllClientClaimsAsync();

            if (clientClaimRepoResults==null)
            {
                throw new RonikaStructureException("clientClaimRepoResults Is Null")
                {
                    ExceptionDetails = new ExceptionDetails
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 107"
                    }
                };                  
            }
            
            var allClientClaimObjects=new List<ClientClaimObject>();
                
            foreach (var clientClaimRepoResult in clientClaimRepoResults)
            {
                allClientClaimObjects.Add(new ClientClaimObject(clientClaimRepoResult));
            }

            return allClientClaimObjects;
        }
        
        public async Task<ClientClaimObject> GetOneClientClaimAsync(int id)
        {
            
            if (id.ToString() == null)
            {
                throw new RonikaStructureException(" Id IS Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 132"
                    }
                };
                
            }

            var clientClaimRepoResult = await _clientClaimRepository.GetOneClientClaimAsync(id);
                
            if (clientClaimRepoResult == null)
            {
                
                throw new RonikaStructureException("clientClaimRepoResult Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 148"
                    }
                };
            }
            
            return new ClientClaimObject(clientClaimRepoResult);
            
        }
        public async Task DeleteClientClaimAsync(int id)
        {
            
            if (id.ToString() == null)
            {
                
                throw new RonikaStructureException(" Id IS Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 167"
                    }
                };
                
            }

            var clientClaimRepoResult = await _clientClaimRepository.GetOneClientClaimAsync(id);
                
            if (clientClaimRepoResult == null)
            {
                
                throw new RonikaStructureException("clientClaimRepoResult Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 183"
                    }
                };
            }
            
            await _clientClaimRepository.DeleteClientClaimAsync(id);

            
        }
        
    }
    
    
}