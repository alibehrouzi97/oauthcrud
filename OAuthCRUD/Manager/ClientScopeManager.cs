using System;
using System.Collections.Generic;
using System.Reflection;
using System.Threading.Tasks;
using Exceptions;
using OAuthCRUD.Models.ApiModels.ClientRedirectUriApiModels;
using OAuthCRUD.Models.Entities.ClientRedirectUriEntity;
using OAuthCRUD.Models.RepoParams.ClientRedirectUriEntity;
using Microsoft.Extensions.Logging;
using OAuthCRUD.Models.ApiModels.ClientScopeApiModels;
using OAuthCRUD.Models.Entities.ClientScopeEntity;
using OAuthCRUD.Models.RepoParams.ClientScopeEntity;
using OAuthCRUD.Repository;
using Ronika.Utility.Consts;

namespace OAuthCRUD.Manager
{
    public interface IClientScopeManager
    {
        Task DeleteClientScopeAsync(int id);
        Task<ClientScopeObject> GetOneClientScopeAsync(int id);
        Task<List<ClientScopeObject>> GetAllClientScopesAsync();
        Task<ClientScopeObject> UpdateClientScopeAsync(UpdateClientScopeApiModel updateClientScopeApiModel, int id);
        Task<ClientScopeObject> CreateClientScopeAsync(CreateClientScopeApiModel createClientScopeApiModel);

    }
    public class ClientScopeManager : IClientScopeManager
    {
        
        private readonly IClientScopeRepository _clientScopeRepository;
        
        public ClientScopeManager( IClientScopeRepository clientScopeRepository)
        {
            
            _clientScopeRepository = clientScopeRepository;
        }
        
        public async Task<ClientScopeObject> CreateClientScopeAsync(CreateClientScopeApiModel createClientScopeApiModel)
        {
            

            var createClientScopeRepoParam = new CreateClientScopeRepoParam(createClientScopeApiModel);
            
            
            var createClientScopeRepoResult = await _clientScopeRepository.CreateClientScopeAsync(createClientScopeRepoParam);

            if (createClientScopeRepoResult == null)
            {
                throw new RonikaStructureException("createClientScopeRepoResult Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 54"
                    }
                };               

            }
            
            return new ClientScopeObject(createClientScopeRepoResult);

            
        }
        public async Task<ClientScopeObject> UpdateClientScopeAsync(UpdateClientScopeApiModel updateClientScopeApiModel,int id)
        {
            
            if (id.ToString() == null)
            {
                throw new RonikaStructureException(" Id IS Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 74"
                    }
                };
            }
            

            var updateClientScopeRepoParam = new UpdateClientScopeRepoParam(updateClientScopeApiModel);
            
            var updateClientScopeRepoResult = await _clientScopeRepository.UpdateClientScopeAsync(updateClientScopeRepoParam,id);

            if (updateClientScopeRepoResult == null)
            {
                throw new RonikaStructureException("updateClientScopeRepoResult Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 91"
                    }
                };               

            }
            
            return new ClientScopeObject(updateClientScopeRepoResult);

            
        }
        
        public async Task<List<ClientScopeObject>> GetAllClientScopesAsync()
        {
            
            var createClientScopeRepoResults = await _clientScopeRepository.GetAllClientScopesAsync();

            if (createClientScopeRepoResults==null)
            {
                throw new RonikaStructureException("createClientScopeRepoResults Is Null")
                {
                    ExceptionDetails = new ExceptionDetails
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 114"
                    }
                };                  
            }
            
            var clientScopeObjects=new List<ClientScopeObject>();
                
            foreach (var clientScopeRepoResult in createClientScopeRepoResults)
            {
                clientScopeObjects.Add(new ClientScopeObject(clientScopeRepoResult));
            }
            
            return clientScopeObjects;
            
        }
        
        public async Task<ClientScopeObject> GetOneClientScopeAsync(int id)
        {
            
            if (id.ToString() == null)
            {
                throw new RonikaStructureException("id Is Null In Manager ")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 140"
                         
                    }
                };
            }

            var clientScopeRepoResult = await _clientScopeRepository.GetOneClientScopeAsync(id);

            if (clientScopeRepoResult == null)
            {
                
                throw new RonikaStructureException("clientScopeRepoResult Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 156"
                    }
                };
            }
            
            return new ClientScopeObject(clientScopeRepoResult);

            
        }
        public async Task DeleteClientScopeAsync(int id)
        {
            
            if (id.ToString() == null)
            {
                throw new RonikaStructureException("Id IS Null  ")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message =  RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 175"
                    }
                };
            }

            var clientScopeRepoResult = await _clientScopeRepository.GetOneClientScopeAsync(id);

            if (clientScopeRepoResult == null)
            {
                
                throw new RonikaStructureException("clientScopeRepoResult Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 190"
                    }
                };
            }
            
            await _clientScopeRepository.DeleteClientScopeAsync(id);

        }
        
    }
}