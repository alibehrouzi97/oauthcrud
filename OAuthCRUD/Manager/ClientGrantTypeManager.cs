using System;
using System.Collections.Generic;
using System.Reflection;
using System.Threading.Tasks;
using Exceptions;
using Microsoft.Extensions.Logging;
using OAuthCRUD.Models.ApiModels.ClientGrantTypeApiModels;
using OAuthCRUD.Models.Entities.ClientGrantTypeEntity;
using OAuthCRUD.Models.RepoParams.ClientGrantType;
using OAuthCRUD.Repository;
using Ronika.Utility.Consts;

namespace OAuthCRUD.Manager
{
    public interface IClientGrantTypeManager
    {
        Task DeleteClientGrantTypeAsync(int id);
        Task<ClientGrantTypeObject> GetOneClientGrantTypeAsync(int id);
        Task<List<ClientGrantTypeObject>> GetAllClientGrantTypesAsync();
        Task<ClientGrantTypeObject> UpdateClientGrantTypeAsync(UpdateClientGrantTypeApiModel updateClientGrantTypeApiModel, int id);
        Task<ClientGrantTypeObject> CreateClientGrantTypeAsync(CreateClientGrantTypeApiModel createClientGrantTypeApi);


    }
    public class ClientGrantTypeManager : IClientGrantTypeManager
    {
        

        
        private readonly IClientGrantTypeRepository _clientGrantTypeRepository;
        
        public ClientGrantTypeManager( IClientGrantTypeRepository clientGrantTypeRepository)
        {

            _clientGrantTypeRepository = clientGrantTypeRepository;
        }
        
        public async Task<ClientGrantTypeObject> CreateClientGrantTypeAsync(CreateClientGrantTypeApiModel createClientGrantTypeApi)
        {
            

            var createClientGrantTypeRepoParam = new CreateClientGrantTypeRepoParam(createClientGrantTypeApi);

            var createClientGrantTypeRepoResult=await _clientGrantTypeRepository.CreateClientGrantTypeAsync(createClientGrantTypeRepoParam);

            if (createClientGrantTypeRepoResult == null)
            {
                throw new RonikaStructureException("createClientGrantTypeRepoResult Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 53"
                    }
                };               

            }
            
            return new ClientGrantTypeObject(createClientGrantTypeRepoResult);
            
        }
        public async Task<ClientGrantTypeObject> UpdateClientGrantTypeAsync(UpdateClientGrantTypeApiModel updateClientGrantTypeApiModel,int id)
        {
            
            if (id.ToString() == null)
            {
                throw new RonikaStructureException(" Id IS Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 72"
                    }
                };
            }
            

            var updateClientGrantTypeRepoParam = new UpdateClientGrantTypeRepoParam(updateClientGrantTypeApiModel);

            var createClientGrantTypeRepoResult=await _clientGrantTypeRepository.UpdateClientGrantTypeAsync(updateClientGrantTypeRepoParam,id);

            if (createClientGrantTypeRepoResult == null)
            {
                throw new RonikaStructureException("createClientGrantTypeRepoResult Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 89"
                    }
                };               

            }
            
            return new ClientGrantTypeObject(createClientGrantTypeRepoResult);
            
        }
        public async Task<List<ClientGrantTypeObject>> GetAllClientGrantTypesAsync()
        {
            
            
            var grantTypeRepoResults = await _clientGrantTypeRepository.GetAllClientGrantTypesAsync();

            if (grantTypeRepoResults==null)
            {
                throw new RonikaStructureException("grantTypeRepoResults Is Null")
                {
                    ExceptionDetails = new ExceptionDetails
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 111"
                    }
                };                  
            }
            
            var clientGrantTypeObjects=new List<ClientGrantTypeObject>();
                
            foreach (var grantTypeRepoResult in grantTypeRepoResults)
            {
                clientGrantTypeObjects.Add(new ClientGrantTypeObject(grantTypeRepoResult));
            }

            return clientGrantTypeObjects;
            
        }
        public async Task<ClientGrantTypeObject> GetOneClientGrantTypeAsync(int id)
        {
            
            if (id.ToString() == null)
            {
                throw new RonikaStructureException(" Id IS Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 136"
                    }
                };
                
            }

            var clientGrantTypeRepoResult = await _clientGrantTypeRepository.GetOneClientGrantTypeAsync(id);
             
            if (clientGrantTypeRepoResult == null)
            {
                
                throw new RonikaStructureException("clientGrantTypeRepoResult Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 152"
                    }
                };
            }
            
            return new ClientGrantTypeObject(clientGrantTypeRepoResult);
            
        }
        public async Task DeleteClientGrantTypeAsync(int id)
        {
            
            if (id.ToString() == null)
            {
                throw new RonikaStructureException(" Id IS Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 170"
                    }
                };
                
            }
            
            var clientGrantTypeRepoResult = await _clientGrantTypeRepository.GetOneClientGrantTypeAsync(id);
             
            if (clientGrantTypeRepoResult == null)
            {
                
                throw new RonikaStructureException("clientGrantTypeRepoResult Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 186"
                    }
                };
            }

            await _clientGrantTypeRepository.DeleteClientGrantTypeAsync(id);

        }
    }
}