using System;
using System.Collections.Generic;
using System.Reflection;
using System.Threading.Tasks;
using Exceptions;
using OAuthCRUD.Models.Entities.ClientClaimEntity;
using Microsoft.Extensions.Logging;
using OAuthCRUD.Models.ApiModels.ClientCorsOriginApiModel;
using OAuthCRUD.Models.Entities.ClientCorsOriginEntity;
using OAuthCRUD.Models.RepoParams.ClientCorsOrigin;
using OAuthCRUD.Repository;
using Ronika.Utility.Consts;

namespace OAuthCRUD.Manager
{
    public interface IClientCorsOriginManager
    {
        Task DeleteClientCorsOriginAsync(int id);
        Task<ClientCorsOriginObject> GetOneClientCorsOriginAsync(int id);
        Task<List<ClientCorsOriginObject>> GetAllClientCorsOriginAsync();
        Task<ClientCorsOriginObject> UpdateClientCorsOriginAsync(UpdateClientCorsOriginApiModel clientCorsOriginApiModel, int id);
        Task<ClientCorsOriginObject> CreateClientCorsOriginAsync(CreateClientCorsOriginApiModel createClientCorsOriginApiModel);

    }
    public class ClientCorsOriginManager : IClientCorsOriginManager
    {
        
        
        private readonly IClientCorsOriginRepository _clientCorsOriginRepository;

        public ClientCorsOriginManager( IClientCorsOriginRepository clientCorsOriginRepository)
        {
            _clientCorsOriginRepository = clientCorsOriginRepository;
            
        }
        
        public async Task<ClientCorsOriginObject> CreateClientCorsOriginAsync(CreateClientCorsOriginApiModel createClientCorsOriginApiModel)
        {
            

            var createClientCorsOriginRepoParam = new CreateClientCorsOriginRepoParam(createClientCorsOriginApiModel);

            var createClientCorsOriginRepoResult=await _clientCorsOriginRepository.CreateClientCorsOriginAsync(createClientCorsOriginRepoParam);

            if (createClientCorsOriginRepoResult == null)
            {
                throw new RonikaStructureException("createClientCorsOriginRepoResult Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 52"
                    }
                };               

            }
            
            return new ClientCorsOriginObject(createClientCorsOriginRepoResult);
            
        }
        public async Task<ClientCorsOriginObject> UpdateClientCorsOriginAsync(UpdateClientCorsOriginApiModel clientCorsOriginApiModel,int id)
        {
            
            if (id.ToString() == null)
            {
                throw new RonikaStructureException(" Id IS Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 71"
                    }
                };
            }
            

            var updateClientCorsOriginRepoParam = new UpdateClientCorsOriginRepoParam(clientCorsOriginApiModel);
            
           var createClientCorsOriginRepoResult =await _clientCorsOriginRepository.UpdateClientCorsOriginAsync(updateClientCorsOriginRepoParam,id);

           if (createClientCorsOriginRepoResult == null)
           {
               throw new RonikaStructureException("createClientCorsOriginRepoResult Is Null")
               {
                   ExceptionDetails = new ExceptionDetails()
                   {
                       Message = RonikaErrorMessages.Public.StructureError.Message,
                       OccurAddress = MethodBase.GetCurrentMethod().Name + ": 88"
                   }
               };               

           }
           
           return new ClientCorsOriginObject(createClientCorsOriginRepoResult);
            
        }
        public async Task<List<ClientCorsOriginObject>> GetAllClientCorsOriginAsync()
        {
            
            var clientCorsOriginRepoResults = await _clientCorsOriginRepository.GetAllClientCorsOriginsAsync();

            if (clientCorsOriginRepoResults==null)
            {
                throw new RonikaStructureException("clientCorsOriginRepoResults Is Null")
                {
                    ExceptionDetails = new ExceptionDetails
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 109"
                    }
                };                  
            }
            
            
            var allClientCorsOriginObjects=new List<ClientCorsOriginObject>();
                
            foreach (var clientCorsOriginRepoResult in clientCorsOriginRepoResults)
            {
                allClientCorsOriginObjects.Add(new ClientCorsOriginObject(clientCorsOriginRepoResult));
            }

            return allClientCorsOriginObjects;
            
        }
        public async Task<ClientCorsOriginObject> GetOneClientCorsOriginAsync(int id)
        {
            
            if (id.ToString() == null)
            {
                throw new RonikaStructureException(" Id IS Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 135"
                    }
                };
                
            }

            var clientCorsOriginRepoResult = await _clientCorsOriginRepository.GetOneClientCorsOriginAsync(id);
                
            if (clientCorsOriginRepoResult == null)
            {
                
                throw new RonikaStructureException("clientCorsOriginRepoResult Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 151"
                    }
                };
            }
            
            return new ClientCorsOriginObject(clientCorsOriginRepoResult);
        }
        public async Task DeleteClientCorsOriginAsync(int id)
        {
            
            if (id.ToString() == null)
            {
                throw new RonikaStructureException(" Id IS Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 168"
                    }
                };
                
            }

            var clientCorsOriginRepoResult = await _clientCorsOriginRepository.GetOneClientCorsOriginAsync(id);
                
            if (clientCorsOriginRepoResult == null)
            {
                
                throw new RonikaStructureException("clientCorsOriginRepoResult Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 184"
                    }
                };
            }
            
            await _clientCorsOriginRepository.DeleteClientCorsOriginAsync(id);

            
        }
    }
}