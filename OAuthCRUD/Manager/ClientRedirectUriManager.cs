using System;
using System.Collections.Generic;
using System.Reflection;
using System.Threading.Tasks;
using Exceptions;
using OAuthCRUD.Models.ApiModels.ClientPostLogoutRedirectUriApiModuels;
using OAuthCRUD.Models.Entities.ClientPostLogoutRedirectUriEntity;
using OAuthCRUD.Models.RepoParams.ClientPostLogoutRedirectUri;
using Microsoft.Extensions.Logging;
using OAuthCRUD.Models.ApiModels.ClientRedirectUriApiModels;
using OAuthCRUD.Models.Entities.ClientRedirectUriEntity;
using OAuthCRUD.Models.RepoParams.ClientRedirectUriEntity;
using OAuthCRUD.Repository;
using Ronika.Utility.Consts;

namespace OAuthCRUD.Manager
{
    public interface IClientRedirectUriManager
    {
        Task DeleteClientRedirectUriAsync(int id);
        Task<ClientRedirectUriObject> GetOneClientRedirectUriAsync(int id);
        Task<List<ClientRedirectUriObject>> GetAllClientRedirectUrisAsync();
        Task<ClientRedirectUriObject> UpdateClientRedirectUriAsync(UpdateClientRedirectUriApiModel updateClientRedirectUriApiModel, int id);
        Task<ClientRedirectUriObject> CreateClientRedirectUriAsync(CreateClientRedirectUriApiModel createClientRedirectUriApiModel);

    }
    public class ClientRedirectUriManager : IClientRedirectUriManager
    {
        
        
        private readonly IClientRedirectUriRepository _clientRedirectUriRepository;
        
        public ClientRedirectUriManager( IClientRedirectUriRepository clientRedirectUriRepository)
        {
            _clientRedirectUriRepository = clientRedirectUriRepository;
        }
        
         public async Task<ClientRedirectUriObject> CreateClientRedirectUriAsync(CreateClientRedirectUriApiModel createClientRedirectUriApiModel)
        {
            

            var createClientRedirectUriRepoParam = new CreateClientRedirectUriRepoParam(createClientRedirectUriApiModel);

            
            
            var createClientRedirectUriRepoResult=await _clientRedirectUriRepository.CreateClientRedirectUriAsync(createClientRedirectUriRepoParam);
            if (createClientRedirectUriRepoResult == null)
            {
                throw new RonikaStructureException("createClientRedirectUriRepoResult Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 54"
                    }
                };               

            }
            
            return new ClientRedirectUriObject(createClientRedirectUriRepoResult);
            
        }
         
        public async Task<ClientRedirectUriObject> UpdateClientRedirectUriAsync(UpdateClientRedirectUriApiModel updateClientRedirectUriApiModel,int id)
        {
            
            if (id.ToString() == null)
            {
                throw new RonikaStructureException(" Id IS Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 74"
                    }
                };
            }
            

            var updateClientRedirectUriRepoParam = new UpdateClientRedirectUriRepoParam(updateClientRedirectUriApiModel);
            
            var updateClientRedirectUriRepoResult = await _clientRedirectUriRepository.UpdateClientRedirectUriAsync(updateClientRedirectUriRepoParam,id);

            if (updateClientRedirectUriRepoResult == null)
            {
                throw new RonikaStructureException("updateClientRedirectUriRepoResult Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 91"
                    }
                };               

            }
            
            return new ClientRedirectUriObject(updateClientRedirectUriRepoResult);
        }
        
        
        public async Task<List<ClientRedirectUriObject>> GetAllClientRedirectUrisAsync()
        {
            
            var createClientRedirectUriRepoResults = await _clientRedirectUriRepository.GetAllClientRedirectUrisAsync();

            if (createClientRedirectUriRepoResults==null)
            {
                throw new RonikaStructureException("createClientRedirectUriRepoResults Is Null")
                {
                    ExceptionDetails = new ExceptionDetails
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 113"
                    }
                };                  
            }
            
            
            var redirectUriObjects=new List<ClientRedirectUriObject>();
                
            foreach (var createClientRedirectUriRepoResult in createClientRedirectUriRepoResults)
            {
                redirectUriObjects.Add(new ClientRedirectUriObject(createClientRedirectUriRepoResult));
            }

            return redirectUriObjects;
            
        }
        
        
        public async Task<ClientRedirectUriObject> GetOneClientRedirectUriAsync(int id)
        {
            
            if (id.ToString() == null)
            {
                throw new RonikaStructureException(" Id IS Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 141"
                    }
                };
            }

            
            var clientRedirectUriRepoResult = await _clientRedirectUriRepository.GetOneClientRedirectUriAsync(id);

            if (clientRedirectUriRepoResult == null)
            {
                
                throw new RonikaStructureException("clientRedirectUriRepoResult Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 157"
                    }
                };
            }
            
            return new ClientRedirectUriObject(clientRedirectUriRepoResult);

        }
        
        
        public async Task DeleteClientRedirectUriAsync(int id)
        {
            
            if (id.ToString() == null)
            {
                throw new RonikaStructureException(" Id IS Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 177"
                    }
                };
            }

            var clientRedirectUriRepoResult = await _clientRedirectUriRepository.GetOneClientRedirectUriAsync(id);

            if (clientRedirectUriRepoResult == null)
            {
                
                throw new RonikaStructureException("clientRedirectUriRepoResult Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 192"
                    }
                };
            }
            
            await _clientRedirectUriRepository.DeleteClientRedirectUriAsync(id);

        }
    }
}