using System;
using System.Collections.Generic;
using System.Reflection;
using System.Threading.Tasks;
using Exceptions;
using OAuthCRUD.Models.ApiModels.ClientGrantTypeApiModels;
using OAuthCRUD.Models.Entities.ClientGrantTypeEntity;
using OAuthCRUD.Models.RepoParams.ClientGrantType;
using Microsoft.Extensions.Logging;
using OAuthCRUD.Models.ApiModels.ClientPostLogoutRedirectUriApiModuels;
using OAuthCRUD.Models.Entities.ClientPostLogoutRedirectUriEntity;
using OAuthCRUD.Models.RepoParams.ClientPostLogoutRedirectUri;
using OAuthCRUD.Repository;
using Ronika.Utility.Consts;

namespace OAuthCRUD.Manager
{
    public interface IClientPostLogoutRedirectUriManager
    {
        Task<ClientPostLogoutRedirectUriObject> CreateClientPostLogoutRedirectUriAsync(
            CreateClientPostLogoutRedirectUriApiModel createClientPostLogoutRedirectUriApiModel);

        Task<ClientPostLogoutRedirectUriObject> UpdateClientPostLogoutRedirectUriAsync(
            UpdateClientPostLogoutRedirectUriApiModel updateClientPostLogoutRedirectUriApiModel, int id);

        Task<List<ClientPostLogoutRedirectUriObject>> GetAllClientPostLogoutRedirectUrisAsync();
        Task<ClientPostLogoutRedirectUriObject> GetOneClientPostLogoutRedirectUriAsync(int id);
        Task DeleteClientPostLogoutRedirectUriAsync(int id);

    }
    public class ClientPostLogoutRedirectUriManager : IClientPostLogoutRedirectUriManager
    {
        
        private readonly IClientPostLogoutRedirectUriRepository _clientPostLogoutRedirectUriRepository;
        
        public ClientPostLogoutRedirectUriManager( IClientPostLogoutRedirectUriRepository clientPostLogoutRedirectUriRepository)
        {
            _clientPostLogoutRedirectUriRepository = clientPostLogoutRedirectUriRepository;
        }
        
        public async Task<ClientPostLogoutRedirectUriObject> CreateClientPostLogoutRedirectUriAsync(CreateClientPostLogoutRedirectUriApiModel createClientPostLogoutRedirectUriApiModel)
        {
            

            var createClientPostLogoutRedirectUriRepoParam = new CreateClientPostLogoutRedirectUriRepoParam(createClientPostLogoutRedirectUriApiModel);

            
            var createClientPostLogoutRedirectUriRepoResult=await _clientPostLogoutRedirectUriRepository.CreateClientPostLogoutRedirectUriAsync(createClientPostLogoutRedirectUriRepoParam);

            if (createClientPostLogoutRedirectUriRepoResult == null)
            {
                throw new RonikaStructureException("createClientPostLogoutRedirectUriRepoResult Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 57"
                    }
                };               

            }
            
            return new ClientPostLogoutRedirectUriObject(createClientPostLogoutRedirectUriRepoResult);
            
        }
        public async Task<ClientPostLogoutRedirectUriObject> UpdateClientPostLogoutRedirectUriAsync(UpdateClientPostLogoutRedirectUriApiModel updateClientPostLogoutRedirectUriApiModel,int id)
        {
            
            if (id.ToString() == null)
            {
                throw new RonikaStructureException(" Id IS Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 76"
                    }
                };
            }
            
            

            var updateClientPostLogoutRedirectUriRepoParam = new UpdateClientPostLogoutRedirectUriRepoParam(updateClientPostLogoutRedirectUriApiModel);
            
            
            var createClientPostLogoutRedirectUriRepoResult =await _clientPostLogoutRedirectUriRepository.UpdateClientPostLogoutRedirectUriAsync(updateClientPostLogoutRedirectUriRepoParam,id);

            if (createClientPostLogoutRedirectUriRepoResult == null)
            {
                throw new RonikaStructureException("updateClientRedirectUriRepoResult Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 95"
                    }
                };               

            }
            
            return new ClientPostLogoutRedirectUriObject(createClientPostLogoutRedirectUriRepoResult);
            
        }
        public async Task<List<ClientPostLogoutRedirectUriObject>> GetAllClientPostLogoutRedirectUrisAsync()
        {
            
            var clientPostLogoutRedirectUriRepoResults = await _clientPostLogoutRedirectUriRepository.GetAllClientPostLogoutRedirectUrisAsync();

            if (clientPostLogoutRedirectUriRepoResults==null)
            {
                throw new RonikaStructureException("clientPostLogoutRedirectUriRepoResults Is Null")
                {
                    ExceptionDetails = new ExceptionDetails
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 116"
                    }
                };                  
            }
            
            var clientPostLogoutRedirectUriObjects=new List<ClientPostLogoutRedirectUriObject>();
                
            foreach (var clientPostLogoutRedirectUriRepoResult in clientPostLogoutRedirectUriRepoResults)
            {
                clientPostLogoutRedirectUriObjects.Add(new ClientPostLogoutRedirectUriObject(clientPostLogoutRedirectUriRepoResult));
            }

            return clientPostLogoutRedirectUriObjects;
            
        }
        
        
        public async Task<ClientPostLogoutRedirectUriObject> GetOneClientPostLogoutRedirectUriAsync(int id)
        {
            
            if (id.ToString() == null)
            {
                throw new RonikaStructureException(" Id IS Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 143"
                    }
                };
            }

            var clientPostLogoutRedirectUriRepoResult = await _clientPostLogoutRedirectUriRepository.GetOneClientPostLogoutRedirectUriAsync(id);

            if (clientPostLogoutRedirectUriRepoResult == null)
            {
                
                throw new RonikaStructureException("clientPostLogoutRedirectUriRepoResult Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 158"
                    }
                };
            }
            
            return new ClientPostLogoutRedirectUriObject(clientPostLogoutRedirectUriRepoResult);

            
        }
        public async Task DeleteClientPostLogoutRedirectUriAsync(int id)
        {
            
            if (id.ToString() == null)
            {
                throw new RonikaStructureException(" Id IS Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 177"
                    }
                };
            }
            
            var clientPostLogoutRedirectUriRepoResult = await _clientPostLogoutRedirectUriRepository.GetOneClientPostLogoutRedirectUriAsync(id);

            if (clientPostLogoutRedirectUriRepoResult == null)
            {
                
                throw new RonikaStructureException("clientPostLogoutRedirectUriRepoResult Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 192"
                    }
                };
            }

            await _clientPostLogoutRedirectUriRepository.DeleteClientPostLogoutRedirectUriAsync(id);

            
        }
    }
}