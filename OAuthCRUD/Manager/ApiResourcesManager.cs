using System;
using System.Collections.Generic;
using System.Reflection;
using System.Threading.Tasks;
using Exceptions;
using Microsoft.Extensions.Logging;
using OAuthCRUD.Models.ApiModels.ApiResourcesModels;
using OAuthCRUD.Models.Entities.ApiResourcesEntity;
using OAuthCRUD.Models.RepoParams.ApiResources;
using OAuthCRUD.Repository;
using Ronika.Utility.Consts;

namespace OAuthCRUD.Manager
{
    public interface IApiResourcesManager
    {
        Task<ApiResourcesObject> CreateApiResourcesAsync(CreateApiResourcesApiModel createApiResourcesApiModel);
        Task<ApiResourcesObject> UpdateApiResourcesAsync(UpdateApiResourcesApiModel updateApiResourcesApiModel, int id);
        Task<List<ApiResourcesObject>> GetAllApiResourcesAsync();
        Task<ApiResourcesObject> GetOneApiResourcesAsync(int id);
        Task DeleteApiResourcesAsync(int id);
    }
    public class ApiResourcesManager : IApiResourcesManager
    {
        
        private readonly ILogger<ApiResourcesManager> _logger;
        
        private readonly IApiResourcesRepository _apiResourcesRepository;
        
        public ApiResourcesManager(ILogger<ApiResourcesManager> logger, IApiResourcesRepository apiResourcesRepository)
        {
            _logger = logger;
            _apiResourcesRepository = apiResourcesRepository;
        }
        
        public async Task<ApiResourcesObject> CreateApiResourcesAsync(CreateApiResourcesApiModel createApiResourcesApiModel)
        {
            

            var createApiResourcesRepoParam = new CreateApiResourcesRepoParam(createApiResourcesApiModel);

            var createdApiResourcesRepoResult = await _apiResourcesRepository.CreateApiResourcesAsync(createApiResourcesRepoParam);

            if (createdApiResourcesRepoResult == null)
            {
                throw new RonikaStructureException("createdApiResourcesRepoResult Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 51"
                    }
                };               

            }
            
            return new ApiResourcesObject(createdApiResourcesRepoResult);

        }
        public async Task<ApiResourcesObject> UpdateApiResourcesAsync(UpdateApiResourcesApiModel updateApiResourcesApiModel,int id)
        {
            
            if (id.ToString() == null)
            {
                throw new RonikaStructureException(" Id IS Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 70"
                    }
                };

            }
            

            var updateApiResourcesRepoParam = new UpdateApiResourcesRepoParam(updateApiResourcesApiModel);
            
            
            var updatedApiResourcesRepoResult = await _apiResourcesRepository.UpdateApiResourcesAsync(updateApiResourcesRepoParam,id);

            if (updatedApiResourcesRepoResult == null)
            {
                throw new RonikaStructureException("updatedApiResourcesRepoResult Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 89"
                    }
                };               

            }
            
            return new ApiResourcesObject(updatedApiResourcesRepoResult);

            
        }
        public async Task<List<ApiResourcesObject>> GetAllApiResourcesAsync()
        {
            
            var createApiResourcesRepoResults = await _apiResourcesRepository.GetAllApiResourcesAsync();

            if (createApiResourcesRepoResults==null)
            {
                throw new RonikaStructureException("createApiResourcesRepoResults Is Null")
                {
                    ExceptionDetails = new ExceptionDetails
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 111"
                    }
                };                  
            }
            
            var apiResourcesObjects=new List<ApiResourcesObject>();
                
            foreach (var createApiResourcesRepoResult in createApiResourcesRepoResults)
            {
                apiResourcesObjects.Add(new ApiResourcesObject(createApiResourcesRepoResult));
            }

            return apiResourcesObjects;

            
        }
        public async Task<ApiResourcesObject> GetOneApiResourcesAsync(int id)
        {
            
            if (id.ToString() == null)
            {
                throw new RonikaStructureException("id Is Null In Manager ")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 137"
                         
                    }
                };
            }

            var apiResourcesRepoResult = await _apiResourcesRepository.GetOneApiResourcesAsync(id);

            if (apiResourcesRepoResult == null)
            {
                
               throw new RonikaStructureException("apiResourcesRepoResult Is Null")
               {
                   ExceptionDetails = new ExceptionDetails()
                   {
                       Message = RonikaErrorMessages.Public.StructureError.Message,
                       OccurAddress = MethodBase.GetCurrentMethod().Name + ": 153"
                   }
               };
            }
            
            return new ApiResourcesObject(apiResourcesRepoResult);

            
        }
        public async Task DeleteApiResourcesAsync(int id)
        {
            
            if (id.ToString() == null)
            {
                throw new RonikaStructureException("Id IS Null  ")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message =  RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 172"
                    }
                };
            }

            var apiResourcesRepoResult = await _apiResourcesRepository.GetOneApiResourcesAsync(id);
            

            if (apiResourcesRepoResult == null)
            {
                
                throw new RonikaStructureException("apiResourcesRepoResult Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 188"
                    }
                };
            }
            
            await _apiResourcesRepository.DeleteApiResourcesAsync(id);
            
        }
        
    }
}