using System;
using System.Collections.Generic;
using System.Reflection;
using System.Threading.Tasks;
using Exceptions;
using Microsoft.Extensions.Logging;
using OAuthCRUD.Models.ApiModels.ClientApiModels;
using OAuthCRUD.Models.Entities.ClientEntity;
using OAuthCRUD.Models.RepoParams.Client;
using OAuthCRUD.Repository;
using Ronika.Utility.Consts;

namespace OAuthCRUD.Manager
{
    public interface IClientManager
    {
        Task<ClientObject> CreateClientAsync(CreateClientApiModel createClientApiModel);
        Task<List<ClientObject>> GetAllClientsAsync();
        Task<ClientObject> GetOneClientAsync(int id);
        Task DeleteClientAsync(int id);
        Task<ClientObject> UpdateClientAsync(UpdateClientApiModel clientModel, int id);
    }
    public class ClientManager : IClientManager
    {
        
        
        private readonly IClientRepository _clientRepository;

        public ClientManager( IClientRepository clientRepository)
        {
            
            _clientRepository = clientRepository;
            
        }

        public async Task<ClientObject> CreateClientAsync(CreateClientApiModel createClientApiModel)
        {
            

            var createClientRepoParam = new CreateClientRepoParam(createClientApiModel);

            var createClientRepoResult=await _clientRepository.CreateClientAsync(createClientRepoParam);

            if (createClientRepoResult == null)
            {
                throw new RonikaStructureException("createClientRepoResult Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 51"
                    }
                };               

            }
            
            return new ClientObject(createClientRepoResult);
            
        }
        
        public async Task<ClientObject> UpdateClientAsync(UpdateClientApiModel clientModel,int id)
        {
            
            if (id.ToString() == null)
            {
                throw new RonikaStructureException(" Id IS Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 71"
                    }
                };
            }
            

            var updateClientRepoParam = new UpdateClientRepoParam(clientModel);
            
            var createClientRepoResult=await _clientRepository.UpdateClientAsync(updateClientRepoParam,id);

            if (createClientRepoResult == null)
            {
                throw new RonikaStructureException("createClientRepoResult Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 88"
                    }
                };               

            }
            
            return new ClientObject(createClientRepoResult);
            
        }

        public async Task<List<ClientObject>> GetAllClientsAsync()
        {
            
            var clientRepoResults = await _clientRepository.GetAllClientsAsync();

            if (clientRepoResults==null)
            {
                throw new RonikaStructureException("clientRepoResults Is Null")
                {
                    ExceptionDetails = new ExceptionDetails
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 110"
                    }
                };                  
            }
            
            var allClientObjects=new List<ClientObject>();
                
            foreach (var clientRepoResult in clientRepoResults)
            {
                allClientObjects.Add(new ClientObject(clientRepoResult));
            }

            return allClientObjects;
        }

        public async Task<ClientObject> GetOneClientAsync(int id)
        {
            
            if (id.ToString() == null)
            {
                throw new RonikaStructureException(" Id IS Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 135"
                    }
                };
                
            }

            var clientRepoResult = await _clientRepository.GetOneClientAsync(id);
            
            if (clientRepoResult == null)
            {
                
                throw new RonikaStructureException("clientRepoResult Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 151"
                    }
                };
            }
            
            return new ClientObject(clientRepoResult);
            
        }
        
        public async Task DeleteClientAsync(int id)
        {
            
            if (id.ToString() == null)
            {
                throw new RonikaStructureException(" Id IS Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 170"
                    }
                };
                
            }

            var clientRepoResult = await _clientRepository.GetOneClientAsync(id);
            
            if (clientRepoResult == null)
            {
                
                throw new RonikaStructureException("clientRepoResult Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 186"
                    }
                };
            }
            
            await _clientRepository.DeleteClientAsync(id);

        }
    }
}