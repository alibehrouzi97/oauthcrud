using System;
using System.Collections.Generic;
using System.Reflection;
using System.Threading.Tasks;
using Exceptions;
using OAuthCRUD.Models.Entities.ClientRedirectUriEntity;
using OAuthCRUD.Models.RepoParams.ClientRedirectUriEntity;
using OAuthCRUD.Models.RepoResults.ClientRedirectUri;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using OAuthCRUD.Data;
using OAuthCRUD.Models.Entities.ClientScopeEntity;
using OAuthCRUD.Models.RepoParams.ClientScopeEntity;
using OAuthCRUD.Models.RepoResults.ClientScope;
using Ronika.Utility.Consts;

namespace OAuthCRUD.Repository
{
    public interface IClientScopeRepository
    {
        Task DeleteClientScopeAsync(int id);
        Task<CreateClientScopeRepoResult> GetOneClientScopeAsync(int id);
        Task<List<CreateClientScopeRepoResult>> GetAllClientScopesAsync();

        Task<CreateClientScopeRepoResult> UpdateClientScopeAsync(
            UpdateClientScopeRepoParam updateClientScopeRepoParam, int id);

        Task<CreateClientScopeRepoResult> CreateClientScopeAsync(
            CreateClientScopeRepoParam createClientScopeRepoParam);
        
    }
    public class ClientScopeRepository : IClientScopeRepository
    {
        
        private readonly MvcIdentityDbContext _context;
        
        public ClientScopeRepository( MvcIdentityDbContext context)
        {
            _context = context;
            context.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
        }
        
        public async Task<CreateClientScopeRepoResult> CreateClientScopeAsync(
            CreateClientScopeRepoParam createClientScopeRepoParam)
        {
            
            if (createClientScopeRepoParam == null)
            {
                throw new RonikaStructureException("createClientScopeRepoParam Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 57"
                    }
                };
            }

            var clientScope = new ClientScope
            {
                Scope = createClientScopeRepoParam.Scope,
                ClientId = createClientScopeRepoParam.ClientId,
            };

            try
            {
                await _context.ClientScopes.AddAsync(clientScope);
                await _context.SaveChangesAsync();
            
            }
            catch (Exception)
            {
                throw new RonikaServerException("Could not create ClientScope")
                {
                    ExceptionDetails = new ExceptionDetails
                    {
                        Message = RonikaErrorMessages.Public.InternalServerError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name+": 82"
                    }
                };
            }
                
            return new CreateClientScopeRepoResult(clientScope);


        }
        
        public async Task<CreateClientScopeRepoResult> UpdateClientScopeAsync(
            UpdateClientScopeRepoParam updateClientScopeRepoParam,int id)
        {
            
            if (id.ToString() == null)
            {
                throw new RonikaStructureException("Id Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 101"
                    }
                };
            }
            
            if (updateClientScopeRepoParam == null)
            {
                throw new RonikaStructureException("updateClientScopeRepoParam Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 113"
                    }
                };
            }

            ClientScope clientScope;

            try
            {
                clientScope = await _context.ClientScopes.FirstOrDefaultAsync(c => c.Id == id);

            }
            catch (Exception )
            {
                throw new RonikaServerException("Could not Get ClientScope")
                {
                    ExceptionDetails = new ExceptionDetails
                    {
                        Message = RonikaErrorMessages.Public.InternalServerError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name+": 132"
                    }
                };
            }
            
            if (clientScope == null)
            {
                
                throw new RonikaStructureException("clientScope Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 145"
                    }
                };
            }

            
            clientScope.Scope = updateClientScopeRepoParam.Scope;

            try
            {
                _context.ClientScopes.Update(clientScope);
                await _context.SaveChangesAsync();
            
            }
            catch (Exception)
            {
                throw new RonikaServerException("Could not Update ClientScope")
                {
                    ExceptionDetails = new ExceptionDetails
                    {
                        Message = RonikaErrorMessages.Public.InternalServerError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name+": 166"
                    }
                };
            }

            return new CreateClientScopeRepoResult(clientScope);

            
        }
        public async Task<List<CreateClientScopeRepoResult>> GetAllClientScopesAsync()
        {

            List<ClientScope> clientScopes;

            try
            {
                clientScopes = await _context.ClientScopes.ToListAsync();

            }
            catch (Exception )
            {
                throw new RonikaServerException("Could not Get All ClientScope")
                {
                    ExceptionDetails = new ExceptionDetails
                    {
                        Message = RonikaErrorMessages.Public.InternalServerError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name+": 193"
                    }
                };
            }
            
            if (clientScopes == null)
            {
                throw new RonikaStructureException("ClientScope Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 205"
                    }
                };
            }

            var clientScopeRepoResults = new List<CreateClientScopeRepoResult>();
            
            foreach (var clientScope in clientScopes)
            {
                clientScopeRepoResults.Add(new CreateClientScopeRepoResult(clientScope));
            }

            return clientScopeRepoResults;
            
        }
        public async Task<CreateClientScopeRepoResult> GetOneClientScopeAsync(int id)
        {
            
            if (id.ToString() == null)
            {
                throw new RonikaStructureException("id Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 230"
                    }
                };
            }

            ClientScope clientScope;

            try
            {
                clientScope = await _context.ClientScopes.FirstOrDefaultAsync(c => c.Id == id);

            }
            catch (Exception)
            {
                throw new RonikaServerException("Could not Get ClientScope")
                {
                    ExceptionDetails = new ExceptionDetails
                    {
                        Message = RonikaErrorMessages.Public.InternalServerError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name+": 249"
                    }
                };
            }

            if (clientScope == null)
            {
                throw new RonikaStructureException("ClientScope Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 261"
                    }
                };
            }
            
            return new CreateClientScopeRepoResult(clientScope);

        }
        public async Task DeleteClientScopeAsync(int id)
        {
            
            if (id.ToString()==null)
            {
                throw new RonikaStructureException("id Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 279"
                    }
                };
            }

            ClientScope scope;
            try
            {
                
                scope = await _context.ClientScopes.FirstOrDefaultAsync(clientScope => clientScope.Id == id);

            }
            
            catch (Exception)
            {
                throw new RonikaServerException("Could not Get ClientScope")
                {
                    ExceptionDetails = new ExceptionDetails
                    {
                        Message = RonikaErrorMessages.Public.InternalServerError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name+": 299"
                    }
                };
            }
            
            if (scope==null)
            {
                throw new RonikaStructureException("ClientScope Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 311"
                    }
                };
            }

            try
            {
                _context.ClientScopes.Remove(scope);
                await _context.SaveChangesAsync();
            }
            catch (Exception )
            {
                throw new RonikaServerException("Could not Delete ClientScope")
                {
                    ExceptionDetails = new ExceptionDetails
                    {
                        Message = RonikaErrorMessages.Public.InternalServerError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name+": 299"
                    }
                };
            }

            
        }
    }
}