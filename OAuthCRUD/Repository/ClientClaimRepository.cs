using System;
using System.Collections.Generic;
using System.Reflection;
using System.Threading.Tasks;
using Exceptions;
using OAuthCRUD.Models.RepoResults.Client;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using OAuthCRUD.Data;
using OAuthCRUD.Models.Entities.ClientClaimEntity;
using OAuthCRUD.Models.RepoParams.ClientClaim;
using OAuthCRUD.Models.RepoResults.ClientClaim;
using Ronika.Utility.Consts;

namespace OAuthCRUD.Repository
{
    public interface IClientClaimRepository
    {
        Task<CreateClientClaimRepoResult> GetOneClientClaimAsync(int id);

        Task<CreateClientClaimRepoResult> CreateClientClaimAsync(
            CreateClientClaimRepoParam createClientClaimRepoParam);

        Task<CreateClientClaimRepoResult> UpdateClientClaimAsync(
            UpdateClientClaimRepoParam updateClientClaimRepoParam, int id);

        Task<List<CreateClientClaimRepoResult>> GetAllClientClaimsAsync();
        Task DeleteClientClaimAsync(int id);

    }
    public class ClientClaimRepository : IClientClaimRepository
    {
        
        private readonly MvcIdentityDbContext _context;
        
        public ClientClaimRepository(MvcIdentityDbContext context)
        {
            _context = context;
            context.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
            
        }

        public async Task<CreateClientClaimRepoResult> CreateClientClaimAsync(
            CreateClientClaimRepoParam createClientClaimRepoParam)
        {
            
            if (createClientClaimRepoParam == null)
            {
                throw new RonikaStructureException("createClientClaimRepoParam Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 54"
                    }
                }; 
            }

            var clientClaim = new ClientClaim
            {
                ClientId = createClientClaimRepoParam.ClientId,
                Type = createClientClaimRepoParam.Type,
                Value = createClientClaimRepoParam.Value
            };

            try
            {
                await _context.ClientClaims.AddAsync(clientClaim);
                await _context.SaveChangesAsync();
            }
            catch (Exception )
            {
                throw new RonikaServerException("Could not create clientClaim")
                {
                    ExceptionDetails = new ExceptionDetails
                    {
                        Message = RonikaErrorMessages.Public.InternalServerError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name+": 78"
                    }
                };
            }

            
            return new CreateClientClaimRepoResult(clientClaim);

        }

        public async Task<CreateClientClaimRepoResult> UpdateClientClaimAsync(
            UpdateClientClaimRepoParam updateClientClaimRepoParam,int id)
        {
            
            if (id.ToString() == null)
            {
                throw new RonikaStructureException("Id Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 94"
                    }
                };
            }
            
            if (updateClientClaimRepoParam == null)
            {
                throw new RonikaStructureException("updateClientClaimRepoParam Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 111"
                    }
                }; 
                
            }

            ClientClaim clientClaim;

            try
            {
                clientClaim = await _context.ClientClaims.FirstOrDefaultAsync(c => c.Id == id);

            }
            catch (Exception )
            {
                throw new RonikaServerException("Could not Get clientClaim")
                {
                    ExceptionDetails = new ExceptionDetails
                    {
                        Message = RonikaErrorMessages.Public.InternalServerError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name+": 131"
                    }
                };
            }

            
            if (clientClaim == null)
            {
                
                throw new RonikaStructureException("clientClaim Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 145"
                    }
                };
                
            }

            clientClaim.Type = updateClientClaimRepoParam.Type;
            clientClaim.Value = updateClientClaimRepoParam.Value;

            try
            {
                _context.ClientClaims.Update(clientClaim);
                await _context.SaveChangesAsync();
            }
            catch (Exception )
            {
                throw new RonikaServerException("Could not Update clientClaim")
                {
                    ExceptionDetails = new ExceptionDetails
                    {
                        Message = RonikaErrorMessages.Public.InternalServerError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name+": 166"
                    }
                };
            }

            
            return new CreateClientClaimRepoResult(clientClaim);
            
        }
        public async Task<List<CreateClientClaimRepoResult>> GetAllClientClaimsAsync()
        {

            List<ClientClaim> allClientClaims;

            try
            {
                allClientClaims = await _context.ClientClaims.ToListAsync();

            }
            catch (Exception)
            {
                throw new RonikaServerException("Could not Get All clientClaims")
                {
                    ExceptionDetails = new ExceptionDetails
                    {
                        Message = RonikaErrorMessages.Public.InternalServerError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name+": 192"
                    }
                };
            }
            
            if (allClientClaims == null)
            {
                throw new RonikaStructureException("allClientClaims Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 204"
                    }
                };
                
            }

            
            var clientClaims = new List<CreateClientClaimRepoResult>();
            
            foreach (var clientClaim in allClientClaims)
            {
                clientClaims.Add(new CreateClientClaimRepoResult(clientClaim));
            }

            return clientClaims;
        }

        public async Task<CreateClientClaimRepoResult> GetOneClientClaimAsync(int id)
        {
            
            if (id.ToString() == null)
            {
                throw new RonikaStructureException("Id Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 231"
                    }
                };
                
            }

            
            ClientClaim clientClaim;

            try
            {
                clientClaim = await _context.ClientClaims.FirstOrDefaultAsync(c => c.Id == id);

            }
            catch (Exception )
            {
                throw new RonikaServerException("Could not Get clientClaim")
                {
                    ExceptionDetails = new ExceptionDetails
                    {
                        Message = RonikaErrorMessages.Public.InternalServerError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name+": 252"
                    }
                };
            }

            
            if (clientClaim == null)
            {
                
                throw new RonikaStructureException("clientClaim Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 266"
                    }
                };
                
            }
            
            return new CreateClientClaimRepoResult(clientClaim);
            
        }
        
        public async Task DeleteClientClaimAsync(int id)
        {
            
            if (id.ToString()==null)
            {
                throw new RonikaStructureException("Id Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 286"
                    }
                };
            }

            ClientClaim clientClaim;

            try
            {
                clientClaim = await _context.ClientClaims.FirstOrDefaultAsync(c => c.Id == id);

            }
            catch (Exception )
            {
                throw new RonikaServerException("Could not Get clientClaim")
                {
                    ExceptionDetails = new ExceptionDetails
                    {
                        Message = RonikaErrorMessages.Public.InternalServerError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name+": 305"
                    }
                };
            }

            
            if (clientClaim == null)
            {
                
                throw new RonikaStructureException("clientClaim Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 319"
                    }
                };
                
            }

            try
            {
                _context.ClientClaims.Remove(clientClaim);
                await _context.SaveChangesAsync();
            }
            catch (Exception )
            {
                throw new RonikaServerException("Could not Delete clientCorsOrigin")
                {
                    ExceptionDetails = new ExceptionDetails
                    {
                        Message = RonikaErrorMessages.Public.InternalServerError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name+": 337"
                    }
                };
                
            }

            
        }
    }
}