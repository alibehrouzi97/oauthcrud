using System;
using System.Collections.Generic;
using System.Reflection;
using System.Threading.Tasks;
using Exceptions;
using Microsoft.EntityFrameworkCore;
using OAuthCRUD.Data;
using OAuthCRUD.Models.Entities.ClientGrantTypeEntity;
using OAuthCRUD.Models.RepoParams.ClientGrantType;
using OAuthCRUD.Models.RepoResults.ClientGrantType;
using Ronika.Utility.Consts;

namespace OAuthCRUD.Repository
{
    public interface IClientGrantTypeRepository
    {
        Task DeleteClientGrantTypeAsync(int id);
        Task<List<CreateClientGrantTypeRepoResult>> GetAllClientGrantTypesAsync();

        Task<CreateClientGrantTypeRepoResult> UpdateClientGrantTypeAsync(
            UpdateClientGrantTypeRepoParam updateClientGrantTypeRepoParam, int id);

        Task<CreateClientGrantTypeRepoResult> GetOneClientGrantTypeAsync(int id);

        Task<CreateClientGrantTypeRepoResult> CreateClientGrantTypeAsync(
            CreateClientGrantTypeRepoParam createClientGrantTypeRepoParam);
    }
    public class ClientGrantTypeRepository : IClientGrantTypeRepository
    {
        

        
        private readonly MvcIdentityDbContext _context;
        
        public ClientGrantTypeRepository( MvcIdentityDbContext context)
        {
            _context = context;
            context.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
        }
        
        public async Task<CreateClientGrantTypeRepoResult> CreateClientGrantTypeAsync(
            CreateClientGrantTypeRepoParam createClientGrantTypeRepoParam)
        {
            
            
            if (createClientGrantTypeRepoParam == null)
            {
                throw new RonikaStructureException("createClientGrantTypeRepoParam Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 52"
                    }
                };
            }

            var clientGrantType = new ClientGrantType
            {
                GrantType = createClientGrantTypeRepoParam.GrantType,
                ClientId = createClientGrantTypeRepoParam.ClientId,
            };

            try
            {
                await _context.ClientGrantTypes.AddAsync(clientGrantType);
                await _context.SaveChangesAsync();
            }
            catch (Exception)
            {
                throw new RonikaServerException("Could not create clientGrantType")
                {
                    ExceptionDetails = new ExceptionDetails
                    {
                        Message = RonikaErrorMessages.Public.InternalServerError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name+": 75"
                    }
                };
            }

            
            return new CreateClientGrantTypeRepoResult(clientGrantType);
        }
        public async Task<CreateClientGrantTypeRepoResult> UpdateClientGrantTypeAsync(
            UpdateClientGrantTypeRepoParam updateClientGrantTypeRepoParam,int id)
        {
            
            if (id.ToString() == null)
            {
                throw new RonikaStructureException("Id Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 94"
                    }
                };
            }
            
            if (updateClientGrantTypeRepoParam == null)
            {
                throw new RonikaStructureException("updateClientGrantTypeRepoParam Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 106"
                    }
                };
                
            }

            ClientGrantType clientGrantType;

            try
            {
                clientGrantType = await _context.ClientGrantTypes.FirstOrDefaultAsync(c => c.Id == id);

            }
            catch (Exception )
            {
                throw new RonikaServerException("Could not Get clientGrantType")
                {
                    ExceptionDetails = new ExceptionDetails
                    {
                        Message = RonikaErrorMessages.Public.InternalServerError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name+": 126"
                    }
                };
            }
            
            if (clientGrantType == null)
            {
                
                throw new RonikaStructureException("clientGrantType Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 139"
                    }
                };
            }
            
            clientGrantType.GrantType = updateClientGrantTypeRepoParam.GrantType;

            try
            {
                _context.ClientGrantTypes.Update(clientGrantType);
                await _context.SaveChangesAsync();
            }
            catch (Exception )
            {
                throw new RonikaServerException("Could not Update clientGrantType")
                {
                    ExceptionDetails = new ExceptionDetails
                    {
                        Message = RonikaErrorMessages.Public.InternalServerError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name+": 158"
                    }
                };
            }

            
            return new CreateClientGrantTypeRepoResult(clientGrantType);
            
        }
        public async Task<List<CreateClientGrantTypeRepoResult>> GetAllClientGrantTypesAsync()
        {

            List<ClientGrantType> allClientGrantTypes;

            try
            {
                allClientGrantTypes = await _context.ClientGrantTypes.ToListAsync();

            }
            catch (Exception )
            {
                throw new RonikaServerException("Could not Get All ClientGrantTypes")
                {
                    ExceptionDetails = new ExceptionDetails
                    {
                        Message = RonikaErrorMessages.Public.InternalServerError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name+": 184"
                    }
                };
            }
            
            if (allClientGrantTypes == null)
            {
                throw new RonikaStructureException("allClientGrantTypes Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 196"
                    }
                };
                
            }

            var clientGrantTypeRepoResults = new List<CreateClientGrantTypeRepoResult>();
            
            foreach (var clientGrantType in allClientGrantTypes)
            {
                clientGrantTypeRepoResults.Add(new CreateClientGrantTypeRepoResult(clientGrantType));
            }

            return clientGrantTypeRepoResults;
        }
        public async Task<CreateClientGrantTypeRepoResult> GetOneClientGrantTypeAsync(int id)
        {
            
            if (id.ToString() == null)
            {
                throw new RonikaStructureException("Id Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 221"
                    }
                };
                
            }

            ClientGrantType clientGrantType;

            try
            {
                clientGrantType = await _context.ClientGrantTypes.FirstOrDefaultAsync(c => c.Id == id);

            }
            catch (Exception )
            {
                throw new RonikaServerException("Could not Get clientGrantType")
                {
                    ExceptionDetails = new ExceptionDetails
                    {
                        Message = RonikaErrorMessages.Public.InternalServerError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name+": 241"
                    }
                };
            }
            

            if (clientGrantType == null)
            {
                throw new RonikaStructureException("clientGrantType Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 254"
                    }
                };
                
                
            }
            
            return new CreateClientGrantTypeRepoResult(clientGrantType);
           
        }
        public async Task DeleteClientGrantTypeAsync(int id)
        {
            if (id.ToString()==null)
            {
                throw new RonikaStructureException("Id Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 273"
                    }
                };
                
            }

            ClientGrantType clientGrantType;

            try
            {
                clientGrantType = await _context.ClientGrantTypes.FirstOrDefaultAsync(c => c.Id == id);

            }
            catch (Exception )
            {
                throw new RonikaServerException("Could not Get clientGrantType")
                {
                    ExceptionDetails = new ExceptionDetails
                    {
                        Message = RonikaErrorMessages.Public.InternalServerError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name+": 293"
                    }
                };
            }
            

            if (clientGrantType == null)
            {
                throw new RonikaStructureException("clientGrantType Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 306"
                    }
                };
                
                
            }

            try
            {
                _context.ClientGrantTypes.Remove(clientGrantType);
                await _context.SaveChangesAsync();
            }
            catch (Exception )
            {
                throw new RonikaServerException("Could not Delete clientGrantType")
                {
                    ExceptionDetails = new ExceptionDetails
                    {
                        Message = RonikaErrorMessages.Public.InternalServerError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name+": 325"
                    }
                };
            }


        }
        
        
    }
}