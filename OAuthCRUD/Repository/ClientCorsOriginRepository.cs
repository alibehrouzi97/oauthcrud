using System;
using System.Collections.Generic;
using System.Reflection;
using System.Threading.Tasks;
using Exceptions;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using OAuthCRUD.Data;
using OAuthCRUD.Models.Entities.ClientCorsOriginEntity;
using OAuthCRUD.Models.RepoParams.ClientCorsOrigin;
using OAuthCRUD.Models.RepoResults.ClientCorsOrigin;
using Ronika.Utility.Consts;

namespace OAuthCRUD.Repository
{
    public interface IClientCorsOriginRepository
    {
        Task DeleteClientCorsOriginAsync(int id);
        Task<CreateClientCorsOriginRepoResult> GetOneClientCorsOriginAsync(int id);
        Task<List<CreateClientCorsOriginRepoResult>> GetAllClientCorsOriginsAsync();

        Task<CreateClientCorsOriginRepoResult> UpdateClientCorsOriginAsync(
            UpdateClientCorsOriginRepoParam updateClientCorsOriginRepoParam, int id);

        Task<CreateClientCorsOriginRepoResult> CreateClientCorsOriginAsync(
            CreateClientCorsOriginRepoParam createClientCorsOriginRepoParam);
    }
    public class ClientCorsOriginRepository : IClientCorsOriginRepository
    {
        
        
        private readonly MvcIdentityDbContext _context;
        
        public ClientCorsOriginRepository( MvcIdentityDbContext context)
        {
            _context = context;
            context.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
        }
        
        public async Task<CreateClientCorsOriginRepoResult> CreateClientCorsOriginAsync(
            CreateClientCorsOriginRepoParam createClientCorsOriginRepoParam)
        {
            
            if (createClientCorsOriginRepoParam == null)
            {
                throw new RonikaStructureException("createClientCorsOriginRepoParam Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 51"
                    }
                };
                
            }

            var clientCorsOrigin = new ClientCorsOrigin
            {
                Origin = createClientCorsOriginRepoParam.Origin,
                ClientId = createClientCorsOriginRepoParam.ClientId,
            };

            try
            {
                await _context.ClientCorsOrigins.AddAsync(clientCorsOrigin);
                await _context.SaveChangesAsync();
            }
            catch (Exception )
            {
                throw new RonikaServerException("Could not create clientCorsOrigin")
                {
                    ExceptionDetails = new ExceptionDetails
                    {
                        Message = RonikaErrorMessages.Public.InternalServerError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name+": 75"
                    }
                };
            }
            
            
            return new CreateClientCorsOriginRepoResult(clientCorsOrigin);
            
        }
        public async Task<CreateClientCorsOriginRepoResult> UpdateClientCorsOriginAsync(
            UpdateClientCorsOriginRepoParam updateClientCorsOriginRepoParam,int id)
        {
            
            if (id.ToString() == null)
            {
                throw new RonikaStructureException("Id Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 94"
                    }
                };
                
            }
            
            if (updateClientCorsOriginRepoParam == null)
            {
                throw new RonikaStructureException("updateClientCorsOriginRepoParam Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 108"
                    }
                };
                
            }

            ClientCorsOrigin clientCorsOrigin;

            try
            {
                clientCorsOrigin = await _context.ClientCorsOrigins.FirstOrDefaultAsync(c => c.Id == id);

            }
            catch (Exception )
            {
                throw new RonikaServerException("Could not Get clientCorsOrigin")
                {
                    ExceptionDetails = new ExceptionDetails
                    {
                        Message = RonikaErrorMessages.Public.InternalServerError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name+": 128"
                    }
                };
            }
            
            if (clientCorsOrigin == null)
            {
                
                throw new RonikaStructureException("clientCorsOrigin Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 141"
                    }
                };
            }

            clientCorsOrigin.Origin = updateClientCorsOriginRepoParam.Origin;

            try
            {
                _context.ClientCorsOrigins.Update(clientCorsOrigin);
                await _context.SaveChangesAsync();
            }
            catch (Exception )
            {
                throw new RonikaServerException("Could not Update clientCorsOrigin")
                {
                    ExceptionDetails = new ExceptionDetails
                    {
                        Message = RonikaErrorMessages.Public.InternalServerError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name+": 160"
                    }
                };
            }

            
            return new CreateClientCorsOriginRepoResult(clientCorsOrigin);
            
        }
        public async Task<List<CreateClientCorsOriginRepoResult>> GetAllClientCorsOriginsAsync()
        {
            List<ClientCorsOrigin> allClientCorsOrigins;

            try
            {
                allClientCorsOrigins = await _context.ClientCorsOrigins.ToListAsync();

            }
            catch (Exception )
            {
                throw new RonikaServerException("Could not Get All clientCorsOrigins")
                {
                    ExceptionDetails = new ExceptionDetails
                    {
                        Message = RonikaErrorMessages.Public.InternalServerError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name+": 184"
                    }
                };
            }
            
            if (allClientCorsOrigins == null)
            {
                throw new RonikaStructureException("allClientCorsOrigins Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 196"
                    }
                };
            }

            var clientCorsOrigins = new List<CreateClientCorsOriginRepoResult>();
            
            foreach (var clientCorsOrigin in allClientCorsOrigins)
            {
                clientCorsOrigins.Add(new CreateClientCorsOriginRepoResult(clientCorsOrigin));
            }

            return clientCorsOrigins;
        }
        
        public async Task<CreateClientCorsOriginRepoResult> GetOneClientCorsOriginAsync(int id)
        {
            
            if (id.ToString() == null)
            {
                throw new RonikaStructureException("Id Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 222"
                    }
                };
                
            }

            ClientCorsOrigin clientCorsOrigin;

            try
            {
                clientCorsOrigin = await _context.ClientCorsOrigins.FirstOrDefaultAsync(c => c.Id == id);

            }
            catch (Exception )
            {
                throw new RonikaServerException("Could not Get clientCorsOrigin")
                {
                    ExceptionDetails = new ExceptionDetails
                    {
                        Message = RonikaErrorMessages.Public.InternalServerError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name+": 241"
                    }
                };
            }

            if (clientCorsOrigin == null)
            {
                throw new RonikaStructureException("clientCorsOrigin Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 254"
                    }
                }; 
            }
            
            return new CreateClientCorsOriginRepoResult(clientCorsOrigin);

        }
        
        public async Task DeleteClientCorsOriginAsync(int id)
        {
            if (id.ToString()==null)
            {
                throw new RonikaStructureException("Id Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 273"
                    }
                };
            }

            ClientCorsOrigin clientCorsOrigin;

            try
            {
                clientCorsOrigin = await _context.ClientCorsOrigins.FirstOrDefaultAsync(c => c.Id == id);

            }
            catch (Exception )
            {
                throw new RonikaServerException("Could not Get clientCorsOrigin")
                {
                    ExceptionDetails = new ExceptionDetails
                    {
                        Message = RonikaErrorMessages.Public.InternalServerError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name+": 291"
                    }
                };
            }

            if (clientCorsOrigin == null)
            {
                throw new RonikaStructureException("clientCorsOrigin Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 303"
                    }
                }; 
            }

            try
            {
                _context.ClientCorsOrigins.Remove(clientCorsOrigin);
                await _context.SaveChangesAsync();
            }
            catch (Exception )
            {
                throw new RonikaServerException("Could not Delete clientCorsOrigin")
                {
                    ExceptionDetails = new ExceptionDetails
                    {
                        Message = RonikaErrorMessages.Public.InternalServerError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name+": 320"
                    }
                };
            }

        }
        
    }
}