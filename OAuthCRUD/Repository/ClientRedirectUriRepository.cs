using System;
using System.Collections.Generic;
using System.Reflection;
using System.Threading.Tasks;
using Exceptions;
using OAuthCRUD.Models.Entities.ClientPostLogoutRedirectUriEntity;
using OAuthCRUD.Models.RepoParams.ClientPostLogoutRedirectUri;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using OAuthCRUD.Data;
using OAuthCRUD.Models.Entities.ClientRedirectUriEntity;
using OAuthCRUD.Models.RepoParams.ClientRedirectUriEntity;
using OAuthCRUD.Models.RepoResults.ClientRedirectUri;
using Ronika.Utility.Consts;

namespace OAuthCRUD.Repository
{
    public interface IClientRedirectUriRepository
    {
        Task<CreateClientRedirectUriRepoResult> CreateClientRedirectUriAsync(
            CreateClientRedirectUriRepoParam createClientRedirectUriRepoParam);

        Task<CreateClientRedirectUriRepoResult> UpdateClientRedirectUriAsync(
            UpdateClientRedirectUriRepoParam updateClientRedirectUriRepoParam, int id);

        Task<List<CreateClientRedirectUriRepoResult>> GetAllClientRedirectUrisAsync();
        Task<CreateClientRedirectUriRepoResult> GetOneClientRedirectUriAsync(int id);
        Task DeleteClientRedirectUriAsync(int id);
    }
    public class ClientRedirectUriRepository : IClientRedirectUriRepository
    {
        
        
        private readonly MvcIdentityDbContext _context;
        
        public ClientRedirectUriRepository( MvcIdentityDbContext context)
        {
            _context = context;
            context.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
        }
        
        public async Task<CreateClientRedirectUriRepoResult> CreateClientRedirectUriAsync(
            CreateClientRedirectUriRepoParam createClientRedirectUriRepoParam)
        {
            
            if (createClientRedirectUriRepoParam == null)
            {
                throw new RonikaStructureException("createClientRedirectUriRepoParam Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 53"
                    }
                };
            }

            var clientRedirectUri = new ClientRedirectUri
            {
                RedirectUri = createClientRedirectUriRepoParam.RedirectUri,
                ClientId = createClientRedirectUriRepoParam.ClientId,
            };

            try
            {
                await _context.ClientRedirectUris.AddAsync(clientRedirectUri);
                await _context.SaveChangesAsync();
            }
            catch (Exception)
            {
                throw new RonikaServerException("Could not create clientRedirectUri")
                {
                    ExceptionDetails = new ExceptionDetails
                    {
                        Message = RonikaErrorMessages.Public.InternalServerError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name+": 76"
                    }
                };
            }

            return new CreateClientRedirectUriRepoResult(clientRedirectUri);
        }
        
        public async Task<CreateClientRedirectUriRepoResult> UpdateClientRedirectUriAsync(
            UpdateClientRedirectUriRepoParam updateClientRedirectUriRepoParam,int id)
        {
            
            if (id.ToString() == null)
            {
                throw new RonikaStructureException("Id Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 95"
                    }
                };
            }
            
            if (updateClientRedirectUriRepoParam == null)
            {
                throw new RonikaStructureException("updateClientRedirectUriRepoParam Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 107"
                    }
                };
            }

            
            ClientRedirectUri clientRedirectUri;

            try
            {
                clientRedirectUri = await _context.ClientRedirectUris.FirstOrDefaultAsync(c => c.Id == id);

            }
            catch (Exception)
            {
                throw new RonikaServerException("Could not Get ClientScope")
                {
                    ExceptionDetails = new ExceptionDetails
                    {
                        Message = RonikaErrorMessages.Public.InternalServerError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name+": 127"
                    }
                };
            }
            
            if (clientRedirectUri == null)
            {
                
                throw new RonikaStructureException("clientRedirectUri Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 148"
                    }
                };
            }



            clientRedirectUri.RedirectUri = updateClientRedirectUriRepoParam.RedirectUri;

            try
            {
                _context.ClientRedirectUris.Update(clientRedirectUri);
                await _context.SaveChangesAsync();
            }
            catch (Exception)
            {
                throw new RonikaServerException("Could not Update clientRedirectUri")
                {
                    ExceptionDetails = new ExceptionDetails
                    {
                        Message = RonikaErrorMessages.Public.InternalServerError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name+": 161"
                    }
                };
            }

            
            return new CreateClientRedirectUriRepoResult(clientRedirectUri);
            
        }
        public async Task<List<CreateClientRedirectUriRepoResult>> GetAllClientRedirectUrisAsync()
        {

            List<ClientRedirectUri> clientRedirectUris;

            try
            {
                clientRedirectUris = await _context.ClientRedirectUris.ToListAsync();

            }
            catch (Exception )
            {
                throw new RonikaServerException("Could not Get All clientRedirectUris")
                {
                    ExceptionDetails = new ExceptionDetails
                    {
                        Message = RonikaErrorMessages.Public.InternalServerError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name+": 187"
                    }
                };
            }
            
            if (clientRedirectUris == null)
            {
                throw new RonikaStructureException("clientRedirectUris Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 199"
                    }
                };
            }

            var clientRedirectUriRepoResults = new List<CreateClientRedirectUriRepoResult>();
            
            foreach (var clientRedirectUri in clientRedirectUris)
            {
                clientRedirectUriRepoResults.Add(new CreateClientRedirectUriRepoResult(clientRedirectUri));
            }

            return clientRedirectUriRepoResults;
        }
        public async Task<CreateClientRedirectUriRepoResult> GetOneClientRedirectUriAsync(int id)
        {
            
            if (id.ToString() == null)
            {
                throw new RonikaStructureException("id Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 223"
                    }
                };
                
            }


            ClientRedirectUri clientRedirectUri;

            try
            {
                clientRedirectUri = await _context.ClientRedirectUris.FirstOrDefaultAsync(c => c.Id == id);

            }
            catch (Exception)
            {
                throw new RonikaServerException("Could not Get clientRedirectUri")
                {
                    ExceptionDetails = new ExceptionDetails
                    {
                        Message = RonikaErrorMessages.Public.InternalServerError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name+": 244"
                    }
                };
            }

            if (clientRedirectUri == null)
            {
                throw new RonikaStructureException("clientRedirectUri Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 256"
                    }
                };
            }
            
            return new CreateClientRedirectUriRepoResult(clientRedirectUri);

        }
        public async Task DeleteClientRedirectUriAsync(int id)
        {
            if (id.ToString()==null)
            {
                throw new RonikaStructureException("id Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 273"
                    }
                };
            }

            ClientRedirectUri clientRedirectUri;

            try
            {
                clientRedirectUri = await _context.ClientRedirectUris.FirstOrDefaultAsync(c => c.Id == id);

            }
            catch (Exception)
            {
                throw new RonikaServerException("Could not Get clientRedirectUri")
                {
                    ExceptionDetails = new ExceptionDetails
                    {
                        Message = RonikaErrorMessages.Public.InternalServerError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name+": 292"
                    }
                };
            }

            if (clientRedirectUri == null)
            {
                throw new RonikaStructureException("clientRedirectUri Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 304"
                    }
                };
            }

            try
            {
                _context.ClientRedirectUris.Remove(clientRedirectUri);
                await _context.SaveChangesAsync();
            }
            catch (Exception)
            {
                throw new RonikaServerException("Could not Delete clientRedirectUri")
                {
                    ExceptionDetails = new ExceptionDetails
                    {
                        Message = RonikaErrorMessages.Public.InternalServerError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name+": 321"
                    }
                };
            }

        }
    }
}