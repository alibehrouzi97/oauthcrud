using System;
using System.Collections.Generic;
using System.Reflection;
using System.Threading.Tasks;
using Exceptions;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using OAuthCRUD.Data;
using OAuthCRUD.Models.Entities.ApiResourcesEntity;
using OAuthCRUD.Models.RepoParams.ApiResources;
using OAuthCRUD.Models.RepoResults.ApiResources;
using Ronika.Utility.Consts;

namespace OAuthCRUD.Repository
{
    public interface IApiResourcesRepository
    {
        Task<CreateApiResourcesRepoResult> CreateApiResourcesAsync(
            CreateApiResourcesRepoParam createApiResourcesRepoParam);

        Task<CreateApiResourcesRepoResult> UpdateApiResourcesAsync(
            UpdateApiResourcesRepoParam updateApiResourcesRepoParam, int id);

        Task<List<CreateApiResourcesRepoResult>> GetAllApiResourcesAsync();
        Task<CreateApiResourcesRepoResult> GetOneApiResourcesAsync(int id);
        Task DeleteApiResourcesAsync(int id);
    }
    public class ApiResourcesRepository : IApiResourcesRepository
    {
        
        
        private readonly MvcIdentityDbContext _context;
        
        public ApiResourcesRepository( MvcIdentityDbContext context)
        {
            _context = context;
            context.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
        }
        
        public async Task<CreateApiResourcesRepoResult> CreateApiResourcesAsync(
            CreateApiResourcesRepoParam createApiResourcesRepoParam)
        {
            
            if (createApiResourcesRepoParam == null)
            {
                throw new RonikaStructureException("createApiResourcesRepoParam Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 53"
                    }
                };
                
            }

            var apiResources = new ApiResources
            {
                Name = createApiResourcesRepoParam.Name,
                Description = createApiResourcesRepoParam.Description,
                DisplayName = createApiResourcesRepoParam.DisplayName,
                Created = DateTime.Now
            };

            try
            {
                await _context.ApiResources.AddAsync(apiResources);
                await _context.SaveChangesAsync();
            }
            catch (Exception )
            {
                throw new RonikaServerException("Could not create ApiResource")
                {
                    ExceptionDetails = new ExceptionDetails
                    {
                        Message = RonikaErrorMessages.Public.InternalServerError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name+": 80"
                    }
                };
                
            }
            
            return new CreateApiResourcesRepoResult(apiResources);


            
            
        }
        public async Task<CreateApiResourcesRepoResult> UpdateApiResourcesAsync(
            UpdateApiResourcesRepoParam updateApiResourcesRepoParam,int id)
        {
            
            if (id.ToString() == null)
            {
                throw new RonikaStructureException("Id Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 100"
                    }
                };
            }
            
            if (updateApiResourcesRepoParam == null)
            {
                throw new RonikaStructureException("updateApiResourcesRepoParam Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 112"
                    }
                };
            }

            ApiResources apiResources;
            try
            {
                apiResources = await _context.ApiResources.FirstOrDefaultAsync(c => c.Id == id);
            

            }
            catch (Exception )
            {
                throw new RonikaServerException("Could not Get ApiResource")
                {
                    ExceptionDetails = new ExceptionDetails
                    {
                        Message = RonikaErrorMessages.Public.InternalServerError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name+": 146"
                    }
                };
            }
            
            if (apiResources == null)
            {
                throw new RonikaStructureException("apiResources Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 128"
                    }
                };
            }

            apiResources.Description = updateApiResourcesRepoParam.Description;
            apiResources.Name = updateApiResourcesRepoParam.Name;
            apiResources.DisplayName = updateApiResourcesRepoParam.DisplayName;
            apiResources.Update=DateTime.Now;
            
            try
            {
                _context.ApiResources.Update(apiResources);
                await _context.SaveChangesAsync();

            }
            catch (Exception )
            {
                throw new RonikaServerException("Could not Update ApiResource")
                {
                    ExceptionDetails = new ExceptionDetails
                    {
                        Message = RonikaErrorMessages.Public.InternalServerError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name+": 166"
                    }
                };
            }
            
            
            return new CreateApiResourcesRepoResult(apiResources);
           
        }
        public async Task<List<CreateApiResourcesRepoResult>> GetAllApiResourcesAsync()
        {

            List<ApiResources> allApiResources;
            
            try
            {
                allApiResources = await _context.ApiResources.ToListAsync();

            }
            catch (Exception )
            {
                throw new RonikaServerException("Could not Get All ApiResource")
                {
                    ExceptionDetails = new ExceptionDetails
                    {
                        Message = RonikaErrorMessages.Public.InternalServerError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name+": 188"
                    }
                };
            }
            
                        
            
            if (allApiResources == null)
            {
                throw new RonikaStructureException("allApiResources Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 202"
                    }
                };
            }
                
            var apiResourcesRepoResults = new List<CreateApiResourcesRepoResult>();
            
            foreach (var apiResources in allApiResources)
            {
                apiResourcesRepoResults.Add(new CreateApiResourcesRepoResult(apiResources));
            }

            return apiResourcesRepoResults;
            
        }
        
        public async Task<CreateApiResourcesRepoResult> GetOneApiResourcesAsync(int id)
        {
            
            if (id.ToString() == null)
            {
                throw new RonikaStructureException("id Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 230"
                    }
                };
                
            }

            ApiResources apiResources;
            
            try
            {
                apiResources = await _context.ApiResources.FirstOrDefaultAsync(c => c.Id == id);


            }
            catch (Exception )
            {
                throw new RonikaServerException("Could not Get ApiResource")
                {
                    ExceptionDetails = new ExceptionDetails
                    {
                        Message = RonikaErrorMessages.Public.InternalServerError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name+": 253"
                    }
                };
            }
            
            if (apiResources == null)
            {
                throw new RonikaStructureException("apiResources Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 247"
                    }
                };
                
            }
            
            return new CreateApiResourcesRepoResult(apiResources);

        }
        
        public async Task DeleteApiResourcesAsync(int id)
        {
            
            if (id.ToString()==null)
            {
                throw new RonikaStructureException("id Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 285"
                    }
                };
            }

            ApiResources apiResource;
            try
            {
                apiResource =
                    await _context.ApiResources.FirstOrDefaultAsync(apiResources => apiResources.Id == id);
                
            }
            
            catch (Exception)
            {
                throw new RonikaServerException("Could not Get ApiResource")
                {
                    ExceptionDetails = new ExceptionDetails
                    {
                        Message = RonikaErrorMessages.Public.InternalServerError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name+": 307"
                    }
                };
                
            }
            
            if (apiResource==null)
            {
                throw new RonikaStructureException("apiResource Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 320"
                    }
                };
                    
            }

            try
            {
                _context.ApiResources.Remove(apiResource);
                await _context.SaveChangesAsync();
            }
            catch (Exception)
            {
                throw new RonikaServerException("Could not Delete ApiResource")
                {
                    ExceptionDetails = new ExceptionDetails
                    {
                        Message = RonikaErrorMessages.Public.InternalServerError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name+": 338"
                    }
                };
            }

        }
    }
}