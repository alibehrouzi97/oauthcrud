using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Exceptions;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using OAuthCRUD.Data;
using OAuthCRUD.Models.Entities.ClientClaimEntity;
using OAuthCRUD.Models.Entities.ClientCorsOriginEntity;
using OAuthCRUD.Models.Entities.ClientEntity;
using OAuthCRUD.Models.Entities.ClientGrantTypeEntity;
using OAuthCRUD.Models.Entities.ClientPostLogoutRedirectUriEntity;
using OAuthCRUD.Models.Entities.ClientRedirectUriEntity;
using OAuthCRUD.Models.Entities.ClientScopeEntity;
using OAuthCRUD.Models.RepoParams.Client;
using OAuthCRUD.Models.RepoResults.Client;
using Ronika.Utility.Consts;

namespace OAuthCRUD.Repository
{
    public interface IClientRepository
    {
        Task<CreateClientRepoResult> CreateClientAsync(CreateClientRepoParam createClientRepoParam);
        Task<List<CreateClientRepoResult>> GetAllClientsAsync();
        Task<CreateClientRepoResult> GetOneClientAsync(int id);
        Task DeleteClientAsync(int id);
        Task<CreateClientRepoResult> UpdateClientAsync(UpdateClientRepoParam updateClientRepoParam, int id);
    }
    public class ClientRepository : IClientRepository
    {
        
        private readonly MvcIdentityDbContext _context;

        public ClientRepository( MvcIdentityDbContext context)
        {
            _context = context;
            context.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
        }

        public async Task<CreateClientRepoResult> CreateClientAsync(CreateClientRepoParam createClientRepoParam)
        {
            
            if (createClientRepoParam == null)
            {
                throw new RonikaStructureException("createClientRepoParam Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 52"
                    }
                }; 
            }
            
            var client = new Client
            {
                ClientName = createClientRepoParam.ClientName,
                Description = createClientRepoParam.Description,
                RequirePkce = createClientRepoParam.RequirePkce,
                AllowOfflineAccess = createClientRepoParam.AllowOfflineAccess,
                AlwaysSendClientClaims = createClientRepoParam.AlwaysSendClientClaims,
                LastAccessed = createClientRepoParam.LastAccessed
            };

            if (createClientRepoParam.AllowedGrantTypes!=null)
            {
                
                var clientGrantTypes= new List<ClientGrantType>();
                
                foreach (var grantType in createClientRepoParam.AllowedGrantTypes)
                {
                    
                    clientGrantTypes.Add(new ClientGrantType
                    {
                        GrantType = grantType,
                    });
                }
                
                client.AllowedGrantTypes=clientGrantTypes; 
            }
            
            if (createClientRepoParam.RedirectUris!=null)
            {
                
                var clientRedirectUris= new List<ClientRedirectUri>();
                
                foreach (var redirectUri in createClientRepoParam.RedirectUris)
                {
                    
                    clientRedirectUris.Add(new ClientRedirectUri
                    {
                        RedirectUri = redirectUri,
                    });
                }
                
                client.RedirectUris = clientRedirectUris;
            }

            if (createClientRepoParam.PostLogoutRedirectUris!=null)
            {
                
                var clientPostLogoutRedirectUriEntity = new List<ClientPostLogoutRedirectUri>();
                
                foreach (var logoutRedirectUri in createClientRepoParam.PostLogoutRedirectUris)
                {
                    clientPostLogoutRedirectUriEntity.Add(new ClientPostLogoutRedirectUri
                    {
                        PostLogoutRedirectUri = logoutRedirectUri,
                    });
                }
                
                client.PostLogoutRedirectUris = clientPostLogoutRedirectUriEntity;
                
            }

            if (createClientRepoParam.AllowedScopes!=null)
            {
                
                var clientScope=new List<ClientScope>();
                
                foreach (var scope in createClientRepoParam.AllowedScopes)
                {
                    
                    clientScope.Add(new ClientScope
                    {
                        Scope = scope,
                    });
                }
                
                client.AllowedScopes = clientScope;
            }


            if (createClientRepoParam.Claims!=null)
            {
                
                var clientClaim=new List<ClientClaim>();
                
                foreach (var claim in createClientRepoParam.Claims)
                {
                    
                    clientClaim.Add(new ClientClaim
                    {
                        Type = claim,
                        Value = claim
                    });
                }
                
                client.Claims = clientClaim;
            }

            if (createClientRepoParam.AllowedCorsOrigins!=null)
            {
                
                var clientCorsOrigin=new List<ClientCorsOrigin>();
                
                foreach (var cors in createClientRepoParam.AllowedCorsOrigins)
                {
                    clientCorsOrigin.Add(new ClientCorsOrigin
                    {
                        Origin = cors
                    });
                }
                
                client.AllowedCorsOrigins = clientCorsOrigin;
            }


            try
            {
                await _context.Client.AddAsync(client);
                await _context.SaveChangesAsync();
            }
            catch (Exception )
            {
                throw new RonikaServerException("Could not Create Client")
                {
                    ExceptionDetails = new ExceptionDetails
                    {
                        Message = RonikaErrorMessages.Public.InternalServerError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name+": 183"
                    }
                };
            }

            
            return new CreateClientRepoResult(client);
            
        }

        public async Task<CreateClientRepoResult> UpdateClientAsync(UpdateClientRepoParam updateClientRepoParam,int id)
        {
            
            if (id.ToString() == null)
            {
                throw new RonikaStructureException("Id Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 203"
                    }
                };
            }
            
            if (updateClientRepoParam == null)
            {
                throw new RonikaStructureException("updateClientRepoParam Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 215"
                    }
                }; 

            }

            Client client;

            try
            {
                client = await _context.Client
                    .Include(c=> c.Claims)
                    .Include(c=>c.AllowedScopes)
                    .Include(c=>c.RedirectUris)
                    .Include(c=> c.AllowedCorsOrigins)
                    .Include(c=>c.AllowedGrantTypes)
                    .Include(c=>c.PostLogoutRedirectUris)
                    .FirstOrDefaultAsync(c => c.Id == id);
            }
            catch (Exception )
            {
                throw new RonikaServerException("Could not Get Client")
                {
                    ExceptionDetails = new ExceptionDetails
                    {
                        Message = RonikaErrorMessages.Public.InternalServerError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name+": 241"
                    }
                };
            }

            
            if (client == null)
            {
                
                throw new RonikaStructureException("Client Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 255"
                    }
                };
                
            }
            
            _context.ClientClaims.RemoveRange( await _context.ClientClaims.Where(c => c.ClientId == client.Id).ToListAsync());
            
            _context.ClientRedirectUris.RemoveRange(await _context.ClientRedirectUris.Where(c => c.ClientId == client.Id).ToListAsync());
            
            _context.ClientPostLogoutRedirectUris.RemoveRange(await _context.ClientPostLogoutRedirectUris.Where(c => c.ClientId == client.Id).ToListAsync());
            
            _context.ClientGrantTypes.RemoveRange(await _context.ClientGrantTypes.Where(c => c.ClientId == client.Id).ToListAsync());
            
            _context.ClientCorsOrigins.RemoveRange(await _context.ClientCorsOrigins.Where(c => c.ClientId == client.Id).ToListAsync());
          
            _context.ClientScopes.RemoveRange(await _context.ClientScopes.Where(c => c.ClientId == client.Id).ToListAsync());
            
            _context.SaveChanges();
            
            
            client.ClientName = updateClientRepoParam.ClientName;
            client.Description = updateClientRepoParam.Description;
            client.RequirePkce = updateClientRepoParam.RequirePkce;
            client.AllowOfflineAccess = updateClientRepoParam.AllowOfflineAccess;
            client.AlwaysSendClientClaims = updateClientRepoParam.AlwaysSendClientClaims;
            client.LastAccessed = updateClientRepoParam.LastAccessed;
            
            
            

            if (updateClientRepoParam.AllowedGrantTypes!=null)
            {
                var clientGrantTypes= new List<ClientGrantType>();
                foreach (var grantType in updateClientRepoParam.AllowedGrantTypes)
                {
                
                    clientGrantTypes.Add(new ClientGrantType
                    {
                        GrantType = grantType,
                        ClientId = client.Id
                    });
                }
            
                client.AllowedGrantTypes=clientGrantTypes;
            }


            if (updateClientRepoParam.RedirectUris!=null)
            {
                
                var clientRedirectUris= new List<ClientRedirectUri>();
                
                foreach (var redirectUri in updateClientRepoParam.RedirectUris)
                {
                    
                    clientRedirectUris.Add(new ClientRedirectUri
                    {
                        RedirectUri = redirectUri,
                        ClientId = client.Id
                    });
                }
                client.RedirectUris = clientRedirectUris;
            }


            if (updateClientRepoParam.PostLogoutRedirectUris!=null)
            {
                
                var clientPostLogoutRedirectUriEntity = new List<ClientPostLogoutRedirectUri>();
                
                foreach (var logoutRedirectUri in updateClientRepoParam.PostLogoutRedirectUris)
                {
                    
                    clientPostLogoutRedirectUriEntity.Add(new ClientPostLogoutRedirectUri
                    {
                        PostLogoutRedirectUri = logoutRedirectUri,
                        ClientId = client.Id
                    });
                }
                
                client.PostLogoutRedirectUris = clientPostLogoutRedirectUriEntity;
            }

            if (updateClientRepoParam.AllowedScopes!=null)
            {
                
                var clientScope=new List<ClientScope>();
                
                foreach (var scope in updateClientRepoParam.AllowedScopes)
                {
                    
                    clientScope.Add(new ClientScope
                    {
                        Scope = scope,
                        ClientId = client.Id
                    });
                }
                
                client.AllowedScopes = clientScope;
            }

            if (updateClientRepoParam.Claims!=null)
            {
                
                var clientClaim=new List<ClientClaim>();
                
                foreach (var claim in updateClientRepoParam.Claims)
                {
                    clientClaim.Add(new ClientClaim
                    {
                        ClientId = client.Id,
                        Type = claim,
                        Value = claim
                    });
                }
                
                client.Claims = clientClaim;
            }

            if (updateClientRepoParam.AllowedCorsOrigins!=null)
            {
                
                var clientCorsOrigin=new List<ClientCorsOrigin>();
                
                foreach (var cors in updateClientRepoParam.AllowedCorsOrigins)
                {
                    
                    clientCorsOrigin.Add(new ClientCorsOrigin
                    {
                        ClientId = client.Id,
                        Origin = cors
                    });
                }
                
                client.AllowedCorsOrigins = clientCorsOrigin;

            }

            try
            {
                _context.Client.Update(client);
                await _context.SaveChangesAsync();
            }
            catch (Exception )
            {
                throw new RonikaServerException("Could not Update clientClaim")
                {
                    ExceptionDetails = new ExceptionDetails
                    {
                        Message = RonikaErrorMessages.Public.InternalServerError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name+": 406"
                    }
                };
            }
            
            return new CreateClientRepoResult(client);
            
        }
        

        public async Task<List<CreateClientRepoResult>> GetAllClientsAsync()
        {
            List<Client> allClients;

            try
            {
                allClients = await _context.Client.ToListAsync();

            }
            catch (Exception )
            {
                throw new RonikaServerException("Could not Get All clients")
                {
                    ExceptionDetails = new ExceptionDetails
                    {
                        Message = RonikaErrorMessages.Public.InternalServerError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name+": 432"
                    }
                };
            }
            
            if (allClients == null)
            {
                throw new RonikaStructureException("allClients Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 444"
                    }
                };
                
            }

            var clients = new List<CreateClientRepoResult>();
            
            foreach (var client in allClients)
            {
                clients.Add(new CreateClientRepoResult(client));
            }

            return clients;
        }

        public async Task<CreateClientRepoResult> GetOneClientAsync(int id)
        {
            
            if (id.ToString() == null)
            {
                throw new RonikaStructureException("Id Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 470"
                    }
                };
                
            }

            Client client;

            try
            {
                client = await _context.Client
                    .Include(c=> c.Claims)
                    .Include(c=>c.AllowedScopes)
                    .Include(c=>c.RedirectUris)
                    .Include(c=> c.AllowedCorsOrigins)
                    .Include(c=>c.AllowedGrantTypes)
                    .Include(c=>c.PostLogoutRedirectUris)
                    .FirstOrDefaultAsync(m => m.Id == id);
            }
            catch (Exception )
            {
                throw new RonikaServerException("Could not Get client")
                {
                    ExceptionDetails = new ExceptionDetails
                    {
                        Message = RonikaErrorMessages.Public.InternalServerError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name+": 496"
                    }
                };
            }


            if (client == null)
            {
                throw new RonikaStructureException("client Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 509"
                    }
                };
                
            }
            
            return new CreateClientRepoResult(client);

        }
        
        public async Task DeleteClientAsync(int id)
        {
            
            if (id.ToString()==null)
            {
                throw new RonikaStructureException("Id Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 529"
                    }
                };
                
            }

            Client client;

            try
            {
                client = await _context.Client
                    .FirstOrDefaultAsync(m => m.Id == id);
            }
            catch (Exception )
            {
                throw new RonikaServerException("Could not Get client")
                {
                    ExceptionDetails = new ExceptionDetails
                    {
                        Message = RonikaErrorMessages.Public.InternalServerError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name+": 549"
                    }
                };
            }


            if (client == null)
            {
                throw new RonikaStructureException("client Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 562"
                    }
                };
                
            }

            try
            {
                _context.Client.Remove(client);
                await _context.SaveChangesAsync();
            }
            catch (Exception )
            {
                throw new RonikaServerException("Could not Delete client")
                {
                    ExceptionDetails = new ExceptionDetails
                    {
                        Message = RonikaErrorMessages.Public.InternalServerError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name+": 580"
                    }
                };
            }


        }
        
    }
}