using System;
using System.Collections.Generic;
using System.Reflection;
using System.Threading.Tasks;
using Exceptions;
using OAuthCRUD.Models.Entities.ClientGrantTypeEntity;
using OAuthCRUD.Models.RepoResults.ClientGrantType;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using OAuthCRUD.Data;
using OAuthCRUD.Models.Entities.ClientPostLogoutRedirectUriEntity;
using OAuthCRUD.Models.RepoParams.ClientPostLogoutRedirectUri;
using OAuthCRUD.Models.RepoResults.ClientPostLogoutRedirectUri;
using Ronika.Utility.Consts;

namespace OAuthCRUD.Repository
{
    public interface IClientPostLogoutRedirectUriRepository
    {
        Task DeleteClientPostLogoutRedirectUriAsync(int id);
        Task<CreateClientPostLogoutRedirectUriRepoResult> GetOneClientPostLogoutRedirectUriAsync(int id);
        
        Task<List<CreateClientPostLogoutRedirectUriRepoResult>> GetAllClientPostLogoutRedirectUrisAsync();

        Task<CreateClientPostLogoutRedirectUriRepoResult> UpdateClientPostLogoutRedirectUriAsync(
            UpdateClientPostLogoutRedirectUriRepoParam updateClientPostLogoutRedirectUriRepoParam, int id);

        Task<CreateClientPostLogoutRedirectUriRepoResult> CreateClientPostLogoutRedirectUriAsync(
            CreateClientPostLogoutRedirectUriRepoParam createClientPostLogoutRedirectUriRepoParam);
    }
    public class ClientPostLogoutRedirectUriRepository : IClientPostLogoutRedirectUriRepository
    {
        
        private readonly MvcIdentityDbContext _context;
        
        public ClientPostLogoutRedirectUriRepository( MvcIdentityDbContext context)
        {
            _context = context;
            context.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
        }
        
        public async Task<CreateClientPostLogoutRedirectUriRepoResult> CreateClientPostLogoutRedirectUriAsync(
            CreateClientPostLogoutRedirectUriRepoParam createClientPostLogoutRedirectUriRepoParam)
        {
            
            if (createClientPostLogoutRedirectUriRepoParam == null)
            {
                throw new RonikaStructureException("createClientPostLogoutRedirectUriRepoParam Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 53"
                    }
                };
            }

            var clientPostLogoutRedirectUri = new ClientPostLogoutRedirectUri
            {
                PostLogoutRedirectUri = createClientPostLogoutRedirectUriRepoParam.PostLogoutRedirectUri,
                ClientId = createClientPostLogoutRedirectUriRepoParam.ClientId,
            };

            try
            {
                await _context.ClientPostLogoutRedirectUris.AddAsync(clientPostLogoutRedirectUri);
                await _context.SaveChangesAsync();
            }
            catch (Exception )
            {
                throw new RonikaServerException("Could not create clientPostLogoutRedirectUri")
                {
                    ExceptionDetails = new ExceptionDetails
                    {
                        Message = RonikaErrorMessages.Public.InternalServerError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name+": 76"
                    }
                };
            }

            
            return new CreateClientPostLogoutRedirectUriRepoResult(clientPostLogoutRedirectUri);
        }
        
        public async Task<CreateClientPostLogoutRedirectUriRepoResult> UpdateClientPostLogoutRedirectUriAsync(
            UpdateClientPostLogoutRedirectUriRepoParam updateClientPostLogoutRedirectUriRepoParam,int id)
        {
            
            if (id.ToString() == null)
            {
                throw new RonikaStructureException("Id Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 96"
                    }
                };
            }
            
            if (updateClientPostLogoutRedirectUriRepoParam == null)
            {
                throw new RonikaStructureException("updateClientPostLogoutRedirectUriRepoParam Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 108"
                    }
                };
            }

            ClientPostLogoutRedirectUri clientPostLogoutRedirectUri;

            try
            {
                clientPostLogoutRedirectUri = await _context.ClientPostLogoutRedirectUris.FirstOrDefaultAsync(c => c.Id == id);

            }
            catch (Exception )
            {
                throw new RonikaServerException("Could not Get ClientPostLogoutRedirectUri")
                {
                    ExceptionDetails = new ExceptionDetails
                    {
                        Message = RonikaErrorMessages.Public.InternalServerError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name+": 127"
                    }
                };
            }
            
            if (clientPostLogoutRedirectUri == null)
            {
                
                throw new RonikaStructureException("clientPostLogoutRedirectUri Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 140"
                    }
                };
            }

            clientPostLogoutRedirectUri.PostLogoutRedirectUri =
                updateClientPostLogoutRedirectUriRepoParam.PostLogoutRedirectUri;



            try
            {
                _context.ClientPostLogoutRedirectUris.Update(clientPostLogoutRedirectUri);
                await _context.SaveChangesAsync();
            }
            catch (Exception )
            {
                throw new RonikaServerException("Could not Update clientPostLogoutRedirectUri")
                {
                    ExceptionDetails = new ExceptionDetails
                    {
                        Message = RonikaErrorMessages.Public.InternalServerError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name+": 162"
                    }
                };
            }
            
            
            return new CreateClientPostLogoutRedirectUriRepoResult(clientPostLogoutRedirectUri);
            
        }
        public async Task<List<CreateClientPostLogoutRedirectUriRepoResult>> GetAllClientPostLogoutRedirectUrisAsync()
        {

            List<ClientPostLogoutRedirectUri> postLogoutRedirectUris;

            try
            {
                postLogoutRedirectUris = await _context.ClientPostLogoutRedirectUris.ToListAsync();
            }
            catch (Exception )
            {
                throw new RonikaServerException("Could not Get All postLogoutRedirectUris")
                {
                    ExceptionDetails = new ExceptionDetails
                    {
                        Message = RonikaErrorMessages.Public.InternalServerError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name+": 187"
                    }
                };
            }
            
            
            if (postLogoutRedirectUris == null)
            {
                throw new RonikaStructureException("postLogoutRedirectUris Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 200"
                    }
                };
            }

            var postLogoutRedirectUriRepoResults = new List<CreateClientPostLogoutRedirectUriRepoResult>();
            
            foreach (var postLogoutRedirectUri in postLogoutRedirectUris)
            {
                postLogoutRedirectUriRepoResults.Add(new CreateClientPostLogoutRedirectUriRepoResult(postLogoutRedirectUri));
            }

            return postLogoutRedirectUriRepoResults;
        }
        public async Task<CreateClientPostLogoutRedirectUriRepoResult> GetOneClientPostLogoutRedirectUriAsync(int id)
        {
            
            if (id.ToString() == null)
            {
                throw new RonikaStructureException("Id Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 224"
                    }
                };
            }

            ClientPostLogoutRedirectUri clientPostLogoutRedirectUri;

            try
            {
                clientPostLogoutRedirectUri = await _context.ClientPostLogoutRedirectUris.FirstOrDefaultAsync(c => c.Id == id);

            }
            catch (Exception )
            {
                throw new RonikaServerException("Could not Get clientPostLogoutRedirectUri")
                {
                    ExceptionDetails = new ExceptionDetails
                    {
                        Message = RonikaErrorMessages.Public.InternalServerError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name+": 244"
                    }
                };
            }

            if (clientPostLogoutRedirectUri == null)
            {
                throw new RonikaStructureException("clientPostLogoutRedirectUri Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 255"
                    }
                };
            }
            
            return new CreateClientPostLogoutRedirectUriRepoResult(clientPostLogoutRedirectUri);

        }
        public async Task DeleteClientPostLogoutRedirectUriAsync(int id)
        {
            
            if (id.ToString()==null)
            {
                throw new RonikaStructureException("Id Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 273"
                    }
                };
            }

            
            ClientPostLogoutRedirectUri clientPostLogoutRedirectUri;

            try
            {
                clientPostLogoutRedirectUri = await _context.ClientPostLogoutRedirectUris.FirstOrDefaultAsync(c => c.Id == id);

            }
            catch (Exception )
            {
                throw new RonikaServerException("Could not Get clientPostLogoutRedirectUri")
                {
                    ExceptionDetails = new ExceptionDetails
                    {
                        Message = RonikaErrorMessages.Public.InternalServerError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name+": 293"
                    }
                };
            }

            if (clientPostLogoutRedirectUri == null)
            {
                throw new RonikaStructureException("clientPostLogoutRedirectUri Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 305"
                    }
                };
            }


            try
            {
                _context.ClientPostLogoutRedirectUris.Remove(clientPostLogoutRedirectUri);
                await _context.SaveChangesAsync();
            }
            catch (Exception )
            {
                throw new RonikaServerException("Could not Delete clientPostLogoutRedirectUri")
                {
                    ExceptionDetails = new ExceptionDetails
                    {
                        Message = RonikaErrorMessages.Public.InternalServerError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name+": 323"
                    }
                };
            }

        }
        
        
        
    }
}