﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using OAuthCRUD.Data;
using OAuthCRUD.Manager;
using OAuthCRUD.Repository;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Ronika.ExceptionMiddleware.Extensions;
using Ronika.Utility.Extensions;

namespace OAuthCRUD
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            services.AddEntityFrameworkNpgsql().AddDbContext<MvcIdentityDbContext>(
                options =>
                    options.UseNpgsql(Configuration.GetConnectionString("IdentityContext")));

            services.AddResponseCreatorExtension();
            services.AddHttpClient();
            
            services.AddTransient<IClientRepository, ClientRepository>();
            services.AddTransient<IClientManager, ClientManager>();
            services.AddTransient<IClientClaimManager, ClientClaimManager>();
            services.AddTransient<IClientClaimRepository, ClientClaimRepository>();
            services.AddTransient<IClientCorsOriginManager, ClientCorsOriginManager>();
            services.AddTransient<IClientCorsOriginRepository, ClientCorsOriginRepository>();
            services.AddTransient<IClientGrantTypeManager, ClientGrantTypeManager>();
            services.AddTransient<IClientGrantTypeRepository, ClientGrantTypeRepository>();
            services.AddTransient<IClientPostLogoutRedirectUriManager, ClientPostLogoutRedirectUriManager>();
            services.AddTransient<IClientPostLogoutRedirectUriRepository,ClientPostLogoutRedirectUriRepository>();
            services.AddTransient<IClientRedirectUriManager, ClientRedirectUriManager>();
            services.AddTransient<IClientRedirectUriRepository,ClientRedirectUriRepository>();
            services.AddTransient<IClientScopeManager, ClientScopeManager>();
            services.AddTransient<IClientScopeRepository,ClientScopeRepository>();
            services.AddTransient<IApiResourcesManager, ApiResourcesManager>();
            services.AddTransient<IApiResourcesRepository,ApiResourcesRepository>();
            
            services.AddMvcCore()
                .AddAuthorization()
                .AddJsonFormatters();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.AddExceptionMiddleWare();
            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}