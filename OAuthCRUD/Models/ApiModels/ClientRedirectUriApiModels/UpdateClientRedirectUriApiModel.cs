namespace OAuthCRUD.Models.ApiModels.ClientRedirectUriApiModels
{
    public class UpdateClientRedirectUriApiModel
    {
        public string RedirectUri{ get; set; }
    }
}