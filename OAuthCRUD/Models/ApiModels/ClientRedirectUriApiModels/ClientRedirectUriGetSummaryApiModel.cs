using OAuthCRUD.Models.RepoResults.ClientRedirectUri;

namespace OAuthCRUD.Models.ApiModels.ClientRedirectUriApiModels
{
    public class ClientRedirectUriGetSummaryApiModel
    {
        public ClientRedirectUriGetSummaryApiModel(CreateClientRedirectUriRepoResult createClientRedirectUriRepoResult)
        {
            Id = createClientRedirectUriRepoResult.Id;
            RedirectUri = createClientRedirectUriRepoResult.RedirectUri;
            ClientId = createClientRedirectUriRepoResult.ClientId;
        }
        public int Id { get; set; }
        public string RedirectUri{ get; set; }
        public int ClientId{ get; set; }
    }
}