using OAuthCRUD.Models.RepoResults.ClientScope;

namespace OAuthCRUD.Models.ApiModels.ClientScopeApiModels
{
    public class ClientScopeGetCompleteApiModel
    {
        public ClientScopeGetCompleteApiModel(CreateClientScopeRepoResult createClientScopeRepoResult)
        {
            Id = createClientScopeRepoResult.Id;
            Scope = createClientScopeRepoResult.Scope;
            ClientId = createClientScopeRepoResult.ClientId;
        }
        public int Id { get; set; }
        public string Scope { get; set; }
        public int ClientId { get; set; }
    }
}