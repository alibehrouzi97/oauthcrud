namespace OAuthCRUD.Models.ApiModels.ClientScopeApiModels
{
    public class UpdateClientScopeApiModel
    {
        public string Scope { get; set; } 
    }
}