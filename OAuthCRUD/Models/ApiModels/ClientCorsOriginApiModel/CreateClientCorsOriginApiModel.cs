namespace OAuthCRUD.Models.ApiModels.ClientCorsOriginApiModel
{
    public class CreateClientCorsOriginApiModel
    {
        public string Origin { get; set; }
        public int ClientId { get; set; }
    }
}