namespace OAuthCRUD.Models.ApiModels.ClientCorsOriginApiModel
{
    public class UpdateClientCorsOriginApiModel
    {
        public string Origin { get; set; }
    }
}