namespace OAuthCRUD.Models.ApiModels.ClientClaimApiModels
{
    public class UpdateClientClaimApiModel
    {
        public string Type { get; set; }
        public string Value { get; set; }
    }
}