using OAuthCRUD.Models.RepoResults.ClientClaim;

namespace OAuthCRUD.Models.ApiModels.ClientClaimApiModels
{
    public class ClientClaimGetSummaryApiModel
    {
        public ClientClaimGetSummaryApiModel(CreateClientClaimRepoResult createClientClaimRepoResult)
        {
            Id = createClientClaimRepoResult.Id;
            Type = createClientClaimRepoResult.Type;
            Value = createClientClaimRepoResult.Value;
            ClientId = createClientClaimRepoResult.ClientId;
        }
        
        public int Id { get; set; }
        public string Type { get; set; }
        public string Value { get; set; }
        public int ClientId { get; set; }
    }
}