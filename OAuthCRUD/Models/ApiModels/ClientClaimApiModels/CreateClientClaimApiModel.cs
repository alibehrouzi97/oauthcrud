namespace OAuthCRUD.Models.ApiModels.ClientClaimApiModels
{
    public class CreateClientClaimApiModel
    {
        public string Type { get; set; }
        public string Value { get; set; }
        public int ClientId { get; set; }
    }
}