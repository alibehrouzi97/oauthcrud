using System;
using OAuthCRUD.Models.RepoResults.Client;

namespace OAuthCRUD.Models.ApiModels.ClientApiModels
{
    public class ClientGetSummaryApiModel
    {
        public ClientGetSummaryApiModel(CreateClientRepoResult createClientRepoResult)
        {
            Id = createClientRepoResult.Id;
            ClientName = createClientRepoResult.ClientName;
            Description = createClientRepoResult.Description;
            RequirePkce = createClientRepoResult.RequirePkce;
            AllowOfflineAccess = createClientRepoResult.AllowOfflineAccess;
            AlwaysSendClientClaims = createClientRepoResult.AlwaysSendClientClaims;
            LastAccessed = createClientRepoResult.LastAccessed;
            

        }
        
        public int Id { get; set; }
        public string ClientName { get; set; }
        public string Description { get; set; }


        public bool RequirePkce { get; set; }
        
        public bool AllowOfflineAccess { get; set; }


        public bool AlwaysSendClientClaims { get ; set; }

        public DateTime? LastAccessed { get ; set; }
    }
}