using System;
using System.Collections.Generic;
using System.Security.Claims;
using OAuthCRUD.Models.Entities.ClientEntity;
using OAuthCRUD.Models.Entities.ClientClaimEntity;
using OAuthCRUD.Models.Entities.ClientCorsOriginEntity;
using OAuthCRUD.Models.Entities.ClientGrantTypeEntity;
using OAuthCRUD.Models.Entities.ClientPostLogoutRedirectUriEntity;
using OAuthCRUD.Models.Entities.ClientRedirectUriEntity;
using OAuthCRUD.Models.Entities.ClientScopeEntity;
using OAuthCRUD.Models.RepoResults.Client;

namespace OAuthCRUD.Models.ApiModels.ClientApiModels
{
    public class ClientGetCompleteApiModel
    {
        public ClientGetCompleteApiModel(CreateClientRepoResult createClientRepoResult)
        {
            Id = createClientRepoResult.Id;
            ClientName = createClientRepoResult.ClientName;
            Description = createClientRepoResult.Description;
            RequirePkce = createClientRepoResult.RequirePkce;
            AllowOfflineAccess = createClientRepoResult.AllowOfflineAccess;
            AlwaysSendClientClaims = createClientRepoResult.AlwaysSendClientClaims;
            LastAccessed = createClientRepoResult.LastAccessed;
            AllowedScopes = createClientRepoResult.AllowedScopes;
            Claims = createClientRepoResult.Claims;
            AllowedGrantTypes = createClientRepoResult.AllowedGrantTypes;
            RedirectUris = createClientRepoResult.RedirectUris;
            PostLogoutRedirectUris = createClientRepoResult.PostLogoutRedirectUris;
            AllowedCorsOrigins = createClientRepoResult.AllowedCorsOrigins;

        }
        
        public int Id { get; set; }
        public string ClientName { get; set; }
        public string Description { get; set; }
        public bool RequirePkce { get; set; }
        
        public bool AllowOfflineAccess { get; set; }
        
        public bool AlwaysSendClientClaims { get ; set; }
        public DateTime? LastAccessed { get ; set; }
        
        public List<ClientGrantType>  AllowedGrantTypes { get; set; }
        public List<ClientRedirectUri> RedirectUris { get ; set; }
        public List<ClientPostLogoutRedirectUri> PostLogoutRedirectUris { get ; set; }
        
        public List<ClientCorsOrigin> AllowedCorsOrigins { get ; set; }
        public List<ClientScope> AllowedScopes { get ; set; }
        public List< ClientClaim> Claims { get ; set; }
    }
}