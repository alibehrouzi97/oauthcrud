using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace OAuthCRUD.Models.ApiModels.ClientApiModels
{
    public class UpdateClientApiModel
    {
        
        public string ClientName { get; set; }
        public string Description { get; set; }
        public List<string>  AllowedGrantTypes { get; set; }
        public bool AllowOfflineAccess { get; set; }
        public bool RequirePkce { get; set; }
        public bool AlwaysSendClientClaims { get ; set; }
        
        public DateTime? LastAccessed { get ; set; }
        
        

        public List<string> RedirectUris { get ; set; }
        public List<string> PostLogoutRedirectUris { get ; set; }
        

        public List<string> AllowedScopes { get ; set; }
        public List<string> Claims { get ; set; }
       
        public List<string> AllowedCorsOrigins { get ; set; }
    }
}