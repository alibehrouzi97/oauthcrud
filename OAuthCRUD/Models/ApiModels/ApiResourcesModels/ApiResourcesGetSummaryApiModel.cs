using OAuthCRUD.Models.RepoResults.ApiResources;

namespace OAuthCRUD.Models.ApiModels.ApiResourcesModels
{
    public class ApiResourcesGetSummaryApiModel
    {
        public ApiResourcesGetSummaryApiModel(CreateApiResourcesRepoResult createApiResourcesRepoResult)
        {
            Id = createApiResourcesRepoResult.Id;
            Name = createApiResourcesRepoResult.Name;
            DisplayName = createApiResourcesRepoResult.DisplayName;
            Description = createApiResourcesRepoResult.Description;
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public string DisplayName { get; set; }
        public string Description { get; set; }
    }
}