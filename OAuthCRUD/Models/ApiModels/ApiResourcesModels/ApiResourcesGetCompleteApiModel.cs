using System;
using OAuthCRUD.Models.RepoResults.ApiResources;

namespace OAuthCRUD.Models.ApiModels.ApiResourcesModels
{
    public class ApiResourcesGetCompleteApiModel
    {
        public ApiResourcesGetCompleteApiModel(CreateApiResourcesRepoResult createApiResourcesRepoResult)
        {
            Id = createApiResourcesRepoResult.Id;
            Name = createApiResourcesRepoResult.Name;
            DisplayName = createApiResourcesRepoResult.DisplayName;
            Description = createApiResourcesRepoResult.Description;
            Created = createApiResourcesRepoResult.Created;
            Update = createApiResourcesRepoResult.Update;
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public string DisplayName { get; set; }
        public string Description { get; set; }
        public DateTime Created { get; set; }
        public DateTime? Update { get; set; }
    }
}