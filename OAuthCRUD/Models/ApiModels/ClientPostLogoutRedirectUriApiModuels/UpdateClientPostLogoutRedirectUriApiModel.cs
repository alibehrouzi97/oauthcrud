namespace OAuthCRUD.Models.ApiModels.ClientPostLogoutRedirectUriApiModuels
{
    public class UpdateClientPostLogoutRedirectUriApiModel
    {
        public string PostLogoutRedirectUri { get; set; }
    }
}