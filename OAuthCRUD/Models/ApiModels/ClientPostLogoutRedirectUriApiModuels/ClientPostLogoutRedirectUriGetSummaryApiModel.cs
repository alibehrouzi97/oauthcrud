using OAuthCRUD.Models.RepoResults.ClientPostLogoutRedirectUri;

namespace OAuthCRUD.Models.ApiModels.ClientPostLogoutRedirectUriApiModuels
{
    public class ClientPostLogoutRedirectUriGetSummaryApiModel
    {
        public ClientPostLogoutRedirectUriGetSummaryApiModel(
            CreateClientPostLogoutRedirectUriRepoResult createClientPostLogoutRedirectUriRepoResult)
        {
            Id = createClientPostLogoutRedirectUriRepoResult.Id;
            PostLogoutRedirectUri = createClientPostLogoutRedirectUriRepoResult.PostLogoutRedirectUri;
            ClientId = createClientPostLogoutRedirectUriRepoResult.ClientId;
        }
        public int Id { get; set; }
        public string PostLogoutRedirectUri { get; set; }
        public int ClientId { get; set; }
    }
}