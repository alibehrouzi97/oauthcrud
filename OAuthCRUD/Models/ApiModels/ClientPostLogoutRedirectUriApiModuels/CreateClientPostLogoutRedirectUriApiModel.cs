namespace OAuthCRUD.Models.ApiModels.ClientPostLogoutRedirectUriApiModuels
{
    public class CreateClientPostLogoutRedirectUriApiModel
    {
        public string PostLogoutRedirectUri { get; set; }
        public int ClientId { get; set; }
    }
}