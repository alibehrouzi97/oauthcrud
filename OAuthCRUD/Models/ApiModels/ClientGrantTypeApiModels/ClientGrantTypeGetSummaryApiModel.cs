using OAuthCRUD.Models.RepoResults.ClientGrantType;

namespace OAuthCRUD.Models.ApiModels.ClientGrantTypeApiModels
{
    public class ClientGrantTypeGetSummaryApiModel
    {
        public ClientGrantTypeGetSummaryApiModel(CreateClientGrantTypeRepoResult createClientGrantTypeRepoResult)
        {
            Id = createClientGrantTypeRepoResult.Id;
            GrantType = createClientGrantTypeRepoResult.GrantType;
            ClientId = createClientGrantTypeRepoResult.ClientId;
        }
        public int Id{ get; set; }
        public string GrantType{ get; set; }
        public int ClientId{ get; set; }
    }
}