namespace OAuthCRUD.Models.ApiModels.ClientGrantTypeApiModels
{
    public class CreateClientGrantTypeApiModel
    {
        public string GrantType{ get; set; }
        public int ClientId{ get; set; }
    }
}