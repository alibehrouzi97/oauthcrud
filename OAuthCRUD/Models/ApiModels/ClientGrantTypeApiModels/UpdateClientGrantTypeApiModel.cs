namespace OAuthCRUD.Models.ApiModels.ClientGrantTypeApiModels
{
    public class UpdateClientGrantTypeApiModel
    {
        public string GrantType{ get; set; }
    }
}