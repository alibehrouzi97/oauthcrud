namespace OAuthCRUD.Models.RepoResults.ClientScope
{
    public class CreateClientScopeRepoResult
    {
        public CreateClientScopeRepoResult(Entities.ClientScopeEntity.ClientScope clientScope)
        {
            Id = clientScope.Id;
            Scope = clientScope.Scope;
            ClientId = clientScope.ClientId;
        }
        public int Id { get; set; }
        public string Scope { get; set; }
        public int ClientId { get; set; }
    }
}