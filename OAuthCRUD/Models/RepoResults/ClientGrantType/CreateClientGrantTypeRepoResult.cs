namespace OAuthCRUD.Models.RepoResults.ClientGrantType
{
    public class CreateClientGrantTypeRepoResult
    {
        public CreateClientGrantTypeRepoResult(Entities.ClientGrantTypeEntity.ClientGrantType clientGrantType)
        {
            Id = clientGrantType.Id;
            GrantType = clientGrantType.GrantType;
            ClientId = clientGrantType.ClientId;
        }
        public int Id{ get; set; }
        public string GrantType{ get; set; }
        public int ClientId{ get; set; }
    }
}