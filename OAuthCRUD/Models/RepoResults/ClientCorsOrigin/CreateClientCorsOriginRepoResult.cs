namespace OAuthCRUD.Models.RepoResults.ClientCorsOrigin
{
    public class CreateClientCorsOriginRepoResult
    {
        public CreateClientCorsOriginRepoResult(Entities.ClientCorsOriginEntity.ClientCorsOrigin clientCorsOrigin)
        {
            Id = clientCorsOrigin.Id;
            Origin = clientCorsOrigin.Origin;
            ClientId = clientCorsOrigin.ClientId;
        }
        public int Id { get; set; }
        public string Origin { get; set; }
        public int ClientId { get; set; }
    }
}