using System;

namespace OAuthCRUD.Models.RepoResults.ApiResources
{
    public class CreateApiResourcesRepoResult
    {
        public CreateApiResourcesRepoResult(Entities.ApiResourcesEntity.ApiResources apiResources)
        {
            
            Id = apiResources.Id;
            Name = apiResources.Name;
            DisplayName = apiResources.DisplayName;
            Description = apiResources.Description;
            Created = apiResources.Created;
            Update = apiResources.Update;
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public string DisplayName { get; set; }
        public string Description { get; set; }
        public DateTime Created { get; set; }
        public DateTime? Update { get; set; }
    }
}