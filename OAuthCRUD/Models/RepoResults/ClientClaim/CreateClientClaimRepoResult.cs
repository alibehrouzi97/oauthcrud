namespace OAuthCRUD.Models.RepoResults.ClientClaim
{
    public class CreateClientClaimRepoResult
    {
        public CreateClientClaimRepoResult(Entities.ClientClaimEntity.ClientClaim clientClaim)
        {
            Id = clientClaim.Id;
            Type = clientClaim.Type;
            Value = clientClaim.Value;
            ClientId = clientClaim.ClientId;
        }
        public int Id { get; set; }
        public string Type { get; set; }
        public string Value { get; set; }
        public int ClientId { get; set; }

    }
}