using System;
using System.Collections.Generic;
using OAuthCRUD.Models.Entities.ClientClaimEntity;
using OAuthCRUD.Models.Entities.ClientCorsOriginEntity;
using OAuthCRUD.Models.Entities.ClientGrantTypeEntity;
using OAuthCRUD.Models.Entities.ClientPostLogoutRedirectUriEntity;
using OAuthCRUD.Models.Entities.ClientRedirectUriEntity;
using OAuthCRUD.Models.Entities.ClientScopeEntity;

namespace OAuthCRUD.Models.RepoResults.Client
{
    public class CreateClientRepoResult
    {
       
        public CreateClientRepoResult(Entities.ClientEntity.Client client)
        {
            Id = client.Id;
            ClientName = client.ClientName;
            Description = client.Description;
            RequirePkce = client.RequirePkce;
            AllowOfflineAccess = client.AllowOfflineAccess;
            AlwaysSendClientClaims = client.AlwaysSendClientClaims;
            LastAccessed = client.LastAccessed;
            AllowedGrantTypes = client.AllowedGrantTypes;
            RedirectUris = client.RedirectUris;
            PostLogoutRedirectUris = client.PostLogoutRedirectUris;
            AllowedScopes = client.AllowedScopes;
            Claims = client.Claims;
            AllowedCorsOrigins = client.AllowedCorsOrigins;



        }
        
        public int Id { get; set; }
        public string ClientName { get; set; }
        public string Description { get; set; }
        
        public bool AllowOfflineAccess { get; set; }
        public bool RequirePkce { get; set; }
        public bool AlwaysSendClientClaims { get ; set; }
        
        public DateTime? LastAccessed { get ; set; }
        
        public List<Entities.ClientGrantTypeEntity.ClientGrantType>  AllowedGrantTypes { get; set; }
        public List<Entities.ClientRedirectUriEntity.ClientRedirectUri> RedirectUris { get ; set; }
        public List<Entities.ClientPostLogoutRedirectUriEntity.ClientPostLogoutRedirectUri> PostLogoutRedirectUris { get ; set; }
        
        public List<Entities.ClientCorsOriginEntity.ClientCorsOrigin> AllowedCorsOrigins { get ; set; }
        public List<Entities.ClientScopeEntity.ClientScope> AllowedScopes { get ; set; }
        public List<Entities.ClientClaimEntity.ClientClaim> Claims { get ; set; }

    }
}