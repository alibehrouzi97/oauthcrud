namespace OAuthCRUD.Models.RepoResults.ClientRedirectUri
{
    public class CreateClientRedirectUriRepoResult
    {
        public CreateClientRedirectUriRepoResult(Entities.ClientRedirectUriEntity.ClientRedirectUri clientRedirectUri)
        {
            Id = clientRedirectUri.Id;
            RedirectUri = clientRedirectUri.RedirectUri;
            ClientId = clientRedirectUri.ClientId;
        }
        public int Id { get; set; }
        public string RedirectUri{ get; set; }
        public int ClientId{ get; set; }
    }
}