namespace OAuthCRUD.Models.RepoResults.ClientPostLogoutRedirectUri
{
    public class CreateClientPostLogoutRedirectUriRepoResult
    {
        public CreateClientPostLogoutRedirectUriRepoResult(
            Entities.ClientPostLogoutRedirectUriEntity.ClientPostLogoutRedirectUri clientPostLogoutRedirectUri)
        {
            Id = clientPostLogoutRedirectUri.Id;
            PostLogoutRedirectUri = clientPostLogoutRedirectUri.PostLogoutRedirectUri;
            ClientId = clientPostLogoutRedirectUri.ClientId;
        }
        public int Id { get; set; }
        public string PostLogoutRedirectUri { get; set; }
        public int ClientId { get; set; }
    }
}