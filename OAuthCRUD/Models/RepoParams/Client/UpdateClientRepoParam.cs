using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using OAuthCRUD.Models.ApiModels.ClientApiModels;

namespace OAuthCRUD.Models.RepoParams.Client
{
    public class UpdateClientRepoParam
    {
        public UpdateClientRepoParam(UpdateClientApiModel updateClientApiModel)
        {
            ClientName = updateClientApiModel.ClientName;
            Description = updateClientApiModel.Description;
            RequirePkce = updateClientApiModel.RequirePkce;
            AllowOfflineAccess = updateClientApiModel.AllowOfflineAccess;
            AlwaysSendClientClaims = updateClientApiModel.AlwaysSendClientClaims;
            LastAccessed = updateClientApiModel.LastAccessed;
            
            AllowedGrantTypes = updateClientApiModel.AllowedGrantTypes;
            RedirectUris = updateClientApiModel.RedirectUris;
            PostLogoutRedirectUris = updateClientApiModel.PostLogoutRedirectUris;
            AllowedScopes = updateClientApiModel.AllowedScopes;
            Claims = updateClientApiModel.Claims;
            AllowedCorsOrigins = updateClientApiModel.AllowedCorsOrigins;
            
        }
        
        public string ClientName { get; set; }
        public string Description { get; set; }
        
        public bool AllowOfflineAccess { get; set; }
        public bool RequirePkce { get; set; }
        public bool AlwaysSendClientClaims { get ; set; }
        
        public DateTime? LastAccessed { get ; set; }
        [Required]
        public List<string>  AllowedGrantTypes { get; set; }
        public List<string> RedirectUris { get ; set; }
        public List<string> PostLogoutRedirectUris { get ; set; }
        

        public List<string> AllowedScopes { get ; set; }
        public List<string> Claims { get ; set; }
       
        public List<string> AllowedCorsOrigins { get ; set; }
    }
}