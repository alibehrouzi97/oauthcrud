using System;
using System.Collections.Generic;
using System.Linq;
using OAuthCRUD.Models.Entities.ClientPostLogoutRedirectUriEntity;
using OAuthCRUD.Models.ApiModels.ClientApiModels;

namespace OAuthCRUD.Models.RepoParams.Client
{
    public class CreateClientRepoParam
    {

        public CreateClientRepoParam(CreateClientApiModel createClientApiModel)
        {
            ClientName = createClientApiModel.ClientName;
            Description = createClientApiModel.Description;
            RequirePkce = createClientApiModel.RequirePkce;
            AllowOfflineAccess = createClientApiModel.AllowOfflineAccess;
            AlwaysSendClientClaims = createClientApiModel.AlwaysSendClientClaims;
            LastAccessed = createClientApiModel.LastAccessed;
            AllowedGrantTypes = createClientApiModel.AllowedGrantTypes;
            RedirectUris = createClientApiModel.RedirectUris; 
            PostLogoutRedirectUris = createClientApiModel.PostLogoutRedirectUris;
            AllowedScopes = createClientApiModel.AllowedScopes;
            Claims = createClientApiModel.Claims;
            AllowedCorsOrigins = createClientApiModel.AllowedCorsOrigins;
            
            
        }
        
        public string ClientName { get; set; }
        public string Description { get; set; }
        public List<string>  AllowedGrantTypes { get; set; }

        public bool RequirePkce { get; set; }
        
        public List<string> RedirectUris { get ; set; }
        public List<string> PostLogoutRedirectUris { get ; set; }
        public bool AllowOfflineAccess { get; set; }

        public List<string> AllowedScopes { get ; set; }
        public List< string> Claims { get ; set; }
        public bool AlwaysSendClientClaims { get ; set; }
        public List<string> AllowedCorsOrigins { get ; set; }
        public DateTime? LastAccessed { get ; set; }
    }
}