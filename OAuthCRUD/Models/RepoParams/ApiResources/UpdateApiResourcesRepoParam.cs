using System;
using OAuthCRUD.Models.ApiModels.ApiResourcesModels;

namespace OAuthCRUD.Models.RepoParams.ApiResources
{
    public class UpdateApiResourcesRepoParam
    {
        public UpdateApiResourcesRepoParam(UpdateApiResourcesApiModel updateApiResourcesApiModel)
        {
            Name = updateApiResourcesApiModel.Name;
            DisplayName = updateApiResourcesApiModel.DisplayName;
            Description = updateApiResourcesApiModel.Description;
        }
        public string Name { get; set; }
        public string DisplayName { get; set; }
        public string Description { get; set; }
    }
}