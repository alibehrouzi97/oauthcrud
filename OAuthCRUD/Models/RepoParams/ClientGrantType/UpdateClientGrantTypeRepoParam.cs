using OAuthCRUD.Models.ApiModels.ClientGrantTypeApiModels;

namespace OAuthCRUD.Models.RepoParams.ClientGrantType
{
    public class UpdateClientGrantTypeRepoParam
    {
        public UpdateClientGrantTypeRepoParam(UpdateClientGrantTypeApiModel createClientGrantTypeApiModel)
        {
            GrantType = createClientGrantTypeApiModel.GrantType;
        }
        public string GrantType{ get; set; }
    }
}