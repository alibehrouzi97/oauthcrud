using OAuthCRUD.Models.ApiModels.ClientApiModels;
using OAuthCRUD.Models.ApiModels.ClientGrantTypeApiModels;

namespace OAuthCRUD.Models.RepoParams.ClientGrantType
{
    public class CreateClientGrantTypeRepoParam
    {
        public CreateClientGrantTypeRepoParam(CreateClientGrantTypeApiModel createClientGrantTypeApiModel)
        {
            GrantType = createClientGrantTypeApiModel.GrantType;
            ClientId = createClientGrantTypeApiModel.ClientId;
        }
        public string GrantType{ get; set; }
        public int ClientId{ get; set; }
    }
}