using OAuthCRUD.Models.ApiModels.ClientPostLogoutRedirectUriApiModuels;

namespace OAuthCRUD.Models.RepoParams.ClientPostLogoutRedirectUri
{
    public class UpdateClientPostLogoutRedirectUriRepoParam
    {
        public UpdateClientPostLogoutRedirectUriRepoParam(
            UpdateClientPostLogoutRedirectUriApiModel createClientPostLogoutRedirectUriApiModel)
        {
            PostLogoutRedirectUri = createClientPostLogoutRedirectUriApiModel.PostLogoutRedirectUri;
        }
        public string PostLogoutRedirectUri { get; set; }
    }
}