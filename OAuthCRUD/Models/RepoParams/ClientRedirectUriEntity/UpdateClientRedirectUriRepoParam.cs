using OAuthCRUD.Models.ApiModels.ClientRedirectUriApiModels;

namespace OAuthCRUD.Models.RepoParams.ClientRedirectUriEntity
{
    public class UpdateClientRedirectUriRepoParam
    {
        public UpdateClientRedirectUriRepoParam(UpdateClientRedirectUriApiModel updateClientRedirectUriApiModel)
        {
            RedirectUri = updateClientRedirectUriApiModel.RedirectUri;
        }
        public string RedirectUri{ get; set; }
    }
}