using OAuthCRUD.Models.ApiModels.ClientRedirectUriApiModels;

namespace OAuthCRUD.Models.RepoParams.ClientRedirectUriEntity
{
    public class CreateClientRedirectUriRepoParam
    {
        public CreateClientRedirectUriRepoParam(CreateClientRedirectUriApiModel createClientRedirectUriApiModel)
        {
            RedirectUri = createClientRedirectUriApiModel.RedirectUri;
            ClientId = createClientRedirectUriApiModel.ClientId;
        }
        public string RedirectUri{ get; set; }
        public int ClientId{ get; set; }
    }
}