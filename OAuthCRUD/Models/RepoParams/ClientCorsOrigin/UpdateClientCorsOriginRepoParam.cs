using OAuthCRUD.Models.ApiModels.ClientCorsOriginApiModel;

namespace OAuthCRUD.Models.RepoParams.ClientCorsOrigin
{
    public class UpdateClientCorsOriginRepoParam
    {
        public UpdateClientCorsOriginRepoParam(UpdateClientCorsOriginApiModel updateClientCorsOriginApiModel)
        {
            Origin = updateClientCorsOriginApiModel.Origin;
        }
        public string Origin { get; set; } 
    }
}