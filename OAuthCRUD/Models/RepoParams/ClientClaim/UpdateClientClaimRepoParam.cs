using OAuthCRUD.Models.ApiModels.ClientClaimApiModels;

namespace OAuthCRUD.Models.RepoParams.ClientClaim
{
    public class UpdateClientClaimRepoParam
    {
        public UpdateClientClaimRepoParam(UpdateClientClaimApiModel clientClaimApiModel)
        {
            Type = clientClaimApiModel.Type;
            Value = clientClaimApiModel.Value;
        }
        public string Type { get; set; }
        public string Value { get; set; }
    }
}