using OAuthCRUD.Models.ApiModels.ClientClaimApiModels;

namespace OAuthCRUD.Models.RepoParams.ClientClaim
{
    public class CreateClientClaimRepoParam
    {
        public CreateClientClaimRepoParam(CreateClientClaimApiModel createClientClaimApiModel)
        {
            Type = createClientClaimApiModel.Type;
            Value = createClientClaimApiModel.Value;
            ClientId = createClientClaimApiModel.ClientId;
        }
        public string Type { get; set; }
        public string Value { get; set; }
        public int ClientId { get; set; }
    }
}