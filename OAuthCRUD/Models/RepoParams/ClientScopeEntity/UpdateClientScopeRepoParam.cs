using OAuthCRUD.Models.ApiModels.ClientScopeApiModels;

namespace OAuthCRUD.Models.RepoParams.ClientScopeEntity
{
    public class UpdateClientScopeRepoParam
    {
        public UpdateClientScopeRepoParam(UpdateClientScopeApiModel createClientScopeApiModel)
        {
            Scope = createClientScopeApiModel.Scope;
        }
        public string Scope { get; set; }
    }
}