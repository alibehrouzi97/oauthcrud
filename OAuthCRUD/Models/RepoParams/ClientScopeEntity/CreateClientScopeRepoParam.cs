using OAuthCRUD.Models.ApiModels.ClientScopeApiModels;

namespace OAuthCRUD.Models.RepoParams.ClientScopeEntity
{
    public class CreateClientScopeRepoParam
    {
        public CreateClientScopeRepoParam(CreateClientScopeApiModel updateClientScopeApiModel)
        {
            ClientId = updateClientScopeApiModel.ClientId;
            Scope = updateClientScopeApiModel.Scope;
        }
        public string Scope { get; set; }
        public int ClientId { get; set; }
    }
}