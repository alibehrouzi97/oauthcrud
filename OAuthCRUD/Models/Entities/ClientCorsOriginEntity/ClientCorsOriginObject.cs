using System;
using OAuthCRUD.Models.ApiModels.ClientCorsOriginApiModel;
using OAuthCRUD.Models.RepoResults.ClientCorsOrigin;

namespace OAuthCRUD.Models.Entities.ClientCorsOriginEntity
{
    public class ClientCorsOriginObject
    {
        private readonly CreateClientCorsOriginRepoResult _createClientCorsOriginRepoResult;

        public ClientCorsOriginObject(CreateClientCorsOriginRepoResult createClientCorsOriginRepoResult)
        {
            _createClientCorsOriginRepoResult = createClientCorsOriginRepoResult;
        }

        public ClientCorsOriginGetCompleteApiModel GetComplete()
        {
            if (_createClientCorsOriginRepoResult == null)
            {
                throw new Exception("CreateClientCorsOriginRepoResult is null");

            }
            return new ClientCorsOriginGetCompleteApiModel(_createClientCorsOriginRepoResult);
        }
        public ClientCorsOriginGetSummaryApiModel GetSummary()
        {
            if (_createClientCorsOriginRepoResult == null)
            {
                throw new Exception("CreateClientCorsOriginRepoResult is null");

            }
            return new ClientCorsOriginGetSummaryApiModel(_createClientCorsOriginRepoResult);
        }
    }
}