using System;
using OAuthCRUD.Models.ApiModels.ClientRedirectUriApiModels;
using OAuthCRUD.Models.ApiModels.ClientScopeApiModels;
using OAuthCRUD.Models.RepoResults.ClientScope;

namespace OAuthCRUD.Models.Entities.ClientScopeEntity
{
    public class ClientScopeObject
    {
        private readonly CreateClientScopeRepoResult _createClientScopeRepoResult;

        public ClientScopeObject(CreateClientScopeRepoResult createClientScopeRepoResult)
        {
            _createClientScopeRepoResult = createClientScopeRepoResult;
            
        }

        public ClientScopeGetCompleteApiModel GetComplete()
        {
            if (_createClientScopeRepoResult == null)
            {
                
                throw new Exception("CreateClientScopeRepoResult is null in GetComplete in ClientRedirectUriObject");
            }
            return new ClientScopeGetCompleteApiModel(_createClientScopeRepoResult);
        }
        
        public ClientScopeGetSummaryApiModel GetSummary()
        {
            if (_createClientScopeRepoResult == null)
            {
                throw new Exception("CreateClientScopeRepoResult is null in GetSummary in ClientRedirectUriObject");
            }
            return new ClientScopeGetSummaryApiModel(_createClientScopeRepoResult);
        }
    }
}