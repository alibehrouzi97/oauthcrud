using System;
using OAuthCRUD.Models.ApiModels.ClientGrantTypeApiModels;
using OAuthCRUD.Models.RepoResults.ClientGrantType;

namespace OAuthCRUD.Models.Entities.ClientGrantTypeEntity
{
    public class ClientGrantTypeObject
    {
        private readonly CreateClientGrantTypeRepoResult _createClientGrantTypeRepoResult;

        public ClientGrantTypeObject(CreateClientGrantTypeRepoResult createClientGrantTypeRepoResult)
        {
            _createClientGrantTypeRepoResult = createClientGrantTypeRepoResult;
        }

        public ClientGrantTypeGetCompleteApiModel GetComplete()
        {
            if (_createClientGrantTypeRepoResult == null)
            {
                throw new Exception("CreateClientGrantTypeRepoResult is null in GetComplete in ClientGrantTypeObject");

            }
            return new ClientGrantTypeGetCompleteApiModel(_createClientGrantTypeRepoResult);
        }
        public ClientGrantTypeGetSummaryApiModel GetSummary()
        {
            if (_createClientGrantTypeRepoResult == null)
            {
                throw new Exception("CreateClientGrantTypeRepoResult is null in GetSummary inClientGrantTypeObject");

            }
            return new ClientGrantTypeGetSummaryApiModel(_createClientGrantTypeRepoResult);
        }
        
    }
}