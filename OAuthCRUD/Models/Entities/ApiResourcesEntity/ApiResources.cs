using System;

namespace OAuthCRUD.Models.Entities.ApiResourcesEntity
{
    public class ApiResources
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string DisplayName { get; set; }
        public string Description { get; set; }
        public DateTime Created { get; set; }
        public DateTime? Update { get; set; }
        
    }
}