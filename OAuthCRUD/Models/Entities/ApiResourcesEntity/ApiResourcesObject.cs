using System;
using OAuthCRUD.Models.ApiModels.ApiResourcesModels;
using OAuthCRUD.Models.RepoResults.ApiResources;

namespace OAuthCRUD.Models.Entities.ApiResourcesEntity
{
    public class ApiResourcesObject
    {
        private readonly CreateApiResourcesRepoResult _createApiResourcesRepoResult;

        public ApiResourcesObject(CreateApiResourcesRepoResult createApiResourcesRepoResult)
        {
            _createApiResourcesRepoResult = createApiResourcesRepoResult;
        }

        public ApiResourcesGetCompleteApiModel GetComplete()
        {
            if (_createApiResourcesRepoResult == null)
            {
                throw new Exception("CreateApiResourcesRepoResult is null in GetComplete");
            }
            return new ApiResourcesGetCompleteApiModel(_createApiResourcesRepoResult);
        }
        public ApiResourcesGetSummaryApiModel GetSummary()
        {
            if (_createApiResourcesRepoResult == null)
            {
                throw new Exception("CreateApiResourcesRepoResult is null in GetSummary");
            }
            return new ApiResourcesGetSummaryApiModel(_createApiResourcesRepoResult);
        }
    }
}