using System;
using Microsoft.Extensions.Logging;
using OAuthCRUD.Models.ApiModels.ClientApiModels;
using OAuthCRUD.Models.RepoResults.Client;

namespace OAuthCRUD.Models.Entities.ClientEntity
{
    public class ClientObject
    {
        private readonly CreateClientRepoResult _createClientRepoResult;

        public ClientObject(CreateClientRepoResult createClientRepoResult)
        {
            _createClientRepoResult = createClientRepoResult;
            
        }

        public ClientGetCompleteApiModel GetComplete()
        {
            if (_createClientRepoResult == null)
            {
                
                throw new Exception("CreateClientRepoResult is null in GetComplete in ClientObject");
            }
            return new ClientGetCompleteApiModel(_createClientRepoResult);
        }
        
        public ClientGetSummaryApiModel GetSummary()
        {
            if (_createClientRepoResult == null)
            {
                throw new Exception("CreateClientRepoResult is null in GetSummary in ClientObject");
            }
            return new ClientGetSummaryApiModel(_createClientRepoResult);
        }
    }
}