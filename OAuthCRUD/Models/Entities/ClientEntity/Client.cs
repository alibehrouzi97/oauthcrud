using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using OAuthCRUD.Models.Entities.ClientClaimEntity;
using OAuthCRUD.Models.Entities.ClientCorsOriginEntity;
using OAuthCRUD.Models.Entities.ClientGrantTypeEntity;
using OAuthCRUD.Models.Entities.ClientPostLogoutRedirectUriEntity;
using OAuthCRUD.Models.Entities.ClientRedirectUriEntity;
using OAuthCRUD.Models.Entities.ClientScopeEntity;

namespace OAuthCRUD.Models.Entities.ClientEntity
{
    public class Client
    {
        public int Id { get; set; }
        [Required]
        public string ClientName { get; set; }
        [Required]
        public string Description { get; set; }
        
        public List<ClientGrantType> AllowedGrantTypes { get; set; }

        public bool RequirePkce { get; set; }

        public List<ClientRedirectUri> RedirectUris { get; set; }
        public List<ClientPostLogoutRedirectUri> PostLogoutRedirectUris { get; set; }
        public bool AllowOfflineAccess { get; set; }

        public List<ClientScope> AllowedScopes { get; set; }

        public List<ClientClaim> Claims { get; set; }
        public bool AlwaysSendClientClaims { get; set; }
        public List<ClientCorsOrigin> AllowedCorsOrigins { get; set; }
        public DateTime? LastAccessed { get; set; }

    }
}