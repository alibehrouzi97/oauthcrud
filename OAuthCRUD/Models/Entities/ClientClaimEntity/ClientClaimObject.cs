using System;
using OAuthCRUD.Models.ApiModels.ClientClaimApiModels;
using OAuthCRUD.Models.RepoResults.ClientClaim;

namespace OAuthCRUD.Models.Entities.ClientClaimEntity
{
    public class ClientClaimObject
    {
        private readonly CreateClientClaimRepoResult _createClientClaimRepoResult;

        public ClientClaimObject(CreateClientClaimRepoResult createClientClaimRepoResult)
        {
            _createClientClaimRepoResult = createClientClaimRepoResult;
        }

        public ClientClaimGetCompleteApiModel GetComplete()
        {
            if (_createClientClaimRepoResult == null)
            {
                throw new Exception("CreateClientClaimRepoResult is null");
            }
            return new ClientClaimGetCompleteApiModel(_createClientClaimRepoResult);
        }
        public ClientClaimGetSummaryApiModel GetSummary()
        {
            if (_createClientClaimRepoResult == null)
            {
                throw new Exception("CreateClientClaimRepoResult is null");
            }
            return new ClientClaimGetSummaryApiModel(_createClientClaimRepoResult);
        }
        
    }
}