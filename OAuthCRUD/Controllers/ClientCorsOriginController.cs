using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Exceptions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using OAuthCRUD.Manager;
using OAuthCRUD.Models.ApiModels.ClientCorsOriginApiModel;
using Ronika.Utility.ApiResponse;
using Ronika.Utility.Consts;

namespace OAuthCRUD.Controllers
{
    [Route("ClientCorsOrigin")]
    public class ClientCorsOriginController : RonikaControllerBase
    {
        
        
        private readonly IClientCorsOriginManager _clientCorsOriginManager;

        public ClientCorsOriginController(IClientCorsOriginManager clientCorsOriginManager)
        {
            _clientCorsOriginManager = clientCorsOriginManager;
        }

        //Create A ClientCorsOrigin
        [HttpPost]
        public async Task<IActionResult> CreateClientCorsOriginAsync([FromBody] CreateClientCorsOriginApiModel clientCorsOriginApiModel)
        {
            
            
            if (!ModelState.IsValid)
            {
                
                throw new BadRequestException("Model Is Not Valid")
                {
                    ExceptionDetails = new ExceptionDetails
                    {
                        Message = RonikaErrorMessages.Public.BadRequest.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 44"
                    }
                };
            }
            
            var clientCorsOriginObject=await _clientCorsOriginManager.CreateClientCorsOriginAsync(clientCorsOriginApiModel);

            if (clientCorsOriginObject == null)
            {
                throw new RonikaStructureException("clientCorsOriginObject is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 58"
                    }
                };                

            }
            
            return Ok(new
            {
                clientCorsOrigin=clientCorsOriginObject.GetComplete()
            });
        }
        
        
        //Update A ClientCorsOrigin
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateClientCorsOriginAsync([FromRoute] int id,[FromBody] UpdateClientCorsOriginApiModel updateClientCorsOriginApiModel)
        {
            
            if (id.ToString()==null)
            {
                throw new BadRequestException("Id is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.BadRequest.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 83"
                    }
                };
                
            }

            if (!ModelState.IsValid)
            {
                
                throw new BadRequestException("Model Is Not Valid")
                {
                    ExceptionDetails = new ExceptionDetails
                    {
                        Message = RonikaErrorMessages.Public.BadRequest.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 97"
                    }
                };
                
            }
            
            var clientCorsOriginObject=await _clientCorsOriginManager.UpdateClientCorsOriginAsync(updateClientCorsOriginApiModel, id);
            
            
            if (clientCorsOriginObject == null)
            {
                throw new RonikaStructureException("clientCorsOriginObject is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 113"
                    }
                };                

            }
            
            return Ok(new
            {
                clientCorsOrigin=clientCorsOriginObject.GetComplete()
            });
            
        }
        
        //Get All ClientCorsOrigin
        [HttpGet]
        public async Task<IActionResult> GetAllClientCorsOriginAsync()
        {
            
            
            var allClientCorsOriginObjects = await _clientCorsOriginManager.GetAllClientCorsOriginAsync();
            
            if (allClientCorsOriginObjects == null)
            {
                throw new RonikaStructureException("allClientCorsOriginObjects is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 141"
                    }
                };
            }
            
            var corsOriginGetSummaryApiModels = new List<ClientCorsOriginGetSummaryApiModel>();
                
            foreach (var clientCorsOriginObject in allClientCorsOriginObjects)
            {
                corsOriginGetSummaryApiModels.Add(clientCorsOriginObject.GetSummary());
            }

           
            return Ok(new
            {
                ClientCorsOrigins=corsOriginGetSummaryApiModels
            });
        }

        //Get One ClientCorsOrigin
        [HttpGet("{id}")]
        public async Task<IActionResult> GetOneClientCorsOriginAsync([FromRoute] int id)
        {
            if (id.ToString() == null)
            {
                throw new BadRequestException("Id Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.BadRequest.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 171"
                    }
                };
            }

            var clientCorsOriginObject = await _clientCorsOriginManager.GetOneClientCorsOriginAsync(id);
               
            if (clientCorsOriginObject == null)
            {
                throw new RonikaStructureException("clientCorsOriginObject Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name +": 185"
                    }
                };                

            }
            
            return Ok(new
            {
                ClientCorsOrigin=clientCorsOriginObject.GetComplete()
            });
            
        }




        //Delete ClientCorsOrigin
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteClientCorsOriginAsync([FromRoute] int id)
        {
            
            if (id.ToString() == null)
            {
                throw new BadRequestException("Id Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.BadRequest.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 213"
                    }
                };
            }
            
            await _clientCorsOriginManager.DeleteClientCorsOriginAsync(id);

            return Ok("Deleted");
        }
    }
}
