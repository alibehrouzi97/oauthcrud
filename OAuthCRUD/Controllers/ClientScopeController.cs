using System;
using System.Collections.Generic;
using System.Reflection;
using System.Threading.Tasks;
using Exceptions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using OAuthCRUD.Manager;
using OAuthCRUD.Models.ApiModels.ClientScopeApiModels;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Math;
using Ronika.Utility.ApiResponse;
using Ronika.Utility.Consts;

namespace OAuthCRUD.Controllers
{
    [Route("ClientScope")]
    public class ClientScopeController : RonikaControllerBase
    {
        private readonly IClientScopeManager _clientScopeManager;
        
        public ClientScopeController(IClientScopeManager clientScopeManager)
        {
            _clientScopeManager = clientScopeManager;
        }
        

        //Create A  ClientScope
        [HttpPost]
        public async Task<IActionResult> CreateClientScopeAsync([FromBody] CreateClientScopeApiModel createClientScopeApiModel)
        {
            
            
            if (!ModelState.IsValid)
            {
                
                throw new BadRequestException("Model Is Not Valid")
                {
                    ExceptionDetails = new ExceptionDetails
                    {
                        Message = RonikaErrorMessages.Public.BadRequest.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 41"
                    }
                };
                
            }
            
            var clientScopeObject = await _clientScopeManager.CreateClientScopeAsync(createClientScopeApiModel);
            
            if (clientScopeObject == null)
            {
                throw new RonikaStructureException("clientScopeObject is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 56"
                    }
                };                

            }
            
            return Ok(new
            {
                clientScope=clientScopeObject.GetComplete()
            });
            
        }
        
        //Update A ClientScope
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateClientScopeAsync([FromRoute] int id,[FromBody] UpdateClientScopeApiModel updateClientScopeApiModel)
        {
            
            if (id.ToString()==null)
            {
                throw new BadRequestException("Id is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.BadRequest.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 81"
                    }
                };
                
            }
            
            
            
            if (!ModelState.IsValid)
            {
                
                throw new BadRequestException("Model Is Not Valid")
                {
                    ExceptionDetails = new ExceptionDetails
                    {
                        Message = RonikaErrorMessages.Public.BadRequest.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 97"
                    }
                };
                
            }
            
            var clientScopeObject  =await _clientScopeManager.UpdateClientScopeAsync(updateClientScopeApiModel, id);
            
            if (clientScopeObject == null)
            {
                throw new RonikaStructureException("clientScopeObject is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 112"
                    }
                };                

            }
            
            return Ok(new
            {
                ClientScope = clientScopeObject.GetComplete()
            });
            
        }
        
        // GET: ClientScope
        [HttpGet]
        public async Task<IActionResult> GetAllClientScopeAsync()
        {
            
            var clientScopeObjects = await _clientScopeManager.GetAllClientScopesAsync();
            
            if (clientScopeObjects == null)
            {
                throw new RonikaStructureException("clientScopeObjects is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 139"
                    }
                };
            }
            
            var clientScopeGetSummaryApiModels = new List<ClientScopeGetSummaryApiModel>();
                
            foreach (var clientScopeObject in clientScopeObjects)
            {
                clientScopeGetSummaryApiModels.Add(clientScopeObject.GetSummary());
            }

            return Ok(new
            {
                clientScopes=clientScopeGetSummaryApiModels
            });

        }

        // GET: ClientScope/Details/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetOneClientScopeAsync([FromRoute] int id)
        {
            
            if (id.ToString() == null)
            {
                throw new BadRequestException("Id Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.BadRequest.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 170"
                    }
                };
            }

            var clientScopeObject = await _clientScopeManager.GetOneClientScopeAsync(id);
            
            
            if (clientScopeObject == null)
            {
                throw new RonikaStructureException("clientScopeObject Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name +": 185"
                    }
                };                

            }

            
            return Ok(new
            {
                clientScope=clientScopeObject.GetComplete()
            });
        }
        
        

        //Delete A Client Scope
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteClientScopeAsync([FromRoute] int id)
        {
            
            if (id.ToString() == null)
            {
                throw new BadRequestException("Id Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.BadRequest.Message,
                        OccurAddress =  MethodBase.GetCurrentMethod().Name +": 212"
                    }
                };
                
            }
            
            await _clientScopeManager.DeleteClientScopeAsync(id);

            return Ok("Deleted");
        }
    }
}