using System;
using System.Collections.Generic;
using System.Reflection;
using System.Threading.Tasks;
using Exceptions;
using OAuthCRUD.Manager;
using OAuthCRUD.Models.ApiModels.ApiResourcesModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Ronika.ExceptionMiddleware;
using Ronika.Utility.ApiResponse;
using Ronika.Utility.Consts;

namespace OAuthCRUD.Controllers
{
    [Route("ApiResource")]
    public class ApiResourcesController : RonikaControllerBase
    {

        private readonly IApiResourcesManager _apiResourcesManager;
        
        
        public ApiResourcesController( IApiResourcesManager apiResourcesManager)
        {
     
            _apiResourcesManager = apiResourcesManager;
        }
        
        //Create A ApiResource
        [HttpPost]
        public async Task<IActionResult> CreateApiResource([FromBody] CreateApiResourcesApiModel createApiResourcesApiModel)
        {

            if (!ModelState.IsValid)
            {
                throw new BadRequestException("Model Is Not Valid")
                {
                    ExceptionDetails = new ExceptionDetails
                    {
                        Message = RonikaErrorMessages.Public.BadRequest.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 41"
                    }
                };
            }

            var apiResourcesObject = await _apiResourcesManager.CreateApiResourcesAsync(createApiResourcesApiModel);

            if (apiResourcesObject==null)
            {
                throw new RonikaStructureException("apiResourceObject is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 55"
                    }
                };
            }
            
            return Ok(new
            {
                ApiResource=apiResourcesObject.GetComplete()
            });
        }
        
        //Update A ApiResource
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateDevice(
            [FromRoute] int id, [FromBody] UpdateApiResourcesApiModel model
        )
        {

            if (id.ToString()==null)
            {
                throw new BadRequestException("Id is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.BadRequest.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 80"
                    }
                };
            }

            if (!ModelState.IsValid)
            {
                
                throw new BadRequestException("Model Is Not Valid")
                {
                    ExceptionDetails = new ExceptionDetails
                    {
                        Message = RonikaErrorMessages.Public.BadRequest.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 93"
                    }
                };
            }


            var apiResourceObject = await _apiResourcesManager.UpdateApiResourcesAsync(model, id);

            if (apiResourceObject == null)
            {
                throw new RonikaStructureException("apiResourceObject is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 108"
                    }
                };                

            }

            return Ok(new
            {
                ApiResourcesGetCompleteApiModel=apiResourceObject.GetComplete()
            });

        }
        
        
        //GetAll ApiResources
        [HttpGet]
        public async Task<IActionResult> GetAllApiResourcesAsync()
        {
            var allApiResourcesObjects = await _apiResourcesManager.GetAllApiResourcesAsync();

            if (allApiResourcesObjects == null)
            {
                throw new RonikaStructureException("allApiResourcesObjects is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 135"
                    }
                };
            }

            var apiResourcesGetSummaryApiModels = new List<ApiResourcesGetSummaryApiModel>();

            
            foreach (var apiResourcesObject in allApiResourcesObjects)
            {
                apiResourcesGetSummaryApiModels.Add(apiResourcesObject.GetSummary());
            }
            
            
            return Ok(new
            {
                ApiResourcesGetSummaryApiModels=apiResourcesGetSummaryApiModels
            });
        }
        
        //GetOne ApiResource
        [HttpGet("{id}")]
        public async Task<IActionResult> GetOneApiResourceAsync([FromRoute] int id)
        {
            
            if (id.ToString() == null)
            {
                throw new BadRequestException("Id Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.BadRequest.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 167"
                    }
                };
            }

            var apiResourceObject = await _apiResourcesManager.GetOneApiResourcesAsync(id);

            
            if (apiResourceObject == null)
            {
                throw new RonikaStructureException("apiResourceObject Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name +": 182"
                    }
                };                

            }

            return Ok(new
            {
                apiResurceGetCompleteApiModel = apiResourceObject.GetComplete()
            });
            
        }
        
        //Delete A ApiResource
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteApiResourceAsync([FromRoute] int id)
        {
            if (id.ToString() == null)
            {
                throw new BadRequestException("Id Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.BadRequest.Message,
                        OccurAddress =  MethodBase.GetCurrentMethod().Name +": 206"
                    }
                };

            }

            await _apiResourcesManager.DeleteApiResourcesAsync(id);

            return Ok("Deleted");
        }
    }
}