using System;
using System.Collections.Generic;
using System.Reflection;
using System.Threading.Tasks;
using Exceptions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.CodeAnalysis.Differencing;
using Microsoft.Extensions.Logging;
using OAuthCRUD.Manager;
using OAuthCRUD.Models.ApiModels.ClientApiModels;
using OAuthCRUD.Models.Entities.ClientClaimEntity;
using OAuthCRUD.Models.Entities.ClientCorsOriginEntity;
using OAuthCRUD.Models.Entities.ClientGrantTypeEntity;
using OAuthCRUD.Models.Entities.ClientPostLogoutRedirectUriEntity;
using OAuthCRUD.Models.Entities.ClientRedirectUriEntity;
using OAuthCRUD.Models.Entities.ClientScopeEntity;
using Ronika.Utility.ApiResponse;
using Ronika.Utility.Consts;

namespace OAuthCRUD.Controllers
{
    [Route("Client")]
    public class ClientController : RonikaControllerBase
    {
        
        
        private readonly IClientManager _clientManager;

        public ClientController(IClientManager clientManager)
        {
            _clientManager = clientManager;
        }
        
        
        //Create A Client
        [HttpPost]
        public async Task<IActionResult> CreateClientAsync([FromBody] CreateClientApiModel clientModel)
        {
            
            
            if (!ModelState.IsValid)
            {

                throw new BadRequestException("Model Is Not Valid")
                {
                    ExceptionDetails = new ExceptionDetails
                    {
                        Message = RonikaErrorMessages.Public.BadRequest.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 49"
                    }
                };
                
            }
            
            var clientObject=await _clientManager.CreateClientAsync(clientModel);

            if (clientObject == null)
            {
                throw new RonikaStructureException("clientObject is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 64"
                    }
                };                

            }
            
            return Ok(new
            {
                client=clientObject.GetComplete()
            });
        }


        //Update A Client
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateClientAsync([FromRoute] int id,[FromBody] UpdateClientApiModel updateClientApiModel)
        {

            if (id.ToString()==null)
            {
                throw new BadRequestException("Id is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.BadRequest.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 89"
                    }
                };
                
            }
            
            
            if (!ModelState.IsValid)
            {
                
                throw new BadRequestException("Model Is Not Valid")
                {
                    ExceptionDetails = new ExceptionDetails
                    {
                        Message = RonikaErrorMessages.Public.BadRequest.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 104"
                    }
                };
                
            }
            
            var clientObject=await _clientManager.UpdateClientAsync(updateClientApiModel, id);
            
            if (clientObject == null)
            {
                throw new RonikaStructureException("clientObject is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 119"
                    }
                };                

            }
            
            return Ok(new
            {
                client=clientObject.GetComplete()
            });

        }


        //Get All Client
        [HttpGet]
        public async Task<IActionResult> GetAllClientAsync()
        {
            
            var clientObjects = await _clientManager.GetAllClientsAsync();

            if (clientObjects == null)
            {
                throw new RonikaStructureException("clientObjects is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 147"
                    }
                };
            }
            
            var clientSummary=new List<ClientGetSummaryApiModel>();
                
            foreach (var clientObject in clientObjects)
            {
                clientSummary.Add(clientObject.GetSummary());
            }
                
            return Ok(new
            {
                Client=clientSummary
            });
            
            
        }

        //Get One Client
        [HttpGet("{id}")]
        public async Task<IActionResult> GetOneClientAsync([FromRoute] int id)
        {
            
            if (id.ToString() == null)
            {
                throw new BadRequestException("Id Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.BadRequest.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 179"
                    }
                };
                
            }

            var clientObject = await _clientManager.GetOneClientAsync(id);
             
            if (clientObject == null)
            {
                throw new RonikaStructureException("clientObject is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 194"
                    }
                };                

            }
            
            return Ok(new
            {
                Client=clientObject.GetComplete()
            });


        }


        


        //Delete Client
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteClientAsync([FromRoute] int id)
        {
            
            if (id.ToString() == null)
            {
                
                throw new BadRequestException("Id Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.BadRequest.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 225"
                    }
                };
            }

            await _clientManager.DeleteClientAsync(id);

            return Ok("Deleted");

        }


    }
}
