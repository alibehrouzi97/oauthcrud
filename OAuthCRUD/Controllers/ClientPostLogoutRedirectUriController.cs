using System.Collections.Generic;
using System.Reflection;
using System.Threading.Tasks;
using Exceptions;
using Microsoft.AspNetCore.Mvc;
using OAuthCRUD.Manager;
using OAuthCRUD.Models.ApiModels.ClientPostLogoutRedirectUriApiModuels;
using Ronika.Utility.ApiResponse;
using Ronika.Utility.Consts;

namespace OAuthCRUD.Controllers
{
    [Route("ClientPostLogoutRedirectUri")]
    public class ClientPostLogoutRedirectUriController : RonikaControllerBase
    {
        
        private readonly IClientPostLogoutRedirectUriManager _clientPostLogoutRedirectUriManager;

        public ClientPostLogoutRedirectUriController(IClientPostLogoutRedirectUriManager clientPostLogoutRedirectUriManager)
        {
            _clientPostLogoutRedirectUriManager = clientPostLogoutRedirectUriManager;
            
        }
        
        //Create A ClientPostLogoutRedirectUri
        [HttpPost]
        public async Task<IActionResult> CreateClientPostLogoutRedirectUriAsync([FromBody] CreateClientPostLogoutRedirectUriApiModel createClientPostLogoutRedirectUriApiModel)
        {
            
            
            if (!ModelState.IsValid)
            {
                
                throw new BadRequestException("Model Is Not Valid")
                {
                    ExceptionDetails = new ExceptionDetails
                    {
                        Message = RonikaErrorMessages.Public.BadRequest.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 39"
                    }
                };
            }
            
            var clientPostLogoutRedirectUriObject=await _clientPostLogoutRedirectUriManager.CreateClientPostLogoutRedirectUriAsync(createClientPostLogoutRedirectUriApiModel);

            if (clientPostLogoutRedirectUriObject == null)
            {
                throw new RonikaStructureException("clientPostLogoutRedirectUriObject is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 53"
                    }
                };                

            }

            return Ok(new
            {
                clientPostLogoutRedirectUri=clientPostLogoutRedirectUriObject.GetComplete()
            });
            
        }
        

        //Update A ClientPostLogoutRedirectUri
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateClientPostLogoutRedirectUriAsync([FromRoute] int id,[FromBody] UpdateClientPostLogoutRedirectUriApiModel updateClientPostLogoutRedirectUriApiModel)
        {
            
            if (id.ToString()==null)
            {
                throw new BadRequestException("Id is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.BadRequest.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 79"
                    }
                };
                
            }
            
            
            if (!ModelState.IsValid)
            {
                
                throw new BadRequestException("Model Is Not Valid")
                {
                    ExceptionDetails = new ExceptionDetails
                    {
                        Message = RonikaErrorMessages.Public.BadRequest.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 94"
                    }
                };
            }
            
            var clientPostLogoutRedirectUriObject=await _clientPostLogoutRedirectUriManager.UpdateClientPostLogoutRedirectUriAsync(updateClientPostLogoutRedirectUriApiModel, id);

            if (clientPostLogoutRedirectUriObject == null)
            {
                throw new RonikaStructureException("clientPostLogoutRedirectUriObject is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 108"
                    }
                };                

            }
            
            return Ok(new
            {
                clientPostLogoutRedirectUri=clientPostLogoutRedirectUriObject.GetComplete()
            });
            
        }
        

        //Get All ClientPostLogoutRedirectUri
        [HttpGet]
        public async Task<IActionResult> GetAllClientPostLogoutRedirectUriAsync()
        {
            
            var clientPostLogoutRedirectUriObjects = await _clientPostLogoutRedirectUriManager.GetAllClientPostLogoutRedirectUrisAsync();

            if (clientPostLogoutRedirectUriObjects == null)
            {
                throw new RonikaStructureException("clientPostLogoutRedirectUriObjects is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 136"
                    }
                };
            }
            
            var clientPostLogoutRedirectUriGetSummaryApiModels = new List<ClientPostLogoutRedirectUriGetSummaryApiModel>();
                
            foreach (var clientPostLogoutRedirectUriObject in clientPostLogoutRedirectUriObjects)
            {
                clientPostLogoutRedirectUriGetSummaryApiModels.Add(clientPostLogoutRedirectUriObject.GetSummary());
            }

            return Ok(new
            {
                clientPostLogoutRedirectUris=clientPostLogoutRedirectUriGetSummaryApiModels
            });
        }

        //Get One ClientPostLogoutRedirectUri
        [HttpGet("{id}")]
        public async Task<IActionResult> GetOneClientPostLogoutRedirectUriAsync([FromRoute] int id)
        {
            
            if (id.ToString() == null)
            {
                throw new BadRequestException("Id Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.BadRequest.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 166"
                    }
                };
            }

            
            var clientPostLogoutRedirectUriObject = await _clientPostLogoutRedirectUriManager.GetOneClientPostLogoutRedirectUriAsync(id);

            if (clientPostLogoutRedirectUriObject == null)
            {
                throw new RonikaStructureException("clientPostLogoutRedirectUriObject Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name +": 181"
                    }
                };                

            }

            return Ok(new
            {
                clientPostLogoutRedirectUri = clientPostLogoutRedirectUriObject.GetComplete()
            });

        }
        
        

        //Delete ClientPostLogoutRedirectUri
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteClientPostLogoutRedirectUriAsync([FromRoute] int id)
        {
            
            if (id.ToString() == null)
            {
                throw new BadRequestException("Id Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.BadRequest.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 208"
                    }
                };
            }
            
            await _clientPostLogoutRedirectUriManager.DeleteClientPostLogoutRedirectUriAsync(id);

            return Ok("Deleted");
        }
    }
}
