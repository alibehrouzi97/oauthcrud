using System;
using System.Collections.Generic;
using System.Reflection;
using System.Threading.Tasks;
using Exceptions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using OAuthCRUD.Manager;
using OAuthCRUD.Models.ApiModels.ClientRedirectUriApiModels;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Math;
using Ronika.Utility.ApiResponse;
using Ronika.Utility.Consts;

namespace OAuthCRUD.Controllers
{
    [Route("ClientRedirectUri")]
    public class ClientRedirectUriController : RonikaControllerBase
    {
        
        private readonly IClientRedirectUriManager _clientRedirectUriManager;

        public ClientRedirectUriController(IClientRedirectUriManager clientRedirectUriManager)
        {
            
            _clientRedirectUriManager = clientRedirectUriManager;

        }
        
        
        //Create A ClientRedirectUri
        
        [HttpPost]
        public async Task<IActionResult> CreateClientRedirectUriAsync([FromBody] CreateClientRedirectUriApiModel createClientRedirectUriApiModel)
        {
            
            
            if (!ModelState.IsValid)
            {
                throw new BadRequestException("Model Is Not Valid")
                {
                    ExceptionDetails = new ExceptionDetails
                    {
                        Message = RonikaErrorMessages.Public.BadRequest.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 44"
                    }
                };
                
            }
            
            var clientRedirectUriObject=await _clientRedirectUriManager.CreateClientRedirectUriAsync(createClientRedirectUriApiModel);

            if (clientRedirectUriObject == null)
            {
                throw new RonikaStructureException("clientRedirectUriObject is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 59"
                    }
                };                

            }
            
            return Ok(new
            {
                clientRedirectUri=clientRedirectUriObject.GetComplete()
            });
            
        }
        
        //Update A ClientRedirectUri

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateClientRedirectUriAsync([FromRoute] int id,[FromBody] UpdateClientRedirectUriApiModel updateClientRedirectUriApiModel)
        {
            
            if (id.ToString()==null)
            {
                throw new BadRequestException("Id is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.BadRequest.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 85"
                    }
                };
                
            }
            
            
            if (!ModelState.IsValid)
            {
                
                throw new BadRequestException("Model Is Not Valid")
                {
                    ExceptionDetails = new ExceptionDetails
                    {
                        Message = RonikaErrorMessages.Public.BadRequest.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 100"
                    }
                };
                
            }
            
            var clientRedirectUriObject=await _clientRedirectUriManager.UpdateClientRedirectUriAsync(updateClientRedirectUriApiModel, id);

            if (clientRedirectUriObject == null)
            {
                throw new RonikaStructureException("clientRedirectUriObject is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 115"
                    }
                };                

            }
            
            return Ok(new
            {
                clientRedirectUri=clientRedirectUriObject.GetComplete()
            });
            
        }
        
         ///Get All ClientRedirectUris
         [HttpGet]
        public async Task<IActionResult> GetAllClientRedirectUrisAsync()
        {
            
            
            var clientRedirectUriObjects = await _clientRedirectUriManager.GetAllClientRedirectUrisAsync();

            if (clientRedirectUriObjects == null)
            {
                throw new RonikaStructureException("clientRedirectUriObjects is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 143"
                    }
                };
            }
            
            var clientRedirectUriGetSummaryApiModels = new List<ClientRedirectUriGetSummaryApiModel>();
                
            foreach (var clientRedirectUriObject in clientRedirectUriObjects)
            {
                clientRedirectUriGetSummaryApiModels.Add(clientRedirectUriObject.GetSummary());
            }

            return Ok(new
            {
                clientRedirectUris = clientRedirectUriGetSummaryApiModels
            });

        }

         ///Get A ClientRedirectUri
         [HttpGet("{id}")]
        public async Task<IActionResult> GetOneClientRedirectUriAsync([FromRoute] int id)
        {
            
            if (id.ToString() == null)
            {
                throw new BadRequestException("Id Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.BadRequest.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 174"
                    }
                };
            }

            var clientRedirectUriObject = await _clientRedirectUriManager.GetOneClientRedirectUriAsync(id);

            if (clientRedirectUriObject == null)
            {
                throw new RonikaStructureException("clientRedirectUriObject Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name +": 188"
                    }
                };                

            }

            return Ok(new
            {
                clientRedirectUri=clientRedirectUriObject.GetComplete()
            });
        }
        

         

         ///Delete A ClientRedirectUri
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteClientRedirectUriAsync([FromRoute] int id)
        {
            
            if (id.ToString() == null)
            {
                throw new BadRequestException("Id Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.BadRequest.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 215"
                    }
                };
            }
            
            await _clientRedirectUriManager.DeleteClientRedirectUriAsync(id);

            return Ok("Deleted");
            
        }
    }
}