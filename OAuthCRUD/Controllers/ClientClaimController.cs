using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Exceptions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using OAuthCRUD.Manager;
using OAuthCRUD.Models.ApiModels.ClientClaimApiModels;
using Ronika.Utility.ApiResponse;
using Ronika.Utility.Consts;

namespace OAuthCRUD.Controllers
{
    [Route("ClientClaim")]
    public class ClientClaimController : RonikaControllerBase
    {
        
        private readonly IClientClaimManager _clientClaimManager;

        public ClientClaimController(IClientClaimManager clientClaimManager)
        {
            _clientClaimManager = clientClaimManager;
            
        }
        
        //Create A ClientClaim
        [HttpPost]
        public async Task<IActionResult> CreateClientClaimAsync([FromBody] CreateClientClaimApiModel clientClaimApiModel)
        {

            if (!ModelState.IsValid)
            {

                throw new BadRequestException("Model Is Not Valid")
                {
                    ExceptionDetails = new ExceptionDetails
                    {
                        Message = RonikaErrorMessages.Public.BadRequest.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 43"
                    }
                };
            }
            
            var clientClaimObject=await _clientClaimManager.CreateClientClaimAsync(clientClaimApiModel);

            if (clientClaimObject == null)
            {
                throw new RonikaStructureException("clientClaimObject is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 57"
                    }
                };                

            }

            return Ok(new
            {
                clientClaim=clientClaimObject.GetComplete()
            });
            
        }


        //Update A ClientClaim
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateClientClaimAsync([FromRoute] int id,[FromBody] UpdateClientClaimApiModel updateClientClaimApiModel)
        {

            if (id.ToString()==null)
            {
                throw new BadRequestException("Id is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.BadRequest.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 83"
                    }
                };
                
            }
            
            
            if (!ModelState.IsValid)
            {
                
                throw new BadRequestException("Model Is Not Valid")
                {
                    ExceptionDetails = new ExceptionDetails
                    {
                        Message = RonikaErrorMessages.Public.BadRequest.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 98"
                    }
                };
            }
            
            var clientClaimObject = await _clientClaimManager.UpdateClientClaimAsync(updateClientClaimApiModel, id);

            
            if (clientClaimObject == null)
            {
                throw new RonikaStructureException("clientClaimObject is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 113"
                    }
                };                

            }

            return Ok(new
            {
                clientClaim=clientClaimObject.GetComplete()
            });
            
        }

        //Get All ClientClaim
        [HttpGet]
        public async Task<IActionResult> GetAllClientClaimAsync()
        {
            
            var clientClaimObjects = await _clientClaimManager.GetAllClientClaimsAsync();

            if (clientClaimObjects == null)
            {
                throw new RonikaStructureException("clientClaimObjects is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 140"
                    }
                };
            }
            
            var clientClaimSummary = new List<ClientClaimGetSummaryApiModel>();
                
            foreach (var clientClaimObject in clientClaimObjects)
            {
                clientClaimSummary.Add(clientClaimObject.GetSummary());
            }

            return Ok(new
            {
                CLientCliams=clientClaimSummary
            });

        }

        //Get All ClientClaim
        [HttpGet("{id}")]
        public async Task<IActionResult> GetOneClientClaimAsync([FromRoute] int id)
        {
            
            if (id.ToString() == null)
            {
                throw new BadRequestException("Id Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.BadRequest.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 171"
                    }
                };
            }

            var clientClaimObject = await _clientClaimManager.GetOneClientClaimAsync(id);

            if (clientClaimObject == null)
            {
                throw new RonikaStructureException("clientClaimObject is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 185"
                    }
                };                

            }

            return Ok(new
            {
                clientClaim=clientClaimObject.GetComplete()
            });

        }

        
        //Get All ClientClaim
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteClientClaimAsync([FromRoute] int id)
        {
            
            if (id.ToString() == null)
            {
                throw new BadRequestException("Id Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.BadRequest.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 211"
                    }
                };
            }

            await _clientClaimManager.DeleteClientClaimAsync(id);
            
            return Ok("Deleted");


        }
    }
}
