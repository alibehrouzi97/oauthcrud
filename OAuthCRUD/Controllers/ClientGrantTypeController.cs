using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Exceptions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using OAuthCRUD.Manager;
using OAuthCRUD.Models.ApiModels.ClientGrantTypeApiModels;
using Ronika.Utility.ApiResponse;
using Ronika.Utility.Consts;

namespace OAuthCRUD.Controllers
{
    [Route("ClientGrantType")]
    public class ClientGrantTypeController : RonikaControllerBase
    {
        
        
        private readonly IClientGrantTypeManager _clientGrantTypeManager;

        public ClientGrantTypeController(IClientGrantTypeManager clientGrantTypeManager )
        {

            _clientGrantTypeManager = clientGrantTypeManager;
        }
        
        
        //Create A ClientGrantType
        [HttpPost]
        public async Task<IActionResult> CreateClientGrantTypeAsync([FromBody] CreateClientGrantTypeApiModel createClientGrantTypeApiModel)
        {
            
            
            if (!ModelState.IsValid)
            {
                
                throw new BadRequestException("Model Is Not Valid")
                {
                    ExceptionDetails = new ExceptionDetails
                    {
                        Message = RonikaErrorMessages.Public.BadRequest.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 46"
                    }
                };

            }
            
            var clientGrantTypeObject=await _clientGrantTypeManager.CreateClientGrantTypeAsync(createClientGrantTypeApiModel);

            if (clientGrantTypeObject == null)
            {
                throw new RonikaStructureException("clientGrantTypeObject is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 61"
                    }
                };                

            }
            return Ok(new
            {
                clientGrantType=clientGrantTypeObject.GetComplete()
            });
        }


        //Update A ClientGrantType
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateClientGrantTypeAsync([FromRoute] int id,[FromBody] UpdateClientGrantTypeApiModel updateClientGrantTypeApiModel)
        {
            
            if (id.ToString()==null)
            {
                throw new BadRequestException("Id is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.BadRequest.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 85"
                    }
                };
                
            }
            
            
            if (!ModelState.IsValid)
            {
                throw new BadRequestException("Model Is Not Valid")
                {
                    ExceptionDetails = new ExceptionDetails
                    {
                        Message = RonikaErrorMessages.Public.BadRequest.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 99"
                    }
                };
            }
            
            var clientGrantTypeObject=await _clientGrantTypeManager.UpdateClientGrantTypeAsync(updateClientGrantTypeApiModel, id);

            if (clientGrantTypeObject == null)
            {
                throw new RonikaStructureException("clientGrantTypeObject is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 113"
                    }
                };                

            }
            
            return Ok(new
            {
                clientGrantType=clientGrantTypeObject.GetComplete()
            });
            
        }

        //Get All ClientGrantType
        [HttpGet]
        public async Task<IActionResult> GetAllClientGrantTypeAsync()
        {
            
            var clientGrantTypeObjects = await _clientGrantTypeManager.GetAllClientGrantTypesAsync();

            if (clientGrantTypeObjects == null)
            {
                throw new RonikaStructureException("clientGrantTypeObjects is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 140"
                    }
                };
            }
            
            var grantTypeGetSummaryApiModels = new List<ClientGrantTypeGetSummaryApiModel>();
                
            foreach (var clientGrantTypeObject in clientGrantTypeObjects)
            {
                grantTypeGetSummaryApiModels.Add(clientGrantTypeObject.GetSummary());
            }

            return Ok(new
            {
                clientgrantTypes=grantTypeGetSummaryApiModels
            });
        }

        //Get One ClientGrantType
        [HttpGet("{id}")]
        public async Task<IActionResult> GetOneClientGrantTypeAsync([FromRoute] int id)
        {
            
            if (id.ToString() == null)
            {
                throw new BadRequestException("Id Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.BadRequest.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 170"
                    }
                };
            }

            var clientGrantTypeObject = await _clientGrantTypeManager.GetOneClientGrantTypeAsync(id);
                
            if (clientGrantTypeObject == null)
            {
                throw new RonikaStructureException("clientGrantTypeObject Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.StructureError.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name +": 184"
                    }
                };                

            }
            
            return Ok(new
            {
                clientGrantType=clientGrantTypeObject.GetComplete()
            });
            
            
        }

        //Delete ClientGrantType

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteClientGrantTypeAsync([FromRoute] int id)
        {
            
            if (id.ToString() == null)
            {
                throw new BadRequestException("Id Is Null")
                {
                    ExceptionDetails = new ExceptionDetails()
                    {
                        Message = RonikaErrorMessages.Public.BadRequest.Message,
                        OccurAddress = MethodBase.GetCurrentMethod().Name + ": 211"
                    }
                };
            }
            
            await _clientGrantTypeManager.DeleteClientGrantTypeAsync(id);

            return Ok("Deleted");

        }
        
    }
}
